<?php
namespace App\Services\ClassListPrice;

use Illuminate\Support\Facades\Facade;

class ListPriceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassListPrice';
    }
}

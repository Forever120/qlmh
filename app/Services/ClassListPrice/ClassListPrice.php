<?php
namespace App\Services\ClassListPrice;

use App\Models\ListPriceModel;

class ClassListPrice
{
    public function htmlListPrice()
    {
        $list = ListPriceModel::active()->orderBy('NO', 'asc')->get();
        $htmlList = '';
        if (count($list) > 0) {
            $htmlList .= '<ol class="dd-list">';
            foreach ($list as $item) {
                $htmlList .= '<li data-id="' . $item->id . '" class="dd-item">'
                . '<div class="option-menu">'
                . '<a onclick="loadPopupLarge(\'/collumn/edit/' . $item->id . '\')" data-toggle="modal" data-target=".bs-modal-lg"><i data-pack="default" class="ion-edit"></i></a>&nbsp;&nbsp;'
                . '<a onclick="deleteRow(\'/collumn/delete/' . $item->id . '\', \'/collumn/list\')"  tabindex="-1"><i data-pack="default" class="ion-trash-a"></i></a></div>'
                    . '<div class="card b0 dd-handle"><div class="mda-list">'
                    . '<div class="mda-list-item"><div class="mda-list-item-icon item-grab"><em class="ion-drag icon-lg"></em></div>'
                    . '<div class="mda-list-item-text mda-2-line">';
                $htmlList .= '<h4>' . $item->name . '</h4>';
                $htmlList .= '</div><div class="_right">'
                    . '</div></div></div></div>';
                $htmlList .= "</li>";
            }
            $htmlList .= '</ol>';
        }
        return ($htmlList);
    }

    public function recursivelyUpdatePosition($listID, $parentId = 0)
    {
        $idx = 0;
        foreach ($listID as $item) {
            $idx++;
            $listPrice = ListPriceModel::find($item['id']);
            $listPrice->NO = $idx;
            $listPrice->save();
            if (isset($item['children'])) {
                \ClassListPrice::recursivelyUpdatePosition($item['children'], $item['id']);
            }
        }
        return \AppConst::SUCCESS;
    }

    public function index()
    {
        $prices = ListPriceModel::active()->orderBy('NO', 'asc')->get();
        return $prices;
    }
    public function getAll()
    {
        $prices = ListPriceModel::active()->get();
        return $prices;
    }

    public function findById($id)
    {
        $listPrice = ListPriceModel::find($id);
        return $listPrice;
    }

    public function deleteById($id)
    {
        $obj = ListPriceModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function listCollumn($request)
    {
        $htmlList = self::htmlListPrice();
        return [
            'htmlList' => $htmlList,
            'selected_menu' => [9, 97],
        ];
    }

    public function postEditListPrice($request, $id)
    {
        try {
            $listPrice = ($id > 0) ? self::findById($id) : new ListPriceModel;
            $listPrice->name = $request->name;
            $listPrice->NO = 0;
            $listPrice->save();
            return 'success';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function editListPrice($request, $id)
    {
        $collumn = self::findById(intval($id));
        return [
            'collumn' => $collumn,
            'id' => $id,
        ];
    }
}

<?php
namespace App\Services\ClassUser;

use Illuminate\Support\Facades\Facade;

class UserFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassUser';
    }
}

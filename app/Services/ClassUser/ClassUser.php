<?php
namespace App\Services\ClassUser;

use App\Models\UserModel;
use App\Models\UserPermissonModel;

class ClassUser
{
    public function index($request)
    {
        $listUser = UserModel::active();
        $listUser = $listUser->where('id', '!=', 1);
        $listUser = $listUser->orderBy('id', 'desc');
        $listUser = $listUser->paginate(\AppConst::PAGE_SIZE);

        return [
            'listUser' => $listUser,
            'selected_menu' => [7],
        ];
    }

    public function findById($id)
    {
        $user = UserModel::find($id);
        return $user;
    }

    public function deleteById($id)
    {
        $obj = UserModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function detailUser($id)
    {
        $user = self::findById($id);
        return $user;
    }

    public function editUser($id)
    {
        //get detail user infomation
        $user = self::findById($id);

        //get all permission for user
        $listPermission = \ClassUserPermisson::getListUserPermissonById($id);

        $listGroup = \ClassGroup::getAll();
        //get list price
        $listPrice = \ClassListPrice::getAll();

        return [
            'user' => $user,
            'id' => $id,
            'listPrice' => $listPrice,
            'listPermission' => $listPermission,
            'listGroup' => $listGroup,
        ];
    }

    public function findByUserName($username)
    {
        $users = UserModel::active()
            ->where('username', $username)
            ->get();
        return $users;
    }

    public function postEditUser($request, $id)
    {
        //validate user name
        if (empty($request->username)) {
            return '["error","' . sprintf(\AppConst::MSG_ERR_EMPTY_FIELD, "Tên đăng nhập") . '"]';
        }
        $users = self::findByUserName($request->username);
        if (intval($id) == 0) {
            // Case add New user
            if (count($users) > 0) {
                return '["error","' . sprintf(\AppConst::MSG_ERR_EXIST_FIELD, "Tên đăng nhập") . '"]';
            }
            if (empty($request->password)) {
                return '["error","' . sprintf(\AppConst::MSG_ERR_EMPTY_FIELD, "Mật khẩu") . '"]';
            }
        } else {
            // Case edit $id > 0
            $user = self::findById($id);
            if ($user == null) {
                return '["error","' . sprintf(\AppConst::MSG_ERR_NOT_EXIST_FIELD, "User") . '"]';
            }
            if (count($users) > 0 && $user->username != $request->username) {
                return '["error","' . sprintf(\AppConst::MSG_ERR_EXIST_FIELD, "Tên đăng nhập") . '"]';
            }
        }

        if ($request->password != $request->passwordConfirm) {
            return '["error","' . \AppConst::MSG_ERR_PASSWORD_NOT_MATCH . '"]';
        }
        if (empty($request->fullName)) {
            return '["error","' . sprintf(\AppConst::MSG_ERR_EMPTY_FIELD, "Họ tên") . '"]';
        }

        if ($request->groupID == 0) {
            return '["error","' . sprintf(\AppConst::MSG_ERR_NOT_SELECTED, "Nhóm quyền") . '"]';
        }

        try {
            \DB::beginTransaction();

            $user = ($id > 0) ? self::findById($id) : new UserModel;

            if (!empty($request->password)) {
                $user->password = bcrypt($request->password);
            }
            if (!empty($request->birthday)) {
                $user->birthday = $request->birthday;
            }
            $user->userType = $request->userType;
            $user->groupID = $request->groupID;
            $user->username = $request->username;
            $user->fullname = $request->fullName;
            $user->email = $request->email;
            $user->address = $request->address;
            $user->phone = $request->phone;
            $user->skype = $request->skype;
            $user->save();

            // save permission
            if (count($request->permission) > 0) {
                //delete all permistion for user
                UserPermissonModel::where('userID', '=', $user->id)->delete();

                //add new permission for user
                $perData = [];
                foreach ($request->permission as $per) {
                    $perData[] = [
                        'userID' => $user->id,
                        'permissionID' => $per,
                    ];
                }
                $userPermission = \DB::table('zzz_user_permisson')->insert($perData);
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return '["error","' . $e->getMessage() . '"]';
        }
        return '["success"]';
    }

    public function changeProfile()
    {
        //get detail user infomation
        $user = \Auth::user();
        //get all permission for user
        $listUserPermission = \ClassUserPermisson::getListUserPermissonById($user->id);
        $listGroup = \ClassGroup::getAll();
        return [
            'user' => $user,
            'id' => $user->id,
            'listGroup' => $listGroup,
            'listUserPermission' => $listUserPermission,
        ];
    }

    public function postChangeProfile($request)
    {
        //get detail user infomation
        $curUser = \Auth::user();
        if ($curUser->username != $request->username) {
            $users = self::findByUserName($request->username);
            if (count($users) > 0) {
                return ["error", sprintf(\AppConst::MSG_ERR_EXIST_FIELD, "Tên đăng nhập")];
            }
        }

        if (!empty($request->password) && $request->password != $request->passwordConfirm) {
            return ["error", \AppConst::MSG_ERR_PASSWORD_NOT_MATCH];
        }
        if (empty($request->fullName)) {
            return ["error", sprintf(\AppConst::MSG_ERR_EMPTY_FIELD, "Họ tên")];
        }

        $listUserPermission = \ClassUserPermisson::getListUserPermissonById($curUser->id);

        try {
            \DB::beginTransaction();

            $user = self::findById($curUser->id);

            if (!empty($request->password)) {
                $user->password = bcrypt($request->password);
            }
            if (!empty($request->birthday)) {
                $user->birthday = $request->birthday;
            }
            $user->groupID = $request->groupID;
            $user->username = $request->username;
            $user->fullname = $request->fullName;
            $user->email = $request->email;
            $user->address = $request->address;
            $user->phone = $request->phone;
            $user->skype = $request->skype;
            $user->save();

            // save permission
            if (count($request->permission) > 0) {
                //delete all permistion for user
                UserPermissonModel::where('userID', '=', $user->id)->delete();

                //add new permission for user
                $perData = [];
                foreach ($request->permission as $per) {
                    $perData[] = [
                        'userID' => $user->id,
                        'permissionID' => $per,
                    ];
                }
                $userPermission = \DB::table('zzz_user_permisson')->insert($perData);
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return '["error","' . $e->getMessage() . '"]';
        }
        return [
            'success' => true,
            'user' => $user,
            'id' => $user->id,
            'listUserPermission' => $listUserPermission,
        ];
    }
    
    public function searchUser($request) {
        $listUser = new UserModel;
        $listUser = $listUser->where('username', 'like', "%{$request->keyword}%")
                ->orWhere('email', 'like', "%{$request->keyword}%")
                ->orWhere('fullName', 'like', "%{$request->keyword}%")
                ->orWhere('id', 'like', "%{$request->keyword}%")
                ->limit(20)
                ->orderBy('username', 'asc')
                ->get();
        $result = [];
        foreach ($listUser as $user) {
            $name = 'NV-' . $user->id;
            if ($user->fullName != '') {
                $name .= ' - ' . $user->fullName;
            }
            if ($user->username != '') {
                $name .= ' - ' . $user->username;
            }
            if ($user->email != '') {
                $name .= ' - ' . $user->email;
            }

            $result[] = [
                'id' => $user->id,
                'name' => $name
            ];
        }

        return $result;
    }
    
}

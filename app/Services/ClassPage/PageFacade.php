<?php
namespace App\Services\ClassPage;

use Illuminate\Support\Facades\Facade;

class PageFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassPage';
    }
}

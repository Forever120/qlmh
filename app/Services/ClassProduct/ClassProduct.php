<?php
namespace App\Services\ClassProduct;

use App\Http\Component\FormatDataComponent;
use App\Models\ImagesModel;
use App\Models\ProductModel;
use App\Models\ProductPriceModel;
use Illuminate\Support\Facades\Auth;

class ClassProduct
{
    public static function import2Excel($listProductID)
    {
    }

    public function postImport2Excel($request)
    {
        $user = Auth::user();
        $listUserPermission = \ClassUserPermisson::getListUserPermissonById($user->id);
        $listPrices = \ClassListPrice::index();
        //format list price
        $priceArr = [];
        foreach ($listPrices as $price) {
            $priceArr[$price->id] = FormatDataComponent::formatText($price->name, '_');
        }

        //get file import
        $file = $request->file('import');
        $filename = $file->getClientOriginalName();
        //get data on import file
        $excelData = \Excel::load($file, function ($reader) {
            $results = $reader->get();
        })->get();

        $categoryMapId2Name = \ClassCategory::getMapId2Name();
        $colorMapId2Name = \ClassColor::getMapId2Name();
        $materialMapId2Name = \ClassMaterial::getMapId2Name();
        try {
            \DB::beginTransaction();
            if (count($excelData) > 0) {
                foreach ($excelData as $data) {
                    //check data product
                    $product = new ProductModel;
                    if (isset($data->ten_san_pham)) {
                        $product->name = $data->ten_san_pham;
                    }
                    $product->categoryID = isset($categoryMapId2Name[$data->nhom_san_pham]) ? $categoryMapId2Name[$data->nhom_san_pham] : '';
                    $product->colorID = isset($colorMapId2Name[$data->mau_sac]) ? $colorMapId2Name[$data->mau_sac] : '';
                    $product->materialID = isset($materialMapId2Name[$data->chat_lieu]) ? $materialMapId2Name[$data->chat_lieu] : '';

                    $product->volume = $data->the_tich;
                    $product->weight = $data->khoi_luong;
                    $product->note = $data->ghi_chu;
                    $product->description = $data->mo_ta;
                    $product->save();

                    foreach ($priceArr as $priceID => $priceName) {
                        $productPrice = new ProductPriceModel;
                        if (!empty($data[$priceName])) {
                            $productPrice->productID = $product->id;
                            $productPrice->listPriceID = $priceID;
                            $productPrice->prodPrice = $data[$priceName];
                        }
                        $productPrice->save();
                    }
                }
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }

        return [
            'user' => $user,
            'listPrices' => $listPrices,
            'excelData' => $excelData,
            'priceArr' => $priceArr,
            'listUserPermission' => $listUserPermission,
            'message' => "Import thành công " . count($excelData) . " bản ghi, kết quả như bên dưới. ",
        ];
    }

    public static function dataExport2Excel($listProductID)
    {
        $data = [];
        $lstCate = \ClassCategory::getMapId2Name();
        $listColors = \ClassColor::getMapId2Name();
        $listMaterial = \ClassMaterial::getMapId2Name();
        $listProduct = ProductModel::active()
            ->whereIn('id', $listProductID)
            ->orderBy('id', 'desc')
            ->get();
        $productIds = [];
        foreach ($listProduct as $product) {
            $productIds[] = $product->id;
        }

        $listPrices = \ClassListPrice::index();
        $listPriceIds = [];
        $listPriceNames = [];
        foreach ($listPrices as $price) {
            $listPriceIds[] = $price->id;
            $listPriceNames[$price->id] = $price->name;
        }

        $mapProductId2ProductPrice = \ClassProductPrice::getMapProductId2ProductPrice($productIds, $listPriceIds);
        foreach ($listProduct as $product) {
            $body = [];
            $body['id'] = 'SP-' . $product->id;
            $body['name'] = $product->name;
            $body['categoryID'] = (isset($lstCate[$product->categoryID])) ? $lstCate[$product->categoryID] : '';
            $body['materialID'] = (isset($lstMaterial[$product->materialID])) ? $lstMaterial[$product->materialID] : '';
            $body['colorID'] = (isset($lstColor[$product->colorID])) ? $lstColor[$product->colorID] : '';
            $body['weight'] = $product->weight;
            $body['volume'] = $product->volume;

            $prodPrices = isset($mapProductId2ProductPrice[$product->id]) ? $mapProductId2ProductPrice[$product->id] : [];

            foreach ($listPriceIds as $priceId) {
                $price = '';
                foreach ($prodPrices as $pr) {
                    if ($pr->listPriceID == $priceId) {
                        $price = $pr->prodPrice;
                        break;
                    }
                }
                $body[$listPriceNames[$priceId]] = $price;
            }
            $body['description'] = $product->description;
            $body['note'] = $product->note;

            $data[] = $body;
        }
        return $data;
    }
    public static function getProduct($request)
    {
        $getRequest = [
            'category' => [],
            'color' => [],
            'material' => [],
            'pid' => '',
            'keyword' => '',
            'volume1' => '',
            'volume2' => '',
            'weight1' => '',
            'weight2' => '',
        ];
        //get all product
        $listProduct = ProductModel::active();

        if (isset($request->ngungKD) && $request->ngungKD == 1) {
            $listProduct = $listProduct->where('ngungKD', '=', 1);
        } else {
            $listProduct = $listProduct->where('ngungKD', '=', 0);
        }

        if (intval($request->weight2) > 0 && intval($request->weight2) >= intval($request->weight1)) {
            $listProduct = $listProduct->where('weight', '<', $request->weight2);
            $listProduct = $listProduct->where('weight', '>', $request->weight1);
            $getRequest['weight2'] = $request->weight2;
            $getRequest['weight1'] = $request->weight1;
        } elseif (intval($request->weight1) > 0) {
            $listProduct = $listProduct->where('weight', '=', $request->weight1);
            $getRequest['weight1'] = $request->weight1;
        }

        if (intval($request->volume2) > 0 && intval($request->volume2) >= intval($request->volume1)) {
            $listProduct = $listProduct->where('volume', '<', $request->volume2);
            $listProduct = $listProduct->where('volume', '>', $request->volume1);
            $getRequest['volume2'] = $request->volume2;
            $getRequest['volume1'] = $request->volume1;
        } elseif (intval($request->volume1) > 0) {
            $listProduct = $listProduct->where('volume', '=', $request->volume1);
            $getRequest['volume1'] = $request->volume1;
        }

        if (isset($request->category) && count($request->category) > 0) {
            $listProduct = $listProduct->whereIn('categoryID', $request->category);
            $getRequest['category'] = $request->category;
        }
        if (isset($request->color) && count($request->color) > 0) {
            $listProduct = $listProduct->whereIn('colorID', $request->color);
            $getRequest['color'] = $request->color;
        }
        if (isset($request->material) && count($request->material) > 0) {
            $listProduct = $listProduct->whereIn('materialID', $request->material);
            $getRequest['material'] = $request->material;
        }
        if (isset($request->keyword) && $request->keyword != '') {
            $listProduct = $listProduct->where('name', 'like', "%{$request->keyword}%");
            $getRequest['keyword'] = $request->keyword;
        }
        if (isset($request->pid) && $request->pid != '') {
            $listProduct = $listProduct->where('id', intval($request->pid));
            $getRequest['pid'] = $request->pid;
        }
        $listProduct = $listProduct->where('parentId', '0');
        $listProduct = $listProduct->orderBy('id', 'desc');
        $listProduct = $listProduct->paginate(\AppConst::PAGE_SIZE);

        $result = [
            'getRequest' => $getRequest,
            'listProduct' => $listProduct,
        ];

        return $result;
    }

    public function index($request, $listUserPermission)
    {
        $user = Auth::user();
        // Get product
        $getProductAndRequest = self::getProduct($request);
        $getRequest = $getProductAndRequest['getRequest'];
        $listProduct = $getProductAndRequest['listProduct'];
        $productIds = [];
        foreach ($listProduct as $product) {
            $productIds[] = $product->id;
        }

        //get all category
        $categores = \ClassCategory::getMapId2Name();

        //get all colors
        $colors = \ClassColor::getMapId2Name();

        //get all material
        $materials = \ClassMaterial::getMapId2Name();

        $listPrices = \ClassListPrice::index();
        $listPriceIds = [];
        foreach ($listPrices as $price) {
            $listPriceIds[] = $price->id;
        }

        $mapProductId2ProductPrice = \ClassProductPrice::getMapProductId2ProductPrice($productIds, $listPriceIds);
        //dd($mapProductId2ProductPrice);
        foreach ($listProduct as $product) {
            //add price to products array
            $prodPrices = isset($mapProductId2ProductPrice[$product->id]) ? $mapProductId2ProductPrice[$product->id] : [];
            $listPrice = [];
            $idx = 0;
            foreach ($listPriceIds as $listPriceId) {
                $productPrice = 'N/A';
                if (in_array('COLLUMN_' . $listPriceId, $listUserPermission)) {
                    $editPermission = 1;
                } else {
                    $editPermission = 0;
                }
                foreach ($prodPrices as $pr) {
                    if ($pr->listPriceID == $listPriceId) {
                        $productPrice = (isset($pr->prodPrice) && $pr->prodPrice != '') ? $pr->prodPrice : 'N/A';
                        break;
                    }
                }
                $listPrice[] = [
                    'listPriceId' => $listPriceId,
                    'prodPrice' => $productPrice,
                    'editPermission' => $editPermission,
                ];
            }
            $product['listPrice'] = $listPrice;

            //add images to products array
            $productImages = ImagesModel::where('productID', $product->id)->get();
            $product['images'] = $productImages;
        }

        $userName = $user->username;
        $listPriceActive = \ClassCommon::getUserListActive($userName, $listPriceIds, \AppConst::LIST_ACTIVE_PRICE_KEY);

        $data = [
            'user' => $user,
            'listProduct' => $listProduct,
            'listPrices' => $listPrices,
            'listPriceActive' => $listPriceActive,
            'lstCate' => $categores,
            'lstColor' => $colors,
            'lstMaterial' => $materials,
            'getRequest' => $getRequest,
            'listUserPermission' => $listUserPermission,
            'selected_menu' => [1],
        ];
        return $data;
    }

    public function findById($id)
    {
        $product = ProductModel::find($id);
        return $product;
    }

    public function findByName($name)
    {
        $products = ProductModel::where('name', 'like', "%{ $name }%")
            ->limit(\AppConst::PAGE_SIZE)
            ->orderBy('name', 'asc')
            ->get();
        return $products;
    }

    public function getMapId2NameByName($name)
    {
        $products = self::findByName($name);
        $mapId2Name = [];
        foreach ($products as $product) {
            $mapId2Name[] = [
                'id' => $product->id,
                'name' => $product->name,
            ];
        }
        return json_encode($mapId2Name);
    }

    public function edit($request, $id)
    {
        $parentID = (isset($request->parentID) && intval($request->parentID) > 0) ? $request->parentID : 0;

        $orgId = $id;
        if ($id == 0 && intval($request->pid) > 0) {
            $id = intval($request->pid);
        }
        $product = ProductModel::find(intval($id));
        //get all category
        $listCategory = \ClassCategory::index(null);
        //get all colors
        $listColors = \ClassColor::index(null);
        //get all material
        $listMaterial = \ClassMaterial::index(null);

        //get all images
        $images = \ClassImage::getByPid($id);

        $data = ['listCategory' => $listCategory,
            'listColors' => $listColors,
            'listMaterial' => $listMaterial,
            'product' => $product,
            'id' => $orgId,
            'images' => $images,
            'parentID' => $parentID,
        ];

        return $data;
    }
    public function store($request)
    {
        $product = new ProductModel;
        $result = self::save($product, $request);
        return $result;
    }
    public function update($request, $id)
    {
        $product = self::findById($id);
        $result = self::save($product, $request);
        return $result;
    }

    private function save($product, $request)
    {
        if (!isset($request->name) || $request->name == '') {
            return '["error","Bạn chưa nhập tên sản phẩm"]';
        }
        $user = Auth::user();

        if ($request->parentID > 0) {
            $product->parentID = $request->parentID;
        }
        $product->name = $request->name;
        $product->categoryID = $request->categoryID;
        $product->colorID = $request->colorID;
        $product->materialID = $request->materialID;
        $product->volume = $request->volume;
        $product->weight = $request->weight;
        $product->note = $request->note;
        $product->description = $request->description;
        $product->ngungKD = $request->ngungKD;
        $product->userID = $user->id;
        $product->save();

        // upload file
        $files = $request->file('images');
        if (count($files[0]) > 0) {
            ImagesModel::saveMultipleImage($files, $product->id);
        }
        return '["success"]';
    }

    public function delete($id)
    {
        $obj = ProductModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return 'success';
    }

    public function deleteListById($ids)
    {
        //dd($ids);
        try {
            \DB::beginTransaction();
            ProductModel::whereIn('id', $ids)
                ->update(['delFlg' => 1]);
            $images = ImagesModel::whereIn('productID', $ids)->get();
            if (count($images) > 0) {
                foreach ($images as $img) {
                    if (file_exists($img->path)) {
                        unlink($img->path);
                    }
                }
                $img->delete();
            }
            \DB::commit();
            return \AppConst::MSG_ERR_DELETE_SUCCESS;
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    public function getProductIDByName($request)
    {
        $product = new ProductModel;
        if ($request->type == 'id') {
            $product = $product->where('id', $request->value);
        } elseif ($request->type == 'name') {
            $product = $product->where('name', $request->value);
        }
        $product = $product->limit(20)
            ->orderBy('name', 'asc')
            ->first();

        if (count($product) > 0) {
            if ($request->type == 'id') {
                $result = $product->name;
            } elseif ($request->type == 'name') {
                $result = $product->id;
            }
        } else {
            $result = 0;
        }
        return $result;
    }
    public function subProduct($pid, $listUserPermission)
    {
        //get all product
        $user = Auth::user();

        $listProduct = ProductModel::active()
            ->where('parentId', $pid)
            ->orderBy('id', 'desc')
            ->paginate(\AppConst::PAGE_SIZE);
        $productIds = [];
        foreach ($listProduct as $product) {
            $productIds[] = $product->id;
        }

        $listPrices = \ClassListPrice::index();
        $listPriceIds = [];
        foreach ($listPrices as $price) {
            $listPriceIds[] = $price->id;
        }

        $mapProductId2ProductPrice = \ClassProductPrice::getMapProductId2ProductPrice($productIds, $listPriceIds);
        foreach ($listProduct as $product) {
            //add price to products array
            $prodPrices = isset($mapProductId2ProductPrice[$product->id]) ? $mapProductId2ProductPrice[$product->id] : [];
            $listPrice = [];
            $idx = 0;
            foreach ($listPriceIds as $listPriceId) {
                $productPrice = 'N/A';
                if (in_array('COLLUMN_' . $listPriceId, $listUserPermission)) {
                    $editPermission = 1;
                } else {
                    $editPermission = 0;
                }
                foreach ($prodPrices as $pr) {
                    if ($pr->listPriceID == $listPriceId) {
                        $productPrice = (isset($pr->prodPrice) && $pr->prodPrice != '') ? $pr->prodPrice : 'N/A';
                        break;
                    }
                }
                $listPrice[] = [
                    'listPriceId' => $listPriceId,
                    'prodPrice' => $productPrice,
                    'editPermission' => $editPermission,
                ];
            }
            $product['listPrice'] = $listPrice;

            //add images to products array
            $productImages = ImagesModel::where('productID', $product->id)->get();
            $product['images'] = $productImages;
        }

        $userName = $user->username;
        $listPriceActive = \ClassCommon::getUserListActive($userName, $listPriceIds, \AppConst::LIST_ACTIVE_PRICE_KEY);

        return [
            'listProduct' => $listProduct,
            'listPrices' => $listPrices,
            'listPriceActive' => $listPriceActive,
        ];
    }
}

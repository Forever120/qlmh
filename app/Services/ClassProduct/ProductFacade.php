<?php
namespace App\Services\ClassProduct;

use Illuminate\Support\Facades\Facade;

class ProductFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassProduct';
    }
}

<?php
namespace App\Services\ClassCategory;

use App\Models\CategoryModel;

class ClassCategory
{
    public function getMapId2Name()
    {
        $categorys = CategoryModel::active()->get();
        $mapId2Name = [];
        foreach ($categorys as $category) {
            $mapId2Name[$category->id] = $category->name;
        }
        return $mapId2Name;
    }

    public function index($request)
    {
        $categories = CategoryModel::active()->get();
        return $categories;
    }

    public function getAll()
    {
        $categories = CategoryModel::active()->get();
        return $categories;
    }

    public function findById($id)
    {
        $category = CategoryModel::find($id);
        return $category;
    }

    public function deleteById($id)
    {
        $obj = CategoryModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function htmlListCategory($parentID)
    {
        $list = CategoryModel::active()->where('parentID', $parentID)
            ->orderBy('NO', 'asc')
            ->get();
        $htmlList = '';
        if (count($list) > 0) {
            $htmlList .= '<ol class="dd-list">';
            foreach ($list as $item) {
                $htmlList .= '<li data-id="' . $item->id . '" class="dd-item">'
                . '<div class="option-menu">'
                . '<a onclick="loadPopupLarge(\'/category/edit/' . $item->id . '\')" data-toggle="modal" data-target=".bs-modal-lg"><i data-pack="default" class="ion-edit"></i></a>&nbsp;&nbsp;'
                . '<a onclick="deleteRow(\'/category/delete/' . $item->id . '\', \'/category/list\')"  tabindex="-1"><i data-pack="default" class="ion-trash-a"></i></a></div>'
                    . '<div class="card b0 dd-handle"><div class="mda-list">'
                    . '<div class="mda-list-item"><div class="mda-list-item-icon item-grab"><em class="ion-drag icon-lg"></em></div>'
                    . '<div class="mda-list-item-text mda-2-line">';
                $htmlList .= '<h4>' . $item->name . '</h4>';
                $htmlList .= '</div><div class="_right">'
                    . '</div></div></div></div>';
                $subMenu = CategoryModel::active()->where('parentID', $item->id)->count();
                if ($subMenu > 0) {
                    $htmlList .= self::htmlListCategory($item->id);
                }
                $htmlList .= "</li>";
            }
            $htmlList .= '</ol>';
        }
        return ($htmlList);
    }

    public function recursivelyUpdatePosition($listID, $parentId = 0)
    {
        $idx = 0;
        foreach ($listID as $item) {
            $idx++;
            $category = CategoryModel::find($item['id']);
            $category->NO = $idx;
            $category->parentID = $parentId;
            $category->save();
            if (isset($item['children'])) {
                self::recursivelyUpdatePosition($item['children'], $item['id']);
            }
        }
        return 'success';
    }

    public function postEditCategory($request, $id)
    {
        try {
            if ($id > 0) {
                $category = CategoryModel::find($id);
            } else {
                $category = new CategoryModel;
            }
            $category->name = $request->name;
            $category->NO = 0;
            $category->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return 'success';
    }

    public function editCategory($request, $id)
    {
        $user = \Auth::user();
        $category = self::findById(intval($id));

        return [
            'category' => $category,
            'id' => $id,
        ];
    }
}

<?php
namespace App\Services\ClassCategory;

use Illuminate\Support\Facades\Facade;

class CategoryFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassCategory';
    }
}

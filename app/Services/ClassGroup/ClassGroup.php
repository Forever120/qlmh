<?php
namespace App\Services\ClassGroup;

use App\Models\GroupModel;

class ClassGroup
{
    public function listGroup($listUserPermission)
    {
        $user = \Auth::user();
        $listGroup = GroupModel::active()
            ->orderBy('id', 'desc')
            ->paginate(10);
        $data = [
            'user' => $user,
            'listGroup' => $listGroup,
            'listUserPermission' => $listUserPermission,
            'selected_menu' => [8],
        ];
        return $data;
    }

    public function findById($id)
    {
        $group = GroupModel::find($id);
        return $group;
    }

    public function detailGroup($id)
    {
        $group = self::findById($id);
        $permissionUsing = [];
        if ($group != null) {
            $permissionUsing = json_decode($group->permission);
        }
        $listPrice = \ClassListPrice::getAll();
        return [
            'group' => $group,
            'listPrice' => $listPrice,
            'permissionUsing' => $permissionUsing,
        ];
    }

    public function editGroup($id)
    {
        //get detail Group infomation
        $group = self::findById($id);
        $permissionUsing = [];
        if ($group != null) {
            $permissionUsing = json_decode($group->permission);
        }
        //get list price
        $listPrice = \ClassListPrice::getAll();

        //get all permission
        $listPermission = \ClassUserPermisson::getListUserPermissonById($id);
        return [
            'group' => $group,
            'id' => $id,
            'listPrice' => $listPrice,
            'listPermission' => $listPermission,
            'permissionUsing' => $permissionUsing,
        ];
    }

    public function postEditGroup($request, $id)
    {
        //validation
        if (empty($request->name)) {
            return '["error","' . sprintf(\AppConst::MSG_ERR_EMPTY_FIELD, "nhóm") . '"]';
        }
        try {
            //save user
            $group = (intval($id) > 0) ? self::findById($id) : (new GroupModel);
            $group->name = $request->name;
            $group->permission = json_encode($request->permission);
            $group->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return '["success"]';
    }

    public function delete($id)
    {
        if ($id > 0) {
            $obj = GroupModel::getModelInstance();
            $ret = $obj->deleteById($id);
        }
        return 'success';
    }

    public function getPermissionById($id)
    {
        $group = self::findById($id);
        if ($group != null && $group->permission != '') {
            return $group->permission;
        }
        return '[]';
    }

    public function getAll()
    {
        $groups = GroupModel::active()
            ->get();
        return $groups;
    }
}

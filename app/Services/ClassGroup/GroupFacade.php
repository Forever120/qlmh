<?php
namespace App\Services\ClassGroup;

use Illuminate\Support\Facades\Facade;

class GroupFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassGroup';
    }
}

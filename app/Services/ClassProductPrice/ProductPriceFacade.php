<?php
namespace App\Services\ClassProductPrice;

use Illuminate\Support\Facades\Facade;

class ProductPriceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassProductPrice';
    }
}

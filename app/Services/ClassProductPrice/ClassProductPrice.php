<?php
namespace App\Services\ClassProductPrice;

use App\Models\ProductPriceModel;

class ClassProductPrice
{
    public function getMapProductId2ProductPrice($productIds, $listPriceIds)
    {
        $mapProductId2ProductPrice = [];
        $productPrices = ProductPriceModel::whereIn('productID', $productIds)
            ->whereIn('listPriceId', $listPriceIds)
            ->get();

        foreach ($productPrices as $productPrice) {
            $mapProductId2ProductPrice[$productPrice->productID][] = $productPrice;
        }
        return $mapProductId2ProductPrice;
    }

    public function findById($id)
    {
        $productPrice = ProductPriceModel::find($id);
        return $productPrice;
    }

    public function findByProductIdAndListPriceId($productId, $listPriceId)
    {
        $productPrice = ProductPriceModel::where([
            'productID' => $productId,
            'listPriceID' => $listPriceId,
        ])->first();
        return $productPrice;
    }

    public function update($data, $productId, $listPriceId)
    {
        try {
            $productPrice = self::findByProductIdAndListPriceId($productId, $listPriceId);
            if (null == $productPrice) {
                $productPrice = new ProductPriceModel;
                $productPrice->productID = $productId;
                $productPrice->listPriceID = $listPriceId;
            } else {
                $productPrice = self::findById($productPrice->id);
            }

            $productPrice->prodPrice = (isset($data['value']) && $data['value'] != '') ? $data['value'] : \AppConst::DATA_NOT_SETTED;
            $productPrice->save();
            return $productPrice;
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
}

<?php
namespace App\Services\ClassStatusSupplier;

use Illuminate\Support\Facades\Facade;

class StatusSupplierFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassStatusSupplier';
    }
}

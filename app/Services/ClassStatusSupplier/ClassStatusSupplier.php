<?php
namespace App\Services\ClassStatusSupplier;

use App\Models\StatusSupplierModel;

class ClassStatusSupplier
{
    public function index()
    {
        echo "Testing the Facades in Laravel.";
    }

    public function getAll()
    {
        $statuses = StatusSupplierModel::active()
            ->get();
        return $statuses;
    }

    public function mapId2Name()
    {
        $statuses = self::getAll();
        $mapId2Name = [];
        foreach ($statuses as $status) {
            $mapId2Name[$status->id] = $status->name;
        }
        return $mapId2Name;
    }
    public static function htmlList()
    {
        $list = StatusSupplierModel::active()->orderBy('sortOrder', 'asc')->get();
        $htmlList = '';
        if (count($list) > 0) {
            $htmlList .= '<ol class="dd-list">';
            foreach ($list as $item) {
                $htmlList .= '<li data-id="' . $item->id . '" class="dd-item">'
                . '<div class="option-menu">'
                . '<a onclick="loadPopupLarge(\'/status-supplier/edit/' . $item->id . '\')" data-toggle="modal" data-target=".bs-modal-lg"><i data-pack="default" class="ion-edit"></i></a>&nbsp;&nbsp;'
                . '<a onclick="deleteRow(\'/status-supplier/delete/' . $item->id . '\', \'/status-supplier/list\')"  tabindex="-1"><i data-pack="default" class="ion-trash-a"></i></a></div>'
                    . '<div class="card b0 dd-handle"><div class="mda-list">'
                    . '<div class="mda-list-item"><div class="mda-list-item-icon item-grab"><em class="ion-drag icon-lg"></em></div>'
                    . '<div class="mda-list-item-text mda-2-line">';
                $htmlList .= '<h4>' . $item->name . '</h4>';
                $htmlList .= '</div><div class="_right">'
                    . '</div></div></div></div>';
                $htmlList .= "</li>";
            }
            $htmlList .= '</ol>';
        }
        return ($htmlList);
    }

    public static function recursivelyUpdatePosition($listID, $parentId = 0)
    {
        $idx = 0;
        foreach ($listID as $item) {
            $idx++;
            $status = StatusSupplierModel::find($item['id']);
            $status->sortOrder = $idx;
            $status->save();
            if (isset($item['children'])) {
                statusSupplierComponent::recursivelyUpdatePosition($item['children'], $item['id']);
            }
        }
        return 'success';
    }

    public function findById($id)
    {
        $statusSupplier = StatusSupplierModel::find($id);
        return $statusSupplier;
    }

    public function deleteById($id)
    {
        $obj = StatusSupplierModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function listStatus($request)
    {
        $htmlList = self::htmlList();

        return [
            'htmlList' => $htmlList,

            'selected_menu' => [9, 94],
        ];
    }

    public function postEditStatus($request, $id)
    {
        try {
            $statusSupplier = ($id > 0) ? self::findById($id) : new StatusSupplierModel;
            $statusSupplier->name = $request->name;
            $statusSupplier->sortOrder = 0;
            $statusSupplier->save();
            return 'success';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function editStatus($request, $id)
    {
        $statusOrder = self::findById(intval($id));
        return [
            'detail' => $statusOrder,
            'id' => $id,
        ];
    }

    public function deleteStatus($id)
    {
        if ($id > 0) {
            StatusSupplier::find($id)->delete();
        }
        return 'success';
    }
}

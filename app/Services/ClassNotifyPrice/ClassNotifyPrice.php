<?php
namespace App\Services\ClassNotifyPrice;

use App\Models\ImagesModel;
use App\Models\NotifyPriceModel;
use App\Models\ProductModel;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;

class ClassNotifyPrice
{
    public function index($request, $listUserPermission)
    {
        $getRequest = [
            'category' => [],
            'color' => [],
            'material' => [],
            'pid' => '',
            'keyword' => '',
            'volume1' => '',
            'volume2' => '',
            'weight1' => '',
            'weight2' => '',
        ];
        //get all product
        $listNotifyPrice = DB::table('zzz_notify_price');
        $listNotifyPrice = $listNotifyPrice->select(array(
            'zzz_notify_price.id as npID',
            'zzz_notify_price.priceID',
            'zzz_notify_price.statusID',
            'zzz_notify_price.nvkdID',
            'zzz_notify_price.nvmhID',
            'zzz_notify_price.adminID',
            'zzz_notify_price.productID',
            'zzz_notify_price.countOrders',
            'zzz_notify_price.priceForNvkd',
            'zzz_notify_price.priceForNvmh',
            'zzz_notify_price.cloneNotifPriceID',
            'zzz_notify_price.notifyUserIDs',
            'zzz_notify_price.deadline',
            'zzz_notify_price.note as npnote',
            'zzz_notify_price.created_at as np_created_at',
            'zzz_product.*',
            'nvkd.fullname as nvkd_name',
            'nvmh.fullname as nvmh_name',
        ));

        $user = Auth::user();
        if (isset($request->category) && count($request->category) > 0) {
            $listNotifyPrice = $listNotifyPrice->whereIn('categoryID', $request->category);
            $getRequest['category'] = $request->category;
        }
        if (isset($request->color) && count($request->color) > 0) {
            $listNotifyPrice = $listNotifyPrice->whereIn('colorID', $request->color);
            $getRequest['color'] = $request->color;
        }
        if (isset($request->material) && count($request->material) > 0) {
            $listNotifyPrice = $listNotifyPrice->whereIn('materialID', $request->material);
            $getRequest['material'] = $request->material;
        }
        if (isset($request->keyword) && $request->keyword != '') {
            $listNotifyPrice = $listNotifyPrice->where('name', 'like', "%{$request->keyword}%");
            $getRequest['keyword'] = $request->keyword;
        }
        if (isset($request->pid) && $request->pid != '') {
            $listNotifyPrice = $listNotifyPrice->where('id', intval($request->pid));
            $getRequest['pid'] = $request->pid;
        }
        $listNotifyPrice = $listNotifyPrice->leftJoin('zzz_product', 'zzz_notify_price.productID', '=', 'zzz_product.id');
        $listNotifyPrice = $listNotifyPrice->leftJoin('zzz_user as nvkd', 'zzz_notify_price.nvkdID', '=', 'nvkd.id');
        $listNotifyPrice = $listNotifyPrice->leftJoin('zzz_user as nvmh', 'zzz_notify_price.nvmhID', '=', 'nvmh.id');
        $listNotifyPrice = $listNotifyPrice->orderBy('npID', 'desc');
        $listNotifyPrice = $listNotifyPrice->paginate(\AppConst::PAGE_SIZE);
        //get all category
        $listCategory = \ClassCategory::getMapId2Name();
        //get all colors
        $listColors = \ClassColor::getMapId2Name();
        //get all material
        $listMaterial = \ClassMaterial::getMapId2Name();

        //get all material
        $status = \ClassStatus::index();
        //check collumn use
        $userName = Auth::user()->username;
        $listNotifyPriceActive = \ClassCommon::getUserListActive($userName, null, \AppConst::LIST_ACTIVE_NOTIFY_PRICE_KEY);
        $listNotifyPriceActiveTmp = \ClassCommon::getUserListNotifyPriceActiveTmp();

        $data = [
            'user' => $user,
            'listNotifyPrice' => $listNotifyPrice,
            'listNotifyPriceActiveTmp' => $listNotifyPriceActiveTmp,
            'listNotifyPriceActive' => $listNotifyPriceActive,
            'lstCate' => $listCategory,
            'lstColor' => $listColors,
            'lstMaterial' => $listMaterial,
            'getRequest' => $getRequest,
            'listUserPermission' => $listUserPermission,
            'selected_menu' => [2],
            'status' => $status,
        ];

        return $data;
    }

    public function findById($id)
    {
        $notifyPrice = NotifyPriceModel::find($id);
        return $notifyPrice;
    }

    public function edit($request, $id)
    {
        $tmpNid = ($id == 0 && intval($request->pid) > 0) ? intval($request->pid) : $id;

        //get detail notify
        $pid = 0;
        if ($tmpNid > 0) {
            $notifyPrice = self::findById(intval($tmpNid));
            $pid = $notifyPrice->productID;
            $user = User::find(intval($notifyPrice->nvkdID));
        } else {
            $user = Auth::user();
            $notifyPrice = new NotifyPriceModel;
            $notifyPrice->statusID = 1; // Default status when create new notify price is: "Yêu cầu kiểm tra giá"
        }

        //get detail product
        $product = \ClassProduct::findById(intval($pid));
        //get all category
        $listCategory = \ClassCategory::index(null);
        //get all colors
        $listColors = \ClassColor::index(null);
        //get all material
        $status = \ClassStatus::index(null);
        //get all material
        $listMaterial = \ClassMaterial::index(null);
        //get all images
        $images = \ClassImage::getByPid($pid);

        $data = ['listCategory' => $listCategory,
            'listColors' => $listColors,
            'listMaterial' => $listMaterial,
            'product' => $product,
            'id' => $id,
            'images' => $images,
            'notifyPrice' => $notifyPrice,
            'user' => $user,
            'status' => $status,
        ];

        return $data;
    }

    public function store()
    {
    }

    public function update()
    {
    }

    public function storeOrUpdate($request, $nid)
    {
        // print_r($request->all());
        if ($request->deadline == '') {
            return '["error","Không được bỏ trống deadline"]';
        }
        if ($request->statusID == '') {
            return '["error","Bạn chưa chọn trạng thái cho báo giá"]';
        }
        if ($request->name == '') {
            return '["error","Tên sản phẩm không được bỏ trống"]';
        }

        if ($nid > 0) {
            $notifyPrice = NotifyPriceModel::find(intval($nid));
            $pid = $notifyPrice->productID;
            //get user create
            $user = User::find(intval($notifyPrice->nvkdID));
        } else {
            $pid = 0;
            $user = Auth::user();
            $notifyPrice = new NotifyPriceModel;
        }
        //insert product
        $product = ProductModel::saveProduct($request, $pid, $user->id);
        // upload and insert images
        $files = $request->file('images');
        if (count($files[0]) > 0) {
            ImagesModel::saveMultipleImage($files, $product->id);
        }
        //
        $thanhphan = [];
        if (count($request->collumn) > 0) {
            $thanhphan['listCollumn'] = $request->collumn;
            $countCollumn = count($request->collumn);
            $thanhphan['row'] = [];
            $idxRow = 0;
            foreach ($request->row as $row) {
                if ($idxRow == 0) {
                    $subRow = [];
                    $subRow[] = $row;
                    $idxRow++;
                } elseif ($idxRow > 0 && $idxRow < $countCollumn) {
                    $subRow[] = $row;
                    $idxRow++;
                } elseif ($idxRow == $countCollumn) {
                    $subRow[] = $row;
                    $thanhphan['row'][] = $subRow;
                    $idxRow = 0;
                }
            }
        }
        $thanhphan_json = json_encode($thanhphan);

        //insert productPrice
        $notifyPrice->statusID = $request->statusID;
        $notifyPrice->nvkdID = $user->id;
        $notifyPrice->deadline = $request->deadline;
        $notifyPrice->note = $request->noteNotify;
        $notifyPrice->productID = $product->id;
        $notifyPrice->countOrders = $thanhphan_json;
        $notifyPrice->save();
        return '["success"]';
    }
}

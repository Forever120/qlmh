<?php
namespace App\Services\ClassNotifyPrice;

use Illuminate\Support\Facades\Facade;

class NotifyPriceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassNotifyPrice';
    }
}

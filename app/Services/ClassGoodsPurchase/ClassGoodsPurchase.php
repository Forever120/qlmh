<?php

namespace App\Services\ClassGoodsPurchase;

use App\Models\GoodsOrdersModel;
use App\Models\GoodsPurchaseModel;
use App\Models\ProductsOrdersModel;
use App\Models\ProductsPurchaseModel;
use App\Models\StatusOrdersModel;
use App\Models\SupplierOrdersModel;

class ClassGoodsPurchase
{

    public function findById($id)
    {
        $goodsPurchase = GoodsPurchaseModel::find(intval($id));
        return $goodsPurchase;
    }

    public function deleteById($id)
    {
        $obj = GoodsPurchaseModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function index($request, $listUserPermission, $user)
    {

        $listGoodsPuchase = GoodsPurchaseModel::active();
        $listGoodsPuchase = $listGoodsPuchase->select([
            'zzz_goods_purchase.id as gpID',
            'zzz_goods_purchase.*',
            'zzz_supplier.name as sName',
            'zzz_user.fullname as uFullname',
            'zzz_user.fullname as uFullname',
        ]);
        $listGoodsPuchase = $listGoodsPuchase->leftJoin('zzz_supplier', 'zzz_goods_purchase.supplierID', '=', 'zzz_supplier.id');
        $listGoodsPuchase = $listGoodsPuchase->leftJoin('zzz_user', 'zzz_goods_purchase.nvmhID', '=', 'zzz_user.id');
        $listGoodsPuchase = $listGoodsPuchase->paginate(\AppConst::PAGE_SIZE);

        $pagination = \ClassCommon::paginationAjax($listGoodsPuchase, '.list-goods-purchase', '/goods-purchase/list', $request);

        $data = [
            'listUserPermission' => $listUserPermission,
            'user' => $user,
            'pagination' => $pagination,
            'listGoodsPuchase' => $listGoodsPuchase,
            'selected_menu' => [10],
        ];
        return $data;
    }

    public function lumpedGoodsOrders($listGoodsOrderID)
    {
        $listStatusOrder = StatusOrdersModel::active()->orderBy('sortOrder', 'asc')->get();
        $listProductsOrders = ProductsOrdersModel::active()
            ->select([
                'zzz_products_orders.id as poID',
                'zzz_products_orders.countProduct',
                'zzz_products_orders.CountPackage',
                'zzz_products_orders.countProductToPackage',
                'zzz_products_orders.price',
                'zzz_products_orders.goodsOrdersID',
                'zzz_products_orders.productID',
                'zzz_product.name as pName',
            ])
            ->whereIn('goodsOrdersID', $listGoodsOrderID)
            ->leftJoin('zzz_product', 'zzz_products_orders.productID', '=', 'zzz_product.id')
            ->orderBy('goodsOrdersID', 'asc')
            ->get();
        // Map productID to Product Order
        $mapPIdToLumpedProduct = [];
        foreach ($listProductsOrders as $value) {
            if (empty($mapPIdToLumpedProduct[$value->productID])) {
                $mapPIdToLumpedProduct[$value->productID]['countProduct'] = $value->countProduct;
                $mapPIdToLumpedProduct[$value->productID]['price'] = $value->price;
                $mapPIdToLumpedProduct[$value->productID]['pName'] = $value->pName;
                $mapPIdToLumpedProduct[$value->productID]['productID'] = $value->productID;
            } else {
                $mapPIdToLumpedProduct[$value->productID]['countProduct'] += $value->countProduct;
                $mapPIdToLumpedProduct[$value->productID]['price'] += $value->price;
            }
        }

        $listSupplierOrders = SupplierOrdersModel::active()
            ->distinct()
            ->select([
                'zzz_supplier_orders.id as soID',
                'zzz_supplier_orders.supplierID',
                'zzz_supplier.name as sName',
            ])
            ->whereIn('goodsOrdersID', $listGoodsOrderID)
            ->leftJoin('zzz_supplier', 'zzz_supplier_orders.supplierID', '=', 'zzz_supplier.id')
            ->groupBy('zzz_supplier.id')
            ->orderBy('goodsOrdersID', 'asc')
            ->get();

        $data = [
            'listGoodsOrderID' => $listGoodsOrderID,
            'listStatusOrder' => $listStatusOrder,
            'listProductsOrders' => $listProductsOrders,
            'listSupplierOrders' => $listSupplierOrders,
            'mapPIdToLumpedProduct' => $mapPIdToLumpedProduct,
        ];
//        echo "<pre>";
        //        print_r($mapPIdToLumpedProduct);

        return $data;
    }

    public function postLumpedGoodsOrders($request, $id = 0)
    {

        try {
            $user = \Auth::user();
            $supplierID = 0;
            if (!empty($request->supplierID)) {
                $supplierID = $request->supplierID;
            } else {
                if (!empty($request->supplierIDOther)) {
                    $supplierID = intval($request->supplierIDOther);
                }
            }
            if ($supplierID <= 0) {
                return [
                    '0' => 'false',
                    '1' => 'Bạn chưa chọn Nhà cung cấp.' . $request->supplierID,
                ];
            }

            if (empty($request->picValue)) {
                return [
                    '0' => 'false',
                    '1' => 'Bạn chưa chọn nhân viên mua hàng.',
                ];
            }
            \DB::beginTransaction();
            if (empty($id)) {
                $goodsPurchase = new GoodsPurchaseModel;
            } else {
                $goodsPurchase = self::findById($id);
            }

            $goodsPurchase->statusID = $request->statusID;
            $goodsPurchase->userID = $user->id;
            $goodsPurchase->nvmhID = $request->picID;
            $goodsPurchase->supplierID = $supplierID;
            $goodsPurchase->price = $request->price;
            $goodsPurchase->currencyUnit = $request->currencyUnit;
            $goodsPurchase->exchangeMoney = empty($request->exchangeMoney) ? 0 : $request->exchangeMoney;
            if (empty($id)) {
                $goodsPurchase->desposits = $goodsPurchase->exchangeMoney;
            } else {
                $goodsPurchase->desposits += $goodsPurchase->exchangeMoney;
            }
            $goodsPurchase->remain = $goodsPurchase->price - $goodsPurchase->desposits;
            $goodsPurchase->orderDate = $request->orderDate;
            $goodsPurchase->produceDate = $request->produceDate;
            $goodsPurchase->deliverDate = $request->deliverDate;
            $goodsPurchase->arriveDate = $request->arriveDate;
            $goodsPurchase->note = $request->note;
            $goodsPurchase->save();
            //upload file & save to GoodsPurchase
            $destinationFilePath = 'data/goodsPuchase/' . $goodsPurchase->id;
            if (!is_dir($destinationFilePath)) {
                mkdir($destinationFilePath, 750, true);
            }

            if (!empty($request->invoiceFilePath)) {
                $invoiceFileName = $request->invoiceFilePath->getClientOriginalName();
                $invoiceFilePath = $destinationFilePath . '/' . $invoiceFileName;
                $request->invoiceFilePath->move($destinationFilePath, $invoiceFileName);

                $goodsPurchase->invoiceFilePath = $invoiceFilePath;
            }

            if (!empty($request->packingListFilePath)) {
                $packingListFileName = $request->packingListFilePath->getClientOriginalName();
                $packingListFilePath = $destinationFilePath . '/' . $packingListFileName;
                $request->packingListFilePath->move($destinationFilePath, $packingListFileName);

                $goodsPurchase->packingListFilePath = $packingListFilePath;
            }

            if (!empty($request->billOfLadingFilePath)) {
                $billceFileName = $request->billOfLadingFilePath->getClientOriginalName();
                $billceFilePath = $destinationFilePath . '/' . $billceFileName;
                $request->billOfLadingFilePath->move($destinationFilePath, $billceFileName);

                $goodsPurchase->billOfLadingFilePath = $billceFilePath;
            }

            $goodsPurchase->save();

            // Insert good purchase
            if (empty($request->productIDs) || empty($request->countProducts) || empty($request->prices ||
                count($request->productIDs) != count($request->countProducts) || count($request->countProducts) != count($request->prices))) {

                return [
                    '0' => 'false',
                    '1' => 'Danh sách mã sản phẩm, số lượng sản phẩm, giá sản phẩm phải tồn tại và bằng nhau',
                ];
            }

            $count = 0;
            $numOfProduct = count($request->productIDs);
            $dataGoodsPurchases = [];
            for ($count = 0; $count < $numOfProduct; ++$count) {
                $data = [];
                $data['goodsPurchaseID'] = $goodsPurchase->id;
                $data['productID'] = $request->productIDs[$count];
                $data['countProduct'] = $request->countProducts[$count];
                $data['price'] = $request->prices[$count];

                $dataGoodsPurchases[] = $data;
            }
            if (empty($id)) {
                ProductsPurchaseModel::insert($dataGoodsPurchases);
                $result = \ClassSupplier::addNewGoodPurchaseToDebit($supplierID, $goodsPurchase->id, $goodsPurchase->exchangeMoney, $goodsPurchase->price);
                $goodsOrderIDs = $request->goodsOrderIDs;
                $isGoodOrderLumped = \ClassGoodsOrder::isGoodsOrdersLumped($goodsOrderIDs);
                if ($isGoodOrderLumped) {
                    return [
                        '0' => 'false',
                        '1' => 'Đơn mua hàng này đã được gộp',
                    ];
                }
                \ClassGoodsOrder::updateGoodsOrderToLumpedByGoIds($goodsPurchase->id, $goodsOrderIDs);
            } else {
                // var_dump($dataGoodsPurchases);
                // die;
                foreach ($dataGoodsPurchases as $gp) {
                    $ret = ProductsPurchaseModel::active()
                        ->where('goodsPurchaseID', '=', $gp['goodsPurchaseID'])
                        ->where('productID', '=', $gp['productID'])
                        ->Update(['countProduct' => $gp['countProduct'],
                            'price' => $gp['price']]);
                }
                $result = \ClassSupplier::addNewGoodPurchaseToDebit($supplierID, $goodsPurchase->id, $goodsPurchase->exchangeMoney, $goodsPurchase->price);

            }

            // Update GoodsOrders

            \DB::commit();
            return [
                '0' => 'success',
                '1' => 'insert thành công',
            ];
        } catch (\Exception $e) {
            \DB::rollback();
            return [
                '0' => 'error',
                '1' => $e->getMessage(),
            ];
        }
    }

    public function getListProduct($request, $goodsPuchaseID)
    {

        $productsPurchase = ProductsPurchaseModel::active()
            ->select([
                'zzz_products_purchase.id as ppID',
                'zzz_products_purchase.productID',
                'zzz_products_purchase.countProduct',
                'zzz_products_purchase.CountPackage',
                'zzz_products_purchase.countProductToPackage',
                'zzz_products_purchase.price',
                'zzz_products_purchase.goodsPurchaseID',
                'zzz_product.name as pName',
            ])
            ->where('goodsPurchaseID', '=', $goodsPuchaseID)
            ->leftJoin('zzz_product', 'zzz_products_purchase.productID', '=', 'zzz_product.id')
            ->get();

        $data = [
            'productsPurchase' => $productsPurchase,
        ];
        return $data;
    }

    public function getListGoodsOrdersID($goodsPuchaseID)
    {
        $goodsPuchase = GoodsOrdersModel::active()->where('goodsPurchaseID', $goodsPuchaseID)->get();
        $lstGoodsOrderID = [];
        foreach ($goodsPuchase as $puchase) {
            $lstGoodsOrderID[] = $puchase->id;
        }
        return $lstGoodsOrderID;
    }
    public function mapId2ProductPurchase($gpId)
    {
        $productsPurchases = ProductsPurchaseModel::active()
            ->select([
                'zzz_products_purchase.id as ppID',
                'zzz_products_purchase.countProduct',
                'zzz_products_purchase.CountPackage',
                'zzz_products_purchase.countProductToPackage',
                'zzz_products_purchase.price',
                'zzz_products_purchase.goodsPurchaseID',
                'zzz_products_purchase.productID',
                'zzz_product.name as pName',
            ])
            ->where('goodsPurchaseID', '=', $gpId)
            ->leftJoin('zzz_product', 'zzz_products_purchase.productID', '=', 'zzz_product.id')
            ->get();
        $mapId2ProductPurchase = [];
        foreach ($productsPurchases as $value) {
            $mapId2ProductPurchase[$value->productID]['countProduct'] = $value->countProduct;
            $mapId2ProductPurchase[$value->productID]['price'] = $value->price;
            $mapId2ProductPurchase[$value->productID]['pName'] = $value->pName;
            $mapId2ProductPurchase[$value->productID]['productID'] = $value->productID;
        }
        return $mapId2ProductPurchase;
    }
}

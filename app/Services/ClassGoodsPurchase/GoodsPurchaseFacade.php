<?php
namespace App\Services\ClassGoodsPurchase;

use Illuminate\Support\Facades\Facade;

class GoodsPurchaseFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassGoodsPurchase';
    }
}

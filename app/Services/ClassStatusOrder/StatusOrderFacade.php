<?php
namespace App\Services\ClassStatusOrder;

use Illuminate\Support\Facades\Facade;

class StatusOrderFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassStatusOrder';
    }
}

<?php
namespace App\Services\ClassStatusOrder;

use App\Models\StatusOrdersModel;

class ClassStatusOrder
{
    public function index()
    {
        echo "Testing the Facades in Laravel.";
    }

    public static function htmlList()
    {
        $list = StatusOrdersModel::active()
            ->orderBy('sortOrder', 'asc')
            ->get();
        $htmlList = '';
        if (count($list) > 0) {
            $htmlList .= '<ol class="dd-list">';
            foreach ($list as $item) {
                $htmlList .= '<li data-id="' . $item->id . '" class="dd-item">'
                . '<div class="option-menu">'
                . '<a onclick="loadPopupLarge(\'/status-orders/edit/' . $item->id . '\')" data-toggle="modal" data-target=".bs-modal-lg"><i data-pack="default" class="ion-edit"></i></a>&nbsp;&nbsp;'
                . '<a onclick="deleteRow(\'/status-orders/delete/' . $item->id . '\', \'/status-orders/list\')"  tabindex="-1"><i data-pack="default" class="ion-trash-a"></i></a></div>'
                    . '<div class="card b0 dd-handle"><div class="mda-list">'
                    . '<div class="mda-list-item"><div class="mda-list-item-icon item-grab"><em class="ion-drag icon-lg"></em></div>'
                    . '<div class="mda-list-item-text mda-2-line">';
                $htmlList .= '<h4>' . $item->name . '</h4>';
                $htmlList .= '</div><div class="_right">'
                    . '</div></div></div></div>';
                $htmlList .= "</li>";
            }
            $htmlList .= '</ol>';
        }
        return ($htmlList);
    }

    public static function recursivelyUpdatePosition($listID, $parentId = 0)
    {
        $idx = 0;
        foreach ($listID as $item) {
            $idx++;
            $status = StatusOrdersModel::find($item['id']);
            $status->sortOrder = $idx;
            $status->save();
            if (isset($item['children'])) {
                StatusOrderComponent::recursivelyUpdatePosition($item['children'], $item['id']);
            }
        }
        return 'success';
    }

    public function listStatus($request)
    {

        $htmlList = self::htmlList();

        return [
            'htmlList' => $htmlList,

            'selected_menu' => [9, 95],
        ];
    }

    public function findById($id)
    {
        $statusOrder = StatusOrdersModel::find($id);
        return $statusOrder;
    }

    public function deleteById($id)
    {
        $obj = StatusOrdersModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function postEditStatus($request, $id)
    {
        try {
            $statusOrder = ($id > 0) ? self::findById($id) : new StatusOrdersModel;
            $statusOrder->name = $request->name;
            $statusOrder->sortOrder = 0;
            $statusOrder->save();
            return 'success';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function editStatus($request, $id)
    {
        $user = \Auth::user();
        $statusOrder = self::findById(intval($id));
        return [
            'detail' => $statusOrder,
            'id' => $id,
        ];
    }

    public function updatePosition($request)
    {
        try {
            if (isset($request->json)) {
                $json_arr = json_decode($request->json, true);
                $result = self::recursivelyUpdatePosition($json_arr);
                if ($result == 'success') {
                    return 'Update success';
                } else {
                    return 'Update error';
                }
            }
            return 'Nothing to update';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getAll()
    {
        $statusOders = StatusOrdersModel::active()->get();
        return $statusOders;
    }
}

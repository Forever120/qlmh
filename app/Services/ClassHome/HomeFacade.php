<?php
namespace App\Services\ClassHome;

use Illuminate\Support\Facades\Facade;

class HomeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassHome';
    }
}

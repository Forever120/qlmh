<?php
namespace App\Services\ClassStatusPrice;

use App\Models\StatusModel;

class ClassStatusPrice
{
    public function index()
    {
        echo "Testing the Facades in Laravel.";
    }
    public static function htmlList()
    {
        $list = StatusModel::active()->orderBy('sortOrder', 'asc')->get();
        $htmlList = '';
        if (count($list) > 0) {
            $htmlList .= '<ol class="dd-list">';
            foreach ($list as $item) {
                $htmlList .= '<li data-id="' . $item->id . '" class="dd-item">'
                . '<div class="option-menu">'
                . '<a onclick="loadPopupLarge(\'/status-price/edit/' . $item->id . '\')" data-toggle="modal" data-target=".bs-modal-lg"><i data-pack="default" class="ion-edit"></i></a>&nbsp;&nbsp;'
                . '<a onclick="deleteRow(\'/status-price/delete/' . $item->id . '\', \'/status-price/list\')"  tabindex="-1"><i data-pack="default" class="ion-trash-a"></i></a></div>'
                    . '<div class="card b0 dd-handle"><div class="mda-list">'
                    . '<div class="mda-list-item"><div class="mda-list-item-icon item-grab"><em class="ion-drag icon-lg"></em></div>'
                    . '<div class="mda-list-item-text mda-2-line">';
                $htmlList .= '<h4>' . $item->name . '</h4>';
                $htmlList .= '</div><div class="_right">'
                    . '</div></div></div></div>';
                $htmlList .= "</li>";
            }
            $htmlList .= '</ol>';
        }
        return ($htmlList);
    }

    public static function recursivelyUpdatePosition($listID, $parentId = 0)
    {
        $idx = 0;
        foreach ($listID as $item) {
            $idx++;
            $status = StatusModel::find($item['id']);
            $status->sortOrder = $idx;
            $status->save();
            if (isset($item['children'])) {
                statusPriceComponent::recursivelyUpdatePosition($item['children'], $item['id']);
            }
        }
        return 'success';
    }

    public function findById($id)
    {
        $status = StatusModel::find($id);
        return $status;
    }

    public function deleteById($id)
    {
        $obj = StatusModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function listStatus($request)
    {
        $htmlList = self::htmlList();
        return [
            'htmlList' => $htmlList,
            'selected_menu' => [9, 96],
        ];
    }

    public function postEditStatus($request, $id)
    {
        try {
            $status = ($id > 0) ? self::findById($id) : new StatusModel;
            $status->name = $request->name;
            $status->sortOrder = 0;
            $status->save();
            return 'success';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function editStatus($request, $id)
    {
        $status = self::findById(intval($id));
        return [
            'detail' => $status,
            'id' => $id,
        ];
    }
}

<?php
namespace App\Services\ClassStatusPrice;

use Illuminate\Support\Facades\Facade;

class StatusPriceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassStatusPrice';
    }
}

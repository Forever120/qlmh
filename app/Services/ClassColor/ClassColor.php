<?php
namespace App\Services\ClassColor;

use App\Models\ColorModel;

class ClassColor
{
    public function getMapId2Name()
    {
        $colors = ColorModel::active()->get();
        $mapId2Name = [];
        foreach ($colors as $color) {
            $mapId2Name[$color->id] = $color->name;
        }
        return $mapId2Name;
    }

    public function index($request)
    {
        $colors = ColorModel::active()->get();
        return $colors;
    }

    public function findById($id)
    {
        $color = ColorModel::find($id);
        return $color;
    }

    public function deleteById($id)
    {
        $obj = ColorModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function htmlListColor()
    {
        $list = ColorModel::active()->orderBy('NO', 'asc')->get();
        $htmlList = '';
        if (count($list) > 0) {
            $htmlList .= '<ol class="dd-list">';
            foreach ($list as $item) {
                $htmlList .= '<li data-id="' . $item->id . '" class="dd-item">'
                . '<div class="option-menu">'
                . '<a onclick="loadPopupLarge(\'/color/edit/' . $item->id . '\')" data-toggle="modal" data-target=".bs-modal-lg"><i data-pack="default" class="ion-edit"></i></a>&nbsp;&nbsp;'
                . '<a onclick="deleteRow(\'/color/delete/' . $item->id . '\', \'/color/list\')"  tabindex="-1"><i data-pack="default" class="ion-trash-a"></i></a></div>'
                    . '<div class="card b0 dd-handle"><div class="mda-list">'
                    . '<div class="mda-list-item"><div class="mda-list-item-icon item-grab"><em class="ion-drag icon-lg"></em></div>'
                    . '<div class="mda-list-item-text mda-2-line">';
                $htmlList .= '<h4>' . $item->name . '</h4>';
                $htmlList .= '</div><div class="_right">'
                    . '</div></div></div></div>';
                $htmlList .= "</li>";
            }
            $htmlList .= '</ol>';
        }
        return ($htmlList);
    }

    public function recursivelyUpdatePosition($listColorID, $parentId = 0)
    {
        $idx = 0;
        foreach ($listColorID as $item) {
            $idx++;
            $color = ColorModel::find($item['id']);
            $color->NO = $idx;
            $color->save();
            if (isset($item['children'])) {
                self::recursivelyUpdatePosition($item['children'], $item['id']);
            }
        }
        return 'success';
    }

    public function editColor($request, $id)
    {
        $user = \Auth::user();
        $color = self::findById(intval($id));

        return ['color' => $color,
            'id' => $id,
        ];
    }

    public function postEditColor($request, $id)
    {
        try {
            $color = ($id > 0) ? self::findById($id) : new ColorModel;
            $color->name = $request->name;
            $color->NO = 0;
            $color->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return 'success';
    }
}

<?php
namespace App\Services\ClassColor;

use Illuminate\Support\Facades\Facade;

class ColorFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassColor';
    }
}

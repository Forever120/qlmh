<?php
namespace App\Services\ClassStatus;

use Illuminate\Support\Facades\Facade;

class StatusFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassStatus';
    }
}

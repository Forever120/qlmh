<?php
namespace App\Services\ClassUserPermisson;

use Illuminate\Support\Facades\Facade;

class UserPermissonFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassUserPermisson';
    }
}

<?php
namespace App\Services\ClassUserPermisson;

use App\Models\UserPermissonModel;
use Illuminate\Support\Facades\Auth;

class ClassUserPermisson
{
    private $listUserPermissions = [];
    public function getListUserPermissonById($id)
    {
        if (!empty($this->listUserPermissions[$id])) {
            return $this->listUserPermissions[$id];
        }
        $listUserPermission = [];
        $userPermission = UserPermissonModel::active()->where('userID', '=', $id)->get();
        foreach ($userPermission as $per) {
            $listUserPermission[] = $per->permissionID;
        }
        $this->listUserPermissions[$id] = $listUserPermission;
        return $listUserPermission;
    }

    // Check User Permisson
    public function checkUserPermisson($permissons)
    {
        $user = \Auth::user();
        $listUserPermission = self::getListUserPermissonById($user->id);
        //check view permission
        if (is_array($permissons)) {
            foreach ($permissons as $permisson) {
                if (!in_array($permisson, $listUserPermission)) {
                    return false;
                }
            }
        }

        if (!in_array($permissons, $listUserPermission)) {
            return false;
        }
        return $listUserPermission;
    }
}

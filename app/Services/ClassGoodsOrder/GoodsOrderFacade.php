<?php
namespace App\Services\ClassGoodsOrder;

use Illuminate\Support\Facades\Facade;

class GoodsOrderFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassGoodsOrder';
    }
}

<?php

namespace App\Services\ClassGoodsOrder;

use App\Models\CustomerModel;
use App\Models\GoodsOrdersModel;
use App\Models\ProductsOrdersModel;
use App\Models\StatusOrdersModel;
use App\Models\SupplierOrdersModel;

class ClassGoodsOrder
{

    public function findById($id)
    {
        $goodsOrder = GoodsOrdersModel::find($id);
        return $goodsOrder;
    }

    public function findSupplierOrdersById($id)
    {
        $supplierOrder = SupplierOrdersModel::find(intval($id));
        return $supplierOrder;
    }

    public function findProductOrderById($id)
    {
        $productOrder = ProductsOrdersModel::find(intval($id));
        return $productOrder;
    }

    public function deleteById($id)
    {
        $obj = GoodsOrdersModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function deleteSupplierOrders($id)
    {
        $obj = SupplierOrdersModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function deleteProductOrder($id)
    {
        $obj = ProductsOrdersModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function checkProductExistInOrder($goId, $pId)
    {
        $countOrders = ProductsOrdersModel::active()
            ->where('goodsOrdersID', '=', intval($goId))
            ->where('productID', '=', intval($pId))
            ->count();
        return ($countOrders > 0);
    }

    public function checkSupplierExistInOrder($goId, $sId)
    {
        $countOrders = SupplierOrdersModel::active()
            ->where('goodsOrdersID', '=', intval($goId))
            ->where('supplierID', '=', intval($sId))
            ->count();
        return ($countOrders > 0);
    }

    public function index($request, $listUserPermission, $user)
    {
        $lstGoodsOrders = GoodsOrdersModel::active();

        if (!empty($request->suplierDeliveryDate1)) {
            $lstGoodsOrders = $lstGoodsOrders->where('zzz_goods_orders.suplierDeliveryDate', '>=', $request->suplierDeliveryDate1);
        }
        if (!empty($request->suplierDeliveryDate2)) {
            $lstGoodsOrders = $lstGoodsOrders->where('zzz_goods_orders.suplierDeliveryDate', '<=', $request->suplierDeliveryDate2);
        }

        if (!empty($request->deliveryToUserDate1)) {
            $lstGoodsOrders = $lstGoodsOrders->where('zzz_goods_orders.deliveryToUserDate', '>=', $request->deliveryToUserDate1);
        }
        if (!empty($request->deliveryToUserDate2)) {
            $lstGoodsOrders = $lstGoodsOrders->where('zzz_goods_orders.deliveryToUserDate', '>=', $request->deliveryToUserDate2);
        }

        if (!empty($request->statusID)) {
            $lstGoodsOrders = $lstGoodsOrders->where('zzz_goods_orders.statusID', '=', $request->statusID);
        }
        if (!empty($request->goodsOrdersID)) {
            $lstGoodsOrders = $lstGoodsOrders->where('zzz_goods_orders.id', '=', intval($request->goodsOrdersID));
        }

        if (!empty($request->pName)) {
            $goodsOrdersIds = self::getGoodsOrdersIdByProductName($request->pName);
            $lstGoodsOrders = $lstGoodsOrders->whereIn('zzz_goods_orders.id', $goodsOrdersIds);
        }
        if (!empty($request->productID)) {
            $goodsOrdersIds = self::getGoodsOrdersIdByProductId($request->productID);
            $lstGoodsOrders = $lstGoodsOrders->whereIn('zzz_goods_orders.id', $goodsOrdersIds);
        }

        $isViewAll = false;
        $isSelectViewAll = false;
        $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_DDH_VIEW_ALL);
        if ($result !== false) {
            $isViewAll = true;
            if (!empty($request->viewType) && $request->viewType == true) {
                // view all record arent grouped
                $lstGoodsOrders = $lstGoodsOrders->where('zzz_goods_orders.goodsPurchaseID', '=', 0);
                $isSelectViewAll = true;
            } else {
                $lstGoodsOrders = $lstGoodsOrders->where('zzz_goods_orders.nvkdID', '=', intval($user->id));
            }
        } else {
            $lstGoodsOrders = $lstGoodsOrders->where('zzz_goods_orders.nvkdID', '=', intval($user->id));
        }

        $lstGoodsOrders = $lstGoodsOrders->select([
            'zzz_goods_orders.id as goID',
            'zzz_goods_orders.nvkdID',
            'zzz_goods_orders.createDate',
            'zzz_goods_orders.deliveryToUserDate',
            'zzz_goods_orders.suplierDeliveryDate',
            'zzz_goods_orders.statusID as goStatusID',
            //'zzz_goods_orders.numberOfProduct',
            'zzz_goods_orders.price',
            'zzz_goods_orders.currencyUnit',
            //'zzz_goods_orders.numOfPackage',
            //'zzz_goods_orders.numProductInPackage',
            'zzz_goods_orders.customerID',
            'zzz_goods_orders.note as goNote',
            'zzz_customer.*',
            'zzz_user.fullname',
        ]);
        $lstGoodsOrders = $lstGoodsOrders->leftJoin('zzz_customer', 'zzz_goods_orders.customerID', '=', 'zzz_customer.id')
            ->leftJoin('zzz_user', 'zzz_goods_orders.nvkdID', '=', 'zzz_user.id')
            ->orderBy('zzz_goods_orders.id', 'desc')
            ->paginate(\AppConst::PAGE_SIZE);

        //get all category
        $listCategory = \ClassCategory::getMapId2Name();
        $listStatus = StatusOrdersModel::all();
        $listColor = \ClassColor::getMapId2Name();
        $listMaterial = \ClassMaterial::getMapId2Name();

        $pagination = \ClassCommon::paginationAjax($lstGoodsOrders, '.list-goods-orders', '/goods-orders/list', $request);

        $data = [
            'lstGoodsOrders' => $lstGoodsOrders,
            'user' => $user,
            'categorys' => $listCategory,
            'listStatus' => $listStatus,
            'materials' => $listMaterial,
            'listUserPermission' => $listUserPermission,
            'pagination' => $pagination,
            'expandDefaultRow' => empty($request->expandDefaultRow) ? 0 : $request->expandDefaultRow,
            'isViewAll' => $isViewAll,
            'isSelectViewAll' => $isSelectViewAll,
            'selected_menu' => [3],
        ];
        return $data;
    }

    private function getGoodsOrdersIdByProductName($pName)
    {
        // if (empty($pName)) {
        //     return [];
        // }
        $goodsOrdersIds = [];
        $listGoodsOrdersId = ProductsOrdersModel::active()
            ->select('goodsOrdersID')
            ->leftJoin('zzz_product', 'productID', '=', 'zzz_product.id')
            ->where('zzz_product.name', 'LIKE', '%' . $pName . '%')
            ->groupBy('goodsOrdersID')
            ->orderBy('goodsOrdersID')
            ->get()
            ->toArray();
        foreach ($listGoodsOrdersId as $goodsOrdersId) {
            $goodsOrdersIds[] = $goodsOrdersId['goodsOrdersID'];
        }
        return $goodsOrdersIds;
    }

    private function getGoodsOrdersIdByProductId($pId)
    {
        $goodsOrdersIds = [];
        $listGoodsOrdersId = ProductsOrdersModel::active()
            ->select('goodsOrdersID')
            ->where('zzz_product.id', '=', $pId)
            ->groupBy('goodsOrdersID')
            ->orderBy('goodsOrdersID')
            ->get()
            ->toArray();
        foreach ($listGoodsOrdersId as $goodsOrdersId) {
            $goodsOrdersIds[] = $goodsOrdersId['goodsOrdersID'];
        }
        return $goodsOrdersIds;
    }

    private function getGoodsOrderIdByProductId($pId)
    {
        if (empty($pId)) {
            return [];
        }
        $goodsOrdersIds = [];
        $listGoodsOrdersId = ProductsOrdersModel::active()
            ->select('goodsOrdersID')
            ->where('zzz_product.productID', '=', $pId)
            ->groupBy('goodsOrdersID')
            ->orderBy('goodsOrdersID')
            ->get()
            ->toArray();
        foreach ($listGoodsOrdersId as $goodsOrdersId) {
            $goodsOrdersIds[] = $goodsOrdersId['goodsOrdersID'];
        }
        return $goodsOrdersIds;
    }

    public function editGoodsOrders($request, $id)
    {
        $user = \Auth::user();
        $goodsOrder = self::findById(intval($id));
        $customer = [];
        $nvkdID = $user->id;
        $product = [];
        if ($goodsOrder != null) {
            $customer = \ClassCustomer::findById(intval($goodsOrder->customerID));
            $nvkdID = $goodsOrder->nvkdID;
            $product = \ClassProduct::findById($goodsOrder->productID);
        }

        $listStatus = \ClassStatusOrder::getAll();

        return [
            'goodsOrder' => $goodsOrder,
            'id' => $id,
            'listStatus' => $listStatus,
            'customer' => $customer,
            'product' => $product,
            'nvkdID' => $nvkdID,
        ];
    }

    public function postEditGoodsOrders($request, $id)
    {
        try {
            \DB::beginTransaction();
            if (isset($request->add2order) && $request->add2order == 1) {
                //validation
                $isExistProduct = self::checkProductExistInOrder($request->goodsOrdersID, $request->productID);
                if ($isExistProduct) {
                    return [
                        '0' => 'error',
                        '1' => 'Sản phẩm này đã tồn tại trong đơn hàng.',
                    ];
                }

                if ($request->optionGoodsOrders == 'chosenByOldList') {
                    $productsOrders = new ProductsOrdersModel;
                    $productsOrders->goodsOrdersID = $request->goodsOrdersID;
                    $productsOrders->productID = $request->productID;
                    $productsOrders->countProduct = $request->countProduct;
                    $productsOrders->CountPackage = $request->CountPackage;
                    $productsOrders->countProductToPackage = $request->countProductToPackage;
                    $productsOrders->price1Product = $request->price1Product;
                    $productsOrders->save();
                    return [
                        '0' => 'success',
                        '1' => 'Save GooodOrder success',
                    ];
                }
            }

            //validation
            if (empty($request->suplierDeliveryDate01)) {
                return [
                    '0' => 'error',
                    '1' => 'Bạn chưa chọn ngày nhận hàng từ nhà cung cấp',
                ];
            }
            if (empty($request->deliveryToUserDate01)) {
                return [
                    '0' => 'error',
                    '1' => 'Bạn chưa chọn ngày dao hàng cho khách',
                ];
            }
            if ($request->option == 'chosenByOldList') {
                if (intval($request->customerID) == 0) {
                    return [
                        '0' => 'error',
                        '1' => 'Bạn chưa chọn mã khách hàng',
                    ];
                }
            } elseif ($request->option == 'addNew') {
                if (empty($request->fullname)) {
                    return [
                        '0' => 'error',
                        '1' => 'Bạn chưa nhập tên khách hàng',
                    ];
                }
            }
            if (empty($request->option)) {
                return [
                    '0' => 'error',
                    '1' => 'Tên đăng nhập không được bỏ trống',
                ];
            }

            //save customer
            $customerID = $request->customerID;
            if (intval($customerID) == 0) {
                $customer = new CustomerModel;
                $customer->fullname = $request->fullname;
                $customer->phone = $request->phone;
                $customer->skype = $request->skype;
                $customer->email = $request->email;
                $customer->address = $request->address;
                $customer->save();
                $customerID = $customer->id;
            }

            //save goods orders
            $goodsOrder = null;
            if ($id > 0) {
                $goodsOrder = self::findById($id);
            }
            if ($goodsOrder == null) {
                $goodsOrder = new GoodsOrdersModel;
            }
            $goodsOrder->nvkdID = $request->nvkdID;
            $goodsOrder->customerID = $customerID;
            $goodsOrder->statusID = $request->statusID;
            $goodsOrder->note = $request->note;
            $goodsOrder->deliveryToUserDate = $request->deliveryToUserDate01 . ' ' . $request->deliveryToUserDate02;
            $goodsOrder->suplierDeliveryDate = $request->suplierDeliveryDate01 . ' ' . $request->suplierDeliveryDate02;

            $goodsOrder->save();

            \DB::commit();
            return [
                '0' => 'success',
                '1' => 'Save new GoodsOrder success',
                '2' => $goodsOrder->id,
            ];
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    public function listSupplier($goodsOrdersID = 0)
    {
        //read account info
        $user = \Auth::user();

        //get all permission for user
        $listUserPermission = \ClassUserPermisson::getListUserPermissonById($user->id);

        if (intval($goodsOrdersID) == 0) {
            return 'Bạn chưa chọn đơn đặt hàng.';
        }
        $supplier = SupplierOrdersModel::active();
        $supplier = $supplier->select(array(
            'zzz_supplier_orders.id as soID',
            'zzz_supplier_orders.supplierID',
            'zzz_supplier_orders.goodsOrdersID',
            'zzz_supplier_orders.status as soStatus',
            'zzz_supplier_orders.status',
            'zzz_supplier.name as sname',
            'zzz_supplier.supplierWebsite',
            'zzz_supplier.supplierAddress',
            'zzz_supplier.supplierNvkdEmail',
            'zzz_supplier.supplierNvkdPhoneNumber',
            'zzz_supplier.id as sID',
            'zzz_supplier.note',
        ));
        $supplier = $supplier->leftJoin('zzz_supplier', 'zzz_supplier_orders.supplierID', '=', 'zzz_supplier.id');
        $supplier = $supplier->where('zzz_supplier_orders.goodsOrdersID', '=', $goodsOrdersID);
        $supplier = $supplier->get();

        return [
            'supplier' => $supplier,
            'listUserPermission' => $listUserPermission,
        ];
    }

    public function listProduct($goodsOrdersID)
    {
        if (intval($goodsOrdersID) == 0) {
            return 'Bạn chưa chọn đơn đặt hàng cần thêm sản phẩm';
        }
        $productOrder = ProductsOrdersModel::active();
        $productOrder = $productOrder->select(array(
            'zzz_products_orders.id as poID',
            'zzz_products_orders.productID',
            'zzz_products_orders.countProduct',
            'zzz_products_orders.CountPackage',
            'zzz_products_orders.countProductToPackage',
            'zzz_products_orders.price',
            'zzz_products_orders.goodsOrdersID',
            'zzz_product.name',
        ));
        $productOrder = $productOrder->leftJoin('zzz_product', 'zzz_products_orders.productID', '=', 'zzz_product.id');
        $productOrder = $productOrder->where('goodsOrdersID', '=', $goodsOrdersID);
        $productOrder = $productOrder->get();

        return [
            'productOrder' => $productOrder,
        ];
    }

    public function editProduct($request, $goodsOrdersID, $productOrderID)
    {
        if (intval($goodsOrdersID) == 0) {
            return 'Bạn chưa chọn đơn đặt hàng cần thêm sản phẩm';
        }

        $productOrder = self::findProductOrderById(intval($productOrderID));
        //get detail customer
        $product = (!is_null($productOrder)) ? \ClassProduct::findById($productOrder->productID) : [];
        return [
            'productOrder' => $productOrder,
            'product' => $product,
            'goodsOrdersID' => $goodsOrdersID,
            'productOrderID' => $productOrderID,
            'isOldGoodsOrder' => !empty($request->isOldGoodsOrder) ? 1 : 0,
        ];
    }

    public function postEditProduct($request, $goodsOrdersID, $productOrderID)
    {
        //validation
        if (intval($request->productID) == 0) {
            return [
                '0' => 'error',
                '1' => 'Bạn chưa nhập mã sản phẩm.',
            ];
        }
        $product = \ClassProduct::findById($request->productID);
        if (is_null($product)) {
            return [
                '0' => 'error',
                '1' => 'Mã sản phẩm không tồn tại.',
            ];
        }
        if ($productOrderID == 0) {
            $isExistProduct = self::checkProductExistInOrder($goodsOrdersID, $request->productID);
            if ($isExistProduct) {
                return [
                    '0' => 'error',
                    '1' => 'Sản phẩm này đã tồn tại trong đơn hàng. Bạn hãy quay lại đơn hàng để sửa.',
                ];
            }
        }
        try {
            //save goods orders
            $productsOrders = null;
            if ($productOrderID > 0) {
                $productsOrders = ProductsOrdersModel::find($productOrderID);
            }
            if (is_null($productsOrders)) {
                $productsOrders = new ProductsOrdersModel;
            }
            $productsOrders->goodsOrdersID = $goodsOrdersID;
            $productsOrders->productID = $request->productID;
            $productsOrders->countProduct = $request->countProduct;
            // $productsOrders->CountPackage = $request->CountPackage;
            // $productsOrders->countProductToPackage = $request->countProductToPackage;
            $productsOrders->price = $request->price;

            $productsOrders->save();
            return [
                '0' => 'success',
                '1' => 'New Product is added',
                '2' => $goodsOrdersID,
            ];
        } catch (\Exception $e) {
            return [
                '0' => 'error',
                '1' => $e->getMessage(),
            ];
        }
    }

    public function editSupplier($request, $supplierOrderID = 0, $goodsOrdersID = 0)
    {
        $supplierOrder = SupplierOrdersModel::find(intval($supplierOrderID));
        $supplier = (!is_null($supplierOrder) && intval($supplierOrder->supplierID) > 0) ?
        \ClassSupplier::findById(intval($supplierOrder->supplierID)) : [];

        return [
            'supplierOrder' => $supplierOrder,
            'goodsOrdersID' => $goodsOrdersID,
            'supplierOrderID' => $supplierOrderID,
            'supplier' => $supplier,
            'isOldGoodsOrder' => !empty($request->isOldGoodsOrder) ? 1 : 0,
        ];
    }

    public function postEditSupplier($request, $supplierOrderID, $goodsOrdersID)
    {
        //validation
        if (intval($request->supplierID) == 0) {
            return [
                '0' => 'error',
                '1' => 'Bạn chưa nhập mã NCC.',
            ];
        }
        $supplier = \ClassSupplier::findById($request->supplierID);
        if (is_null($supplier)) {
            return [
                '0' => 'error',
                '1' => 'Mã NCC không tồn tại.',
            ];
        }

        $isExistSupplier = self::checkSupplierExistInOrder($goodsOrdersID, $request->supplierID);
        if ($isExistSupplier) {
            return [
                '0' => 'error',
                '1' => 'NCC này đã tồn tại trong đơn hàng. Bạn hãy quay lại đơn hàng để sửa.',
            ];
        }

        //save goods orders
        $supplierOrders = null;
        if ($supplierOrderID > 0) {
            $supplierOrders = SupplierOrdersModel::find($supplierOrderID);
        }
        if (is_null($supplierOrders)) {
            $supplierOrders = new SupplierOrdersModel;
        }
        $supplierOrders->supplierID = $request->supplierID;
        $supplierOrders->goodsOrdersID = $goodsOrdersID;
        $supplierOrders->status = $request->status;

        $supplierOrders->save();
        return [
            '0' => 'success',
            '1' => 'New Supplier is added',
            '2' => $goodsOrdersID,
        ];
    }

    public function getGoodsOrdersByID($request)
    {
        $goodsOrder = self::findById(intval($request->id));
        $detailGoodsOrders = [];
        if (!is_null($goodsOrder) > 0) {
            $customer = ClassCustomer::findById(intval($goodsOrder->customerID));
            $detailGoodsOrders = [
                'suplierDeliveryDate01' => date('Y-m-d', strtotime($goodsOrder->suplierDeliveryDate)),
                'suplierDeliveryDate02' => date('H:i', strtotime($goodsOrder->suplierDeliveryDate)),
                'deliveryToUserDate01' => date('Y-m-d', strtotime($goodsOrder->deliveryToUserDate)),
                'deliveryToUserDate02' => date('H:i', strtotime($goodsOrder->deliveryToUserDate)),
                'statusID' => $goodsOrder->statusID,
                'gnote' => $goodsOrder->note,
                //customer
                'id' => $customer->id,
                'fullname' => $customer->fullname,
                'address' => $customer->address,
                'phone' => $customer->phone,
                'skype' => $customer->skype,
                'email' => $customer->email,
            ];
        }
        return $detailGoodsOrders;
    }

    public function updateGoodsOrderToLumpedByGoIds($gpId, $goIds)
    {
        GoodsOrdersModel::active()
            ->whereIn('id', $goIds)
            ->update(['goodsPurchaseID' => $gpId]);
    }

    public function unUpdateGoodsOrderToLumpedByGpId($gpID)
    {
        GoodsOrdersModel::where('goodsPurchaseID', $gpID)
            ->update(['goodsPurchaseID' => 0]);
    }

    public function isGoodsOrdersLumped($ids)
    {
        $goodsOrders = GoodsOrdersModel::active()
            ->whereIn('id', $ids)
            ->where('goodsPurchaseID', '=', '0')
            ->get();
        if (count($goodsOrders) != count($ids)) {
            return true;
        }
        return false;
    }

    public function save($request)
    {

    }

    public function getGoodsOrders($gpID)
    {
        $lstGoodsOrders = GoodsOrdersModel::active()
            ->select([
                'zzz_goods_orders.id as goID',
                'zzz_goods_orders.nvkdID',
                'zzz_goods_orders.createDate',
                'zzz_goods_orders.deliveryToUserDate',
                'zzz_goods_orders.suplierDeliveryDate',
                'zzz_goods_orders.statusID as goStatusID',
                'zzz_goods_orders.price',
                'zzz_goods_orders.currencyUnit',
                'zzz_goods_orders.customerID',
                'zzz_goods_orders.note as goNote',
                'zzz_customer.*',
                'zzz_user.fullname',
            ])
            ->where('zzz_goods_orders.goodsPurchaseID', '=', $gpID)
            ->leftJoin('zzz_customer', 'zzz_goods_orders.customerID', '=', 'zzz_customer.id')
            ->leftJoin('zzz_user', 'zzz_goods_orders.nvkdID', '=', 'zzz_user.id')
            ->orderBy('zzz_goods_orders.id', 'desc')
            ->paginate(\AppConst::PAGE_SIZE);

        $listStatus = StatusOrdersModel::all();
        $data = [
            'lstGoodsOrders' => $lstGoodsOrders,
            'listStatus' => $listStatus,
        ];
        return $data;
    }

}

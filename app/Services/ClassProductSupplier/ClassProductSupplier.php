<?php
namespace App\Services\ClassProductSupplier;

use App\Models\ProductSupplierModel;

class ClassProductSupplier
{
    public function index()
    {
        echo "Testing the Facades in Laravel.";
    }

    public function findById($id)
    {
        $productSupplier = ProductSupplierModel::find($id);
        return $productSupplier;
    }

    public function deleteById($id)
    {
        $obj = ProductSupplierModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function listProductSupplierByProductID($request, $productID)
    {
        //read account info
        $user = \Auth::user();

        //get all permission for user
        $listUserPermission = \ClassUserPermisson::getListUserPermissonById($user->id);

        $list = ProductSupplierModel::active()
            ->select(array(
                'zzz_product_supplier.id as psID',
                'zzz_product_supplier.countProduct',
                'zzz_product_supplier.price',
                'zzz_product_supplier.productID',
                'zzz_product_supplier.supplierID',
                'zzz_supplier.name as supplierName',
            ))
            ->leftJoin('zzz_supplier', 'zzz_supplier.id', '=', 'zzz_product_supplier.supplierID')
            ->where('productID', '=', $productID)
            ->get();

        return [
            'list' => $list,
            'listUserPermission' => $listUserPermission,
            'user' => $user,
        ];
    }

    public function editProductSupplier($request, $psID, $productID)
    {
        $user = \Auth::user();

        //get all permission for user
        $listUserPermission = \ClassUserPermisson::getListUserPermissonById($user->id);

        $productSupplier = self::findById($psID);

        $supplier = [];
        if (isset($productSupplier->supplerID) && $productSupplier->supplerID > 0) {
            $supplier = \ClassSupplier::findById($productSupplier->supplerID);
        }
        return [
            'productSupplier' => $productSupplier,
            '$user' => $user,
            'listUserPermission' => $listUserPermission,
            'psID' => $psID,
            'productID' => $productID,
            'supplier' => $supplier,
        ];
    }

    public function postEditProductSupplier($request, $psID, $productID)
    {
        if (intval($request->countProduct) == 0) {
            return '["error","' . sprintf(\AppConst::MSG_ERR_EMPTY_FIELD, 'Số lượng sản phẩm') . '"]';
        }
        if (intval($request->price) == 0) {
            return '["error","' . sprintf(\AppConst::MSG_ERR_EMPTY_FIELD, 'Giá sản phẩm') . '"]';
        }

        if (intval($request->supplierID) == 0) {
            return '["error","' . sprintf(\AppConst::MSG_ERR_EMPTY_FIELD, 'Thông tin nhà cung cấp') . '"]';
        }

        $supplier = \ClassSupplier::findById(intval($request->supplierID));

        if ($supplier == null) {
            return '["error","' . \AppConst::MSG_ERR_SUPPLIER_NAME_AND_CODE_NOT_CORRECT . '"]';
        }

        $user = \Auth::user();

        //get all permission for user
        $listUserPermission = \ClassUserPermisson::getListUserPermissonById($user->id);
        try {
            $productSupplier = (intval($psID) > 0) ? selff::findById($psID) : new ProductSupplierModel;
            $productSupplier->supplierID = $request->supplierID;
            $productSupplier->productID = $productID;
            $productSupplier->countProduct = $request->countProduct;
            $productSupplier->price = $request->price;
            $productSupplier->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return '["success"]';
    }
}

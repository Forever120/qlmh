<?php
namespace App\Services\ClassProductSupplier;

use Illuminate\Support\Facades\Facade;

class ProductSupplierFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassProductSupplier';
    }
}

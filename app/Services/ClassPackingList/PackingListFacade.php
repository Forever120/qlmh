<?php
namespace App\Services\ClassPackingList;

use Illuminate\Support\Facades\Facade;

class PackingListFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassPackingList';
    }
}

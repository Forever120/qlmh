<?php
namespace App\Services\ClassSupplier;

use App\Models\DebitNoteModel;
use App\Models\SupplierModel;

class ClassSupplier
{
    public function findById($id)
    {
        $supplier = SupplierModel::find($id);
        return $supplier;
    }

    public function deleteById($id)
    {
        $obj = SupplierModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function findByIdAndChangeAttributeName($id)
    {
        $supplier = SupplierModel::select([
            'id as supplierID',
            'name as sName',
        ])
            ->find($id);
        return [$supplier];
    }

    public function index($request)
    {
        $user = \Auth::user();
        // Check limit for page
        $limit = (isset($request->show) && intval($request->show) > 0) ?
        intval($request->show) : \AppConst::PAGE_SIZE;

        $supplier = SupplierModel::active();
        if (!empty($request->supplierCodeOrName)) {
            $sCodeOrName = $request->supplierCodeOrName;
            $supplier = $supplier->where(function ($query) use ($sCodeOrName) {
                $query->where('id', '=', intval($sCodeOrName))
                    ->orWhere('name', 'LIKE', '%' . $sCodeOrName . '%');
            });
        }
        $supplier = $supplier->paginate($limit);

        $status = \ClassStatusSupplier::mapId2Name();

        return [
            'supplier' => $supplier,
            'user' => $user,
            'status' => $status,
            'request' => $request,
            'selected_menu' => [5],
        ];
    }

    public function postEditSupplier($request, $id)
    {
        try {
            //save goods orders
            $supplier = ($id > 0) ? self::findById($id) : new SupplierModel;

            $supplier->name = $request->name;
            $supplier->supplierNvkdName = $request->supplierNvkdName;
            $supplier->supplierNvkdPhoneNumber = $request->supplierNvkdPhoneNumber;
            $supplier->supplierNvkdEmail = $request->supplierNvkdEmail;
            $supplier->supplierNvkdSkyAccount = $request->supplierNvkdSkyAccount;
            $supplier->supplierWebsite = $request->supplierWebsite;
            $supplier->supplierAddress = $request->supplierAddress;
            $supplier->supplierBankInfo = $request->supplierBankInfo;
            $supplier->note = $request->note;
            $supplier->status = $request->status;

            $supplier->save();
            return '["success"]';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function editSupplier($request, $id)
    {
        $supplier = self::findById(intval($id));
        $listStatus = \ClassStatusSupplier::getAll();
        return [
            'supplier' => $supplier,
            'id' => $id,
            'listStatus' => $listStatus,
        ];
    }

    public function history($sid)
    {
        $productPriceOfSupplier = DB::table('zzz_product_price_of_supplier')
            ->select(array(
                'zzz_product_price_of_supplier.id as ppsID',
                'zzz_product_price_of_supplier.*',
                'zzz_status_supplier.name as sName',
                'zzz_list_price.name as lpName',
                'zzz_product.name as pName',
                'zzz_product.id as pid',
            ))
            ->leftJoin('zzz_product', 'zzz_product_price_of_supplier.productID', '=', 'zzz_product.id')
            ->leftJoin('zzz_list_price', 'zzz_product_price_of_supplier.listPriceID', '=', 'zzz_list_price.id')
            ->leftJoin('zzz_status_supplier', 'zzz_product_price_of_supplier.statusSupplierID', '=', 'zzz_status_supplier.id')
            ->where('supplierID', '=', $sid)
            ->get();

        return view('Supplier/history', [
            'productPriceOfSupplier' => $productPriceOfSupplier,
        ]);
    }

    public function searchSupplierByName($request)
    {
        $listSupplier = SupplierModel::active();
        if (!empty($request->keyword)) {
            $listSupplier = $listSupplier->where('name', 'like', "%{$request->keyword}%");
        }
        $listSupplier = $listSupplier->limit(\AppConst::PAGE_SIZE)
            ->select('id', 'name')
            ->orderBy('name', 'asc')
            ->get();
        return $listSupplier->toArray();
    }

    public function getSupplierIDByName($request)
    {
        $supplier = SupplierModel::active();
        if ($request->type == 'id') {
            $supplier = $supplier->where('id', $request->value);
        } elseif ($request->type == 'name') {
            $supplier = $supplier->where('name', $request->value);
        }
        $supplier = $supplier->orderBy('name', 'asc')
            ->first();
        $result = 0;
        if ($supplier != null) {
            if ($request->type == 'id') {
                $result = $supplier->name;
            } elseif ($request->type == 'name') {
                $result = $supplier->id;
            }
        }
        return $result;
    }

    public function getLastestDebitRecordByGoodsPurchaseId($sId, $gpId)
    {
        $lastestDebit = DebitNoteModel::active()
            ->where('supplierID', '=', $sId)
            ->where('goodsPurchaseID', '=', $gpId)
            ->orderBy('date', 'DESC')
            ->first();

        return $lastestDebit;
    }

    public function findDebitBySupplierIdAndGoodsPurcharId($sId, $gpId = null)
    {
        $debitNotes = DebitNoteModel::active();
        if (!empty($sId)) {
            $debitNotes = $debitNotes->where('supplierID', '=', $sId);
        }
        if (!empty($gpId)) {
            $debitNotes = $debitNotes->where('goodsPurchaseID', '=', $gpId);
        }
        $debitNotes = $debitNotes->orderBy('date', 'DESC');
        $debitNotes = $debitNotes->get();
        return $debitNotes;
    }

    public function showDebitHistory($request, $sId)
    {
        $debitNotes = self::findDebitBySupplierIdAndGoodsPurcharId($sId);
        $statusDebits = \AppConst::getListStatusDebit();
        return [
            'debitNotes' => $debitNotes,
            'statusDebits' => $statusDebits,
            'sId' => $sId,
        ];
    }

    public function createExchangeMoney($request, $sId)
    {
        $statusDebits = \AppConst::getListStatusDebit();
        return [
            'statusDebits' => $statusDebits,
            'sId' => $sId,
        ];
    }

    public function storeExchangeMoney($request, $sId)
    {
        $user = \Auth::user();
        if (empty($request->goodsPurchaseID)) {
            return [
                '0' => 'false',
                '1' => 'Bạn chưa nhập mã hóa đơn mua hàng',
            ];
        } else {
            $goodPurchase = \ClassGoodsPurchase::findById($request->goodsPurchaseID);
            if (is_null($goodPurchase)) {
                return [
                    '0' => 'false',
                    '1' => 'Đơn mua hàng này không tồn tại',
                ];
            }
        }
        if (empty($request->exchangeMonney)) {
            return [
                '0' => 'false',
                '1' => 'Bạn chưa nhập số ghi nợ',
            ];
        }
        if (empty($request->status)) {
            return [
                '0' => 'false',
                '1' => 'Bạn chưa trạng thái cho ghi nợ này',
            ];
        }
        try {
            // Lấy thông tin lần cuối cùng ghi nợ của nhà cung cấp này
            $lastestDebit = self::getLastestDebitRecordByGoodsPurchaseId($sId, $request->goodsPurchaseID);
            if (is_null($lastestDebit)) {
                // Thực tế ko thể vào đây
                return [
                    '0' => 'false',
                    '1' => 'Không có dư nợ của đơn hàng này, bạn ko thể trả nợ',
                ];
            }
            $debitNote = new DebitNoteModel;
            $debitNote->supplierID = $sId;
            $debitNote->goodsPurchaseID = $request->goodsPurchaseID;
            $debitNote->exchangeMonney = $request->exchangeMonney;
            $debitNote->status = $request->status;
            $debitNote->note = !empty($request->goodsPurchaseID) ? $request->goodsPurchaseID : '';

            $debitNote->nvmhID = $user->id;
            $debitNote->desposited = $lastestDebit->desposited + $debitNote->exchangeMonney;
            $debitNote->remain = $lastestDebit->remain - $debitNote->exchangeMonney;
            $debitNote->totalDebitMoney = $lastestDebit->totalDebitMoney;

            $debitNote->save();
        } catch (\Exception $e) {
            return [
                '0' => 'false',
                '1' => $e->getMessage(),
            ];
        }

        return [
            '0' => 'success',
            '1' => 'Thêm ghi nợ thành công',
        ];
    }

    public function addNewGoodPurchaseToDebit($sId, $gpId, $exchangeMonney, $price)
    {
        $user = \Auth::user();
        $lastestDebit = self::getLastestDebitRecordByGoodsPurchaseId($sId, $gpId);

        $debitNoteNewGoodsPurchase = new DebitNoteModel;
        $debitNoteNewGoodsPurchase->supplierID = $sId;
        $debitNoteNewGoodsPurchase->goodsPurchaseID = $gpId;
        $debitNoteNewGoodsPurchase->exchangeMonney = -$price;
        $debitNoteNewGoodsPurchase->status = 1; // Mua hàng, đặt đơn hàng mới
        $debitNoteNewGoodsPurchase->nvmhID = $user->id;
        $debitNoteNewGoodsPurchase->desposited = (empty($lastestDebit)) ? 0 : $lastestDebit->desposited;
        $debitNoteNewGoodsPurchase->remain = (empty($lastestDebit)) ? $price : $lastestDebit->remain + $price;
        $debitNoteNewGoodsPurchase->totalDebitMoney = (empty($lastestDebit)) ? $price : $lastestDebit->totalDebitMoney + $price;
        $debitNoteNewGoodsPurchase->note = 'Dặt đơn hàng mới';
        $debitNoteNewGoodsPurchase->save();
        if ($exchangeMonney > 0) {
            $debitNote = new DebitNoteModel;
            $debitNote->supplierID = $sId;
            $debitNote->goodsPurchaseID = $gpId;
            $debitNote->exchangeMonney = $exchangeMonney;
            $debitNote->status = 2; // Trả tiền NCC
            $debitNote->nvmhID = $user->id;
            $debitNote->desposited = $debitNoteNewGoodsPurchase->desposited + $exchangeMonney;
            $debitNote->remain = $debitNoteNewGoodsPurchase->remain - $exchangeMonney;
            $debitNote->totalDebitMoney = $debitNoteNewGoodsPurchase->totalDebitMoney;
            $debitNote->note = 'Trả tiền';
            $debitNote->save();
        }
    }
    // public function
}

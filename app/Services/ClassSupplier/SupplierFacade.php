<?php
namespace App\Services\ClassSupplier;

use Illuminate\Support\Facades\Facade;

class SupplierFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassSupplier';
    }
}

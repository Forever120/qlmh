<?php
namespace App\Services\ClassCommon;

use Illuminate\Support\Facades\Facade;

class CommonFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassCommon';
    }
}

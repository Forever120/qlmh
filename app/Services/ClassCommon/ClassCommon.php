<?php
namespace App\Services\ClassCommon;

use App\Http\Component\formatData;
use Illuminate\Support\Facades\Auth;

class ClassCommon
{
    public static function getUserConifgTmp()
    {
        $userConfigFilePathTmp = config_path() . '/userConfig.json.tmp';
        if (file_exists($userConfigFilePathTmp)) {
            $listUserConfigTmp = file_get_contents($userConfigFilePathTmp);
        } else {
            return false;
        }
        $listUserConfigTmp = json_decode($listUserConfigTmp);

        return $listUserConfigTmp;
    }

    public static function getUserListNotifyPriceActiveTmp()
    {
        $listUserConfigTmp = self::getUserConifgTmp();
        if ($listUserConfigTmp === false) {
            return false;
        }
        return $listUserConfigTmp->listNotifyPriceActive;
    }

    private static function initUserConfig($userName)
    {
        $userConfigPath = public_path() . DATA_USER . $userName;
        $userConfigFilePath = $userConfigPath . '/userConfig.json';
        if (!file_exists($userConfigFilePath)) {
            formatData::makeDirectory($userConfigPath, 0777, true, true);

            $listUserConfig = self::getUserConifgTmp();
            if ($listUserConfig === false) {
                return false;
            }

            $arr_notify_price_id = [];
            foreach ($listUserConfig->listNotifyPriceActive as $notifyPrice) {
                $arr_notify_price_id[] = $notifyPrice->id;
            }
            $listUserConfig->listNotifyPriceActive = $arr_notify_price_id;

            $fh = fopen($userConfigFilePath, 'w');
            fwrite($fh, json_encode($listUserConfig, true));
            fclose($fh);
        }
        return true;
    }

    private static function writeUserConfig($userName, $listUserConfig)
    {
        $userConfigFilePath = (public_path() . DATA_USER . $userName . '/userConfig.json');
        $fh = fopen($userConfigFilePath, 'w');
        fwrite($fh, json_encode($listUserConfig, true));
        fclose($fh);
    }

    public static function getUserConifg($userName)
    {
        $initConfig = self::initUserConfig($userName);
        if ($initConfig === false) {
            return false;
        }

        $userConfigFilePath = (public_path() . DATA_USER . $userName . '/userConfig.json');
        $listUserConfig = file_get_contents($userConfigFilePath);
        $listUserConfig = json_decode($listUserConfig);

        return $listUserConfig;
    }

    public static function getUserListActive($userName, $listAcitveDefault, $attributeName = \AppConst::LIST_ACTIVE_PRICE)
    {
        $listUserConfig = self::getUserConifg($userName);
        if ($listUserConfig === false) {
            return false;
        }
        if (count($listUserConfig->$attributeName) == 0 && $listAcitveDefault != null) {
            $listUserConfig->$attributeName = $listAcitveDefault;
            self::writeUserConfig($userName, $listUserConfig);
        }
        return $listUserConfig->$attributeName;
    }

    public static function setUserListActive($userName, $listPriceActive, $attributeName = \AppConst::LIST_ACTIVE_PRICE)
    {
        $listUserConfig = self::getUserConifg($userName);
        if ($listUserConfig === false) {
            return false;
        }
        if ($listPriceActive != null) {
            $listUserConfig->$attributeName = $listPriceActive;
            self::writeUserConfig($userName, $listUserConfig);
        }
    }

    public function changeUserListActive($attribute, $userActive, $type)
    {
        try {
            $userName = Auth::user()->username;

            $listActive = \ClassCommon::getUserListActive($userName, null, $attribute);
            $arr_active = [];

            if ($type == 'unset') {
                foreach ($listActive as $key => $active) {
                    if ($active == $userActive) {
                        unset($listActive[$key]);
                        continue;
                    }
                    $arr_active[] = $active;
                }
            } elseif ($type == 'add') {
                foreach ($listActive as $key => $active) {
                    $arr_active[] = $active;
                }
                $arr_active[] = $userActive;
            }
            \ClassCommon::setUserListActive($userName, $arr_active, $attribute);
            return response()->json([
                '0' => 'success',
                '1' => 'change list user success',
            ]);
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function paginationAjax($listPagingRecord, $reloadClassName, $rootLink, $request)
    {
        $params = $request->all();
        $requestStr = "";
        foreach ($params as $key => $value) {
            if ($key != 'page') {
                $requestStr .= '&' . $key . '=' . $value;
            }
        }
        $pagination = "";
        $currPage = $listPagingRecord->currentPage();
        $lastPage = $listPagingRecord->lastPage();
        $pagination .= '<div class="text-center">';
        $pagination .= '<ul class="pagination">';

        $pagination .= '<li ' . (($currPage == 1) ? '' : 'onclick="loadPaginator(this, \'' . $reloadClassName . '\', \'' . $rootLink . '?page=1' . $requestStr . '\')"');
        $pagination .= 'class="' . (($currPage == 1) ? 'disabled' : '') . ' ">';
        $pagination .= '<a> << </a>';
        $pagination .= '</li>';

        $pagination .= '<li ' . (($currPage == 1) ? '' : 'onclick="loadPaginator(this, \'' . $reloadClassName . '\', \'' . $rootLink . '?page=' . ($currPage - 1) . $requestStr . '\')"');
        $pagination .= 'class="' . (($currPage == 1) ? 'disabled' : '') . ' ">';
        $pagination .= '<a> < </a>';
        $pagination .= '</li>';

        for ($page = 1; $page <= $lastPage; $page++) {
            $pagination .= '<li ' . (($currPage == $page) ? '' : 'onclick="loadPaginator(this, \'' . $reloadClassName . '\', \'' . $rootLink . '?page=' . $page . $requestStr . '\')"');
            $pagination .= 'class="' . (($currPage == $page) ? 'active' : '') . ' ">';
            $pagination .= '<a>' . $page . '</a>';
            $pagination .= '</li>';
        }

        $pagination .= '<li ' . (($currPage == $lastPage) ? '' : 'onclick="loadPaginator(this, \'' . $reloadClassName . '\', \'' . $rootLink . '?page=' . ($currPage + 1) . $requestStr . '\')"');
        $pagination .= 'class="' . (($currPage == $lastPage) ? 'disabled' : '') . ' ">';
        $pagination .= '<a> > </a>';
        $pagination .= '</li>';

        $pagination .= '<li ' . (($currPage == $lastPage) ? '' : 'onclick="loadPaginator(this, \'' . $reloadClassName . '\', \'' . $rootLink . '?page=' . ($lastPage) . $requestStr . '\')"');
        $pagination .= 'class="' . (($currPage == $lastPage) ? 'disabled' : '') . ' ">';
        $pagination .= '<a> >> </a>';
        $pagination .= '</li>';

        $pagination .= '</ul>';
        $pagination .= '</div>';

        return $pagination;
    }
}

<?php
namespace App\Services\ClassImage;

use Illuminate\Support\Facades\Facade;

class ImageFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassImage';
    }
}

<?php
namespace App\Services\ClassImage;

use App\Models\ImagesModel;

class ClassImage
{
    public function getByPid($pid)
    {
        $images = ImagesModel::active()->where('productID', $pid)->get();
        return $images;
    }

    public function delete($id)
    {
        $obj = ImagesModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }
}

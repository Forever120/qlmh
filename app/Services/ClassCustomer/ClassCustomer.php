<?php
namespace App\Services\ClassCustomer;

use App\Models\CustomerModel;

class ClassCustomer
{
    public function index()
    {
        echo "Testing the Facades in Laravel.";
    }

    public function findById($id)
    {
        $customer = CustomerModel::find($id);
        return $customer;
    }

    public function deleteById($id)
    {
        $obj = CustomerModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function getCustomerByID($id)
    {
        $customer = self::findById(intval($id));
        if ($customer != null) {
            return json_encode($customer->toArray());
        }
        return 0;
    }
}

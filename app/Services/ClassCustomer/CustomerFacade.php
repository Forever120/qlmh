<?php
namespace App\Services\ClassCustomer;

use Illuminate\Support\Facades\Facade;

class CustomerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassCustomer';
    }
}

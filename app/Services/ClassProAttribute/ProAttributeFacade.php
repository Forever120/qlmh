<?php
namespace App\Services\ClassProAttribute;

use Illuminate\Support\Facades\Facade;

class ProAttributeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassProAttribute';
    }
}

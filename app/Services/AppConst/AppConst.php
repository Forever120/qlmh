<?php
namespace App\Services\AppConst;

class AppConst
{
    const ADMIN_TYPE = 1;
    const NVKD_TYPE = 2;
    const NVMH_TYPE = 3;

    const SUPPER_ADMIN = 3;

    // Permisson
    const PERMISSON_BANG_GIA_VIEW = 'BANG_GIA_VIEW';
    const PERMISSON_BANG_GIA_EDIT = 'BANG_GIA_EDIT';
    const PERMISSON_BANG_GIA_DELETE = 'BANG_GIA_DELETE';

    const PERMISSON_BAO_GIA_VIEW = 'BAO_GIA_VIEW';
    const PERMISSON_BAO_GIA_EDIT = 'BAO_GIA_EDIT';
    const PERMISSON_BAO_GIA_DELETE = 'BAO_GIA_DELETE';

    const PERMISSON_DDH_VIEW_ALL = 'DDH_VIEW_ALL';
    const PERMISSON_DDH_VIEW = 'DDH_VIEW';
    const PERMISSON_DDH_EDIT = 'DDH_EDIT';
    const PERMISSON_DDH_DELETE = 'DDH_DELETE';

    const PERMISSON_DMH_VIEW = 'DMH_VIEW';
    const PERMISSON_DMH_VIEW_ALL = 'DMH_VIEW_ALL';
    const PERMISSON_DMH_EDIT = 'DMH_EDIT';
    const PERMISSON_DMH_DELETE = 'DMH_DELETE';

    const PERMISSON_DOI_TAC_VIEW = 'DOI_TAC_VIEW';
    const PERMISSON_DOI_TAC_EDIT = 'DOI_TAC_EDIT';
    const PERMISSON_DOI_TAC_DELETE = 'DOI_TAC_DELETE';

    const PERMISSON_PACKING_LIST_VIEW = 'PACKING_LIST_VIEW';
    const PERMISSON_PACKING_LIST_EDIT = 'PACKING_LIST_EDIT';
    const PERMISSON_PACKING_LIST_DELETE = 'PACKING_LIST_DELETE';

    const PERMISSON_USER_VIEW = 'USER_VIEW';
    const PERMISSON_USER_EDIT = 'USER_EDIT';
    const PERMISSON_USER_DELETE = 'USER_DELETE';

    const PERMISSON_GROUP_VIEW = 'GROUP_VIEW';
    const PERMISSON_GROUP_EDIT = 'GROUP_EDIT';
    const PERMISSON_GROUP_DELETE = 'GROUP_DELETE';

    const PERMISSON_MAU_SAC = 'MAU_SAC';
    const PERMISSON_CHAT_LIEU = 'CHAT_LIEU';
    const PERMISSON_CATEGORY = 'CATEGORY';
    const PERMISSON_CUSTOMER = 'CUSTOMER';
    const PERMISSON_STATUS_DON_DAT_HANG = 'STATUS_DON_DAT_HANG';
    const PERMISSON_STATUS_NCC = 'STATUS_NCC';
    const PERMISSON_STATUS_BAO_GIA = 'STATUS_BAO_GIA';
    const PERMISSON_COLLUMN = 'COLLUMN';

    const VIEW_NOT_HAVE_PERMISSON = 'element/notPermission';

    const VIEW_TYPE_ALL = 'VIEW_ALL';
    const VIEW_TYPE_OWN = 'VIEW_OWN';

    // User list paging
    const USER_PAGING = 3;
    const PAGE_SIZE = 5;

    const RETURN_SUCCESS = 'success';
    const RETURN_MSG = 'msg';
    const RETURN_OBJ = 'obj';

    // public function getDefaultResult()
    // {
    //     return [
    //         self::RETURN_SUCCESS => true,
    //         self::RETURN_MSG => '',
    //         self::RETURN_OBJ => null,
    //     ];
    // }
    // Message
    const ERR_MSG_PERMISSON_DENY = 'Permisson deny';

    const DATA_NOT_SETTED = 'N/A';

    // Active Name
    const LIST_ACTIVE_PRICE_KEY = 'listPriceActive';
    const LIST_ACTIVE_NOTIFY_PRICE_KEY = 'listNotifyPriceActive';

    // Message error
    const MSG_ERR_EMPTY_FIELD = '%s không được bỏ trống';
    const MSG_ERR_EXIST_FIELD = '%s đã tồn tại';
    const MSG_ERR_NOT_EXIST_FIELD = '%s không đã tồn tại';
    const MSG_ERR_PASSWORD_NOT_MATCH = 'Mật khẩu và mật khẩu xác nhận không giống nhau';
    const MSG_ERR_NOT_SELECTED = '% chưa được chọn';
    const MSG_ERR_SUPPLIER_NAME_AND_CODE_NOT_CORRECT = 'Tên và mã nhà cung cấp không đúng';

    const MSG_ERR_DELETE_SUCCESS = 'Xóa thành công';

    public static function getReturn()
    {
        return [
            self::RETURN_SUCCESS => true,
            self::RETURN_MSG => 'Nothing',
            self::RETURN_OBJ => null,
        ];
    }

    const STATUS_DEBIT_NO_CHOISE = 0;
    const STATUS_DEBIT_NEW_GOODS_PURCHASE = 1;
    const STATUS_DEBIT_PAY_EXCHANGE_MONEY = 2;
    public static function getListStatusDebit()
    {
        return [
            self::STATUS_DEBIT_NO_CHOISE => 'Chưa chọn trạng thái',
            self::STATUS_DEBIT_NEW_GOODS_PURCHASE => 'Mua đơn hàng mới',
            self::STATUS_DEBIT_PAY_EXCHANGE_MONEY => 'Trả tiền cho NCC',
        ];
    }
}

<?php
namespace App\Services\AppConst;

use Illuminate\Support\Facades\Facade;

class AppConstFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AppConst';
    }
}

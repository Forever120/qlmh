<?php
namespace App\Services\ClassMaterial;

use App\Models\MaterialModel;

class ClassMaterial
{
    public function getMapId2Name()
    {
        $materials = MaterialModel::active()->get();
        $mapId2Name = [];
        foreach ($materials as $material) {
            $mapId2Name[$material->id] = $material->name;
        }
        return $mapId2Name;
    }

    public function index($request)
    {
        $materials = MaterialModel::active()->get();
        return $materials;
    }

    public function findById($id)
    {
        $material = MaterialModel::find($id);
        return $material;
    }
    public function deleteById($id)
    {
        $obj = MaterialModel::getModelInstance();
        $ret = $obj->deleteById($id);
        return $ret;
    }

    public function htmlListMaterial()
    {
        $list = MaterialModel::active()->orderBy('NO', 'asc')->get();
        $htmlList = '';
        if (count($list) > 0) {
            $htmlList .= '<ol class="dd-list">';
            foreach ($list as $item) {
                $htmlList .= '<li data-id="' . $item->id . '" class="dd-item">'
                . '<div class="option-menu">'
                . '<a onclick="loadPopupLarge(\'/chat-lieu/edit/' . $item->id . '\')" data-toggle="modal" data-target=".bs-modal-lg"><i data-pack="default" class="ion-edit"></i></a>&nbsp;&nbsp;'
                . '<a onclick="deleteRow(\'/chat-lieu/delete/' . $item->id . '\', \'/chat-lieu/list\')"  tabindex="-1"><i data-pack="default" class="ion-trash-a"></i></a></div>'
                    . '<div class="card b0 dd-handle"><div class="mda-list">'
                    . '<div class="mda-list-item"><div class="mda-list-item-icon item-grab"><em class="ion-drag icon-lg"></em></div>'
                    . '<div class="mda-list-item-text mda-2-line">';
                $htmlList .= '<h4>' . $item->name . '</h4>';
                $htmlList .= '</div><div class="_right">'
                    . '</div></div></div></div>';
                $htmlList .= "</li>";
            }
            $htmlList .= '</ol>';
        }
        return ($htmlList);
    }

    public function recursivelyUpdatePosition($listMaterialID, $parentId = 0)
    {
        $idx = 0;
        foreach ($listMaterialID as $item) {
            $idx++;
            $material = MaterialModel::find($item['id']);
            $material->NO = $idx;
            $material->save();
            if (isset($item['children'])) {
                self::recursivelyUpdatePosition($item['children'], $item['id']);
            }
        }
        return 'success';
    }

    public function editMaterial($request, $id)
    {
        $user = \Auth::user();
        $material = self::findById(intval($id));

        return [
            'material' => $material,
            'id' => $id,
        ];
    }

    public function postEditMaterial($request, $id)
    {
        try {
            if ($id > 0) {
                $material = MaterialModel::find($id);
            } else {
                $material = new MaterialModel;
            }
            $material->name = $request->name;
            $material->NO = 0;
            $material->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return 'success';
    }
}

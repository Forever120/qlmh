<?php
namespace App\Services\ClassMaterial;

use Illuminate\Support\Facades\Facade;

class MaterialFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ClassMaterial';
    }
}

<?php

namespace App\Models;

class GoodsOrdersModel extends BaseModel
{
    //
    protected $table = 'zzz_goods_orders';
    protected $fillable = [
        'id',
        'nvkdID',
        'productID',
        'customerID',
        'createDate',
        'deliveryToUserDate',
        'suplierDeliveryDate',
        'statusID',
        'numberOfProduct',
        'price',
        'currencyUnit',
        'numOfPackage',
        'numProductInPackage',
        'note',
        'goodsPurchaseID',
        'created_at',
        'updated_at',
    ];
}

<?php

namespace App\Models;

class ProductModel extends BaseModel
{
    protected $table = 'zzz_product';
    protected $fillable = [
        'id',
        'name',
        'code',
        'categoryID',
        'parentId',
        'colorID',
        'materialID',
        'volume',
        'weight',
        'description',
        'note',
        'listPriceNote',
        'packingList',
        'StatusID',
        'ngungKD',
        'userID',
        'created_at',
        'updated_at',
    ];

    public static function saveProduct($request, $pid, $userId)
    {
        if ($pid > 0) {
            $product = ProductModel::find($pid);
        } else {
            $product = new ProductModel;
        }
        if ($request->parentID > 0) {
            $product->parentID = $request->parentID;
        }
        $product->name = $request->name;
        $product->categoryID = $request->categoryID;
        $product->colorID = $request->colorID;
        $product->materialID = $request->materialID;
        $product->volume = $request->volume;
        $product->weight = $request->weight;
        $product->note = $request->note;
        $product->description = $request->description;
        $product->ngungKD = $request->ngungKD;
        $product->userID = $userId;
        $product->save();

        return $product;
    }
}

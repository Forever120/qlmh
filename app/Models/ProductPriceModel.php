<?php

namespace App\Models;

class ProductPriceModel extends BaseModel
{
    //
    protected $table = 'zzz_product_price';
    protected $fillable = [
        'id',
        'productID',
        'listPriceID',
        'prodPrice',
        'created_at',
        'updated_at',
    ];
}

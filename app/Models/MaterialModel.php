<?php

namespace App\Models;

class MaterialModel extends BaseModel
{
    //
    protected $table = 'zzz_material';
    protected $fillable = [
        'id',
        'name',
        'NO',
        'materialParentID',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

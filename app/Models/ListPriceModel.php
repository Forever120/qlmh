<?php

namespace App\Models;

class ListPriceModel extends BaseModel
{
    //
    protected $table = 'zzz_list_price';
    protected $fillable = [
        'id',
        'name',
        'NO',
        'functionID',
        'created_at',
        'updated_at',
        'active',
        'active',
    ];
}

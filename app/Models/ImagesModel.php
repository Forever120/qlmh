<?php

namespace App\Models;

class ImagesModel extends BaseModel
{

    //
    protected $table = 'zzz_image';
    protected $fillable = [
        'id',
        'productID',
        'path',
        'note',
        'created_at',
        'updated_at',
        'updated_at',
    ];

    public static function saveMultipleImage($files, $productID)
    {
        $file_count = count($files);
        $uploadcount = 0;
        $imgData = [];
        foreach ($files as $file) {
            $destinationPath = 'imgs/products';
            $filename = $file->getClientOriginalName();
            $filePath = $destinationPath . '/' . $filename;
            $upload_success = $file->move($destinationPath, $filename);
            $uploadcount++;
            $imgData[] = [
                'productID' => $productID,
                'path' => $filePath,
            ];
        }
        ImagesModel::insert($imgData);
    }
}

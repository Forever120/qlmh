<?php

namespace App\Models;

class SupplierModel extends BaseModel
{
    //
    protected $table = 'zzz_supplier';
    protected $fillable = [
        'id',
        'name',
        'supplierNvkdName',
        'supplierNvkdEmail',
        'supplierNvkdPhoneNumber',
        'supplierNvkdSkyAccount',
        'supplierWebsite',
        'supplierAddress',
        'supplierBankInfo',
        'status',
        'note',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

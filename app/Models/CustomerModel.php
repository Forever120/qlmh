<?php

namespace App\Models;

class CustomerModel extends BaseModel
{
    //
    protected $table = 'zzz_customer';
    protected $fillable = [
        'id',
        'fullname',
        'address',
        'phone',
        'skype',
        'email',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

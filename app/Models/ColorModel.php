<?php

namespace App\Models;

class ColorModel extends BaseModel
{
    //
    protected $table = 'zzz_color';
    protected $fillable = [
        'id',
        'name',
        'NO',
        'colorParentID',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

<?php

namespace App\Models;

class GroupModel extends BaseModel
{
    //
    protected $table = 'zzz_group';
    protected $fillable = [
        'id',
        'name',
        'permission',
        'created_at',
        'updated_at',
    ];
}

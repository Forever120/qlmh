<?php

namespace App\Models;

class ProductsPurchaseModel extends BaseModel
{
    //
    protected $table = 'zzz_products_purchase';
    protected $fillable = [
        'id',
        'productID',
        'countProduct',
        'CountPackage',
        'countProductToPackage',
        'price',
        'goodsPurchaseID',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

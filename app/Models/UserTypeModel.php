<?php

namespace App\Models;

class UserTypeModel extends BaseModel
{
    //
    protected $table = 'zzz_user_type';
    protected $fillable = [
        'userTypeName',
        'userTypeID',
        'defaultPermisson',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

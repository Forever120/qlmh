<?php

namespace App\Models;

class SupplierOrdersModel extends BaseModel
{
    //
    protected $table = 'zzz_supplier_orders';
    protected $fillable = [
        'id',
        'supplierID',
        'goodsOrdersID',
        'status',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

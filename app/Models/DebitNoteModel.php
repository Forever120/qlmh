<?php

namespace App\Models;

class DebitNoteModel extends BaseModel
{
    //
    protected $table = 'zzz_debit_note';
    protected $fillable = [
        'id',
        'supplierID',
        'goodsPurchaseID',
        'nvmhID',
        'date',
        'exchangeMonney',
        'desposited',
        'remain',
        'status',
        'totalDebitMoney',
        'note',
        'created_at',
        'updated_at',
        'delFlg',
    ];
}

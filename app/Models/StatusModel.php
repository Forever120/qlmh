<?php

namespace App\Models;

class StatusModel extends BaseModel
{
    //
    protected $table = 'zzz_status';
    protected $fillable = [
        'id',
        'statusTypeID',
        'name',
        'sortOrder',
        'note',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

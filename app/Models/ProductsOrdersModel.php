<?php

namespace App\Models;

class ProductsOrdersModel extends BaseModel
{
    //
    protected $table = 'zzz_products_orders';
    protected $fillable = [
        'id as poID',
        'productID',
        'countProduct',
        'CountPackage',
        'countProductToPackage',
        'price1Product',
        'goodsOrdersID',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

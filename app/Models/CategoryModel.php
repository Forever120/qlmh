<?php

namespace App\Models;

class CategoryModel extends BaseModel
{
    //
    protected $table = 'zzz_category';

    protected $fillable = [
        'id',
        'name',
        'parentID',
        'NO',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

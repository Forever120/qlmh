<?php

namespace App\Models;

class UsersModel extends BaseModel
{
    //
    protected $table = 'zzz_user';
    protected $fillable = [
        'id',
        'groupID',
        'username',
        'userType',
        'password',
        'firstName',
        'lastName',
        'fullname',
        'avatar',
        'birthday',
        'phone',
        'skype',
        'email',
        'address',
        'remember_token',
        'created_at',
        'updated_at',
        'updated_at',
    ];
    public static function getListUser($paging)
    {
        //return
    }
}

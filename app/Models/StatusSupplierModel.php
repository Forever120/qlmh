<?php

namespace App\Models;

class StatusSupplierModel extends BaseModel
{
    //
    protected $table = 'zzz_status_supplier';
    protected $fillable = [
        'id',
        'name',
        'sortOrder',
        'updated_at',
        'created_at',
        'created_at',
    ];
}

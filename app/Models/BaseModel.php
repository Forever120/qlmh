<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    private static $modelInstances = [];
    /*
     * Use Singleton Mode
     */
    public static function getModelInstance()
    {
        $callClassPath = get_called_class();
        $callClassArr = explode("\\", $callClassPath);
        $callClass = end($callClassArr);
        if (isset(self::$modelInstances[$callClass])) {
            return self::$modelInstances[$callClass];
        } else {
            $obj = new $callClassPath;
            self::$modelInstances[$callClass] = $obj;
            return self::$modelInstances[$callClass];
        }
    }

    public function scopeActive($query)
    {
        return $query->where($this->table . '.delFlg', '<>', '1');
    }

    /*
     * Delete record by Id
     */
    public function deleteById($id)
    {
        $ret = \AppConst::getReturn();
        if ($id > 0) {
            $obj = self::getModelInstance();
            try {
                $obj = $obj->find($id);
                $obj->delFlg = 1;
                $obj->save();
            } catch (\Exception $e) {
                $ret[\AppConst::RETURN_MSG] = $e->getMessage();
            }
        }
        return 'success';
    }
}

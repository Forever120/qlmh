<?php

namespace App\Models;

class NotifyPriceModel extends BaseModel
{
    //
    protected $table = 'zzz_notify_price';
    protected $fillable = [
        'id',
        'priceID',
        'statusID',
        'nvkdID',
        'nvmhID',
        'adminID',
        'productID',
        'countOrders',
        'priceForNvkd',
        'priceForNvmh',
        'cloneNotifPriceID',
        'notifyUserIDs',
        'deadline',
        'note',
        'created_at',
        'updated_at',
        'updated_at',
    ];

    public static function saveNotifyPrice($request, $userID, $productID)
    {
    }
}

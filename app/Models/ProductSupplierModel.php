<?php

namespace App\Models;

class ProductSupplierModel extends BaseModel
{
    //
    protected $table = 'zzz_product_supplier';
    protected $fillable = [
        'id',
        'productID',
        'supplierID',
        'countProduct',
        'price',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

<?php

namespace App\Models;

class UserPermissonModel extends BaseModel
{
    //
    protected $table = 'zzz_user_permisson';
    protected $fillable = [
        'userID',
        'permissionID',
        'created_at',
        'updated_at',
        'updated_at',
    ];
    //protected $primaryKey = 'id';
}

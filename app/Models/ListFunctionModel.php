<?php

namespace App\Models;

class ListFunctionModel extends BaseModel
{
    //
    protected $table = 'zzz_list_function';
    protected $fillable = [
        'functionName',
        'funcID',
        'note',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

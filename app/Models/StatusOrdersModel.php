<?php

namespace App\Models;

class StatusOrdersModel extends BaseModel
{
    //
    protected $table = 'zzz_status_orders';
    protected $fillable = [
        'id',
        'name',
        'sortOrder',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

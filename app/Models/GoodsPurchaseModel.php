<?php

namespace App\Models;

class GoodsPurchaseModel extends BaseModel
{
    //
    protected $table = 'zzz_goods_purchase';
    protected $fillable = [
        'id',
        'nvmhID',
        'listGoodsOrderID',
        'statusID',
        'invoiceFilePath',
        'packingListFilePath',
        'billOfLadingFilePath',
        'orderDate',
        'deliverDate',
        'arriveDate',
        'produceDate',
        'price',
        'desposits',
        'remain',
        'currencyUnit',
        'note',
        'created_at',
        'updated_at',
        'updated_at',
    ];
}

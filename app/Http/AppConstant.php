<?php

namespace App\Http;

class AppConstant
{
    const ADMIN_TYPE = 1;
    const NVMH_TYPE = 2;
    const NVKD_TYPE = 3;

    // User list paging
    const USER_PAGING = 3;
}

 <?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::match(['get', 'post'], '/update-color', 'AjaxController@updateColor');
Route::get('/get-ajax', 'AjaxController@getAjax');
Route::get('/get-ajax-form', 'AjaxController@getAjaxForm');

//Route::auth();
// Authentication Routes...
Route::get('login-popup', 'Auth\AuthController@showLoginForm');
Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');

// Registration Routes...
Route::get('register', 'Auth\AuthController@showRegistrationForm');
Route::post('register', 'Auth\AuthController@register');

// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

Route::match(['get', 'post'], '/them-san-pham', 'ProductController@addProduct');
Route::get('/user/detail/{id}', 'UserController@detailUser');

Route::group(['middleware' => 'auth'], function () {
    //User
    Route::get('/user/list', 'UserController@listUser');
    Route::get('/user/delete/{id}', 'UserController@deleteUser');
    Route::get('/user/edit/{pid}', 'UserController@editUser');
    Route::post('/user/edit/{pid}', 'UserController@postEditUser');
    Route::get('/user/detail/{pid}', 'UserController@detailUser');
    
    Route::get('/user/search', 'UserController@searchUser');

    Route::get('/changeprofile/', 'UserController@changeProfile');
    Route::post('/changeprofile/', 'UserController@postChangeProfile');

    //permission
    Route::get('/group/list', 'GroupController@listGroup');
    Route::get('/group/delete/{id}', 'GroupController@deleteGroup');
    Route::get('/group/edit/{id}', 'GroupController@editGroup');
    Route::post('/group/edit/{id}', 'GroupController@postEditGroup');
    Route::get('/group/detail/{id}', 'GroupController@detailGroup');
    Route::get('/group/get-permission/{id}', 'GroupController@getPermissionById');

    //product
    Route::get('/', 'ProductController@listPrice');
    Route::get('/home', 'ProductController@listPrice');
    Route::get('/product/search', 'ProductController@searchProductByName');
    Route::get('/product/check-exist', 'ProductController@getProductIDByName');

    //bang gia
    Route::get('/', 'ProductController@listPrice');
    Route::get('/bang-gia', 'ProductController@listPrice');
    Route::match(['get', 'post'], '/edit-product/{pid}', 'ProductController@editProduct');
    Route::get('/change-lstprice/{id}/{type}', 'ProductController@changeListPriceActive');
    Route::get('/sub-product/{id}', 'ProductController@subProduct');
    Route::match(['get', 'post'], '/update-price/{pid}/{listPriceID}', 'ProductController@updatePrice');
    Route::get('/detete-image/{id}', 'ProductController@deleteImage');
    Route::get('/product/delete/{id}', 'ProductController@deleteProduct');

    Route::post('bang-gia/export', 'ProductController@export2Excel');
    Route::get('bang-gia/import', 'ProductController@import2Excel');
    Route::post('bang-gia/import', 'ProductController@postImport2Excel');
    //dat hang
    Route::get('/goods-orders/list', 'GoodsOrdersController@listGoodsOrders');
    Route::get('/goods-orders/delete/{id}', 'GoodsOrdersController@deleteGoodsOrders');
    Route::get('/goods-orders/edit/{pid}', 'GoodsOrdersController@editGoodsOrders');
    Route::post('/goods-orders/edit/{pid}', 'GoodsOrdersController@postEditGoodsOrders');

    Route::post('/goods-orders/add/{pid}', 'GoodsOrdersController@postEditGoodsOrders');
    Route::get('/goods-orders/find', 'GoodsOrdersController@getGoodsOrdersByID');

    Route::get('/goods-orders/edit-product/{goodsOrdersID}/{productOrderID}', 'GoodsOrdersController@editProduct');
    Route::post('/goods-orders/edit-product/{goodsOrdersID}/{productOrderID}', 'GoodsOrdersController@postEditProduct');
    Route::get('/goods-orders/list-product/{goodsOrdersID}', 'GoodsOrdersController@listProduct');
    Route::get('/goods-orders/deleteProduct/{id}', 'GoodsOrdersController@deleteProduct');

    Route::get('/goods-orders/combine', 'GoodsOrdersController@combineOrders');
    Route::post('/goods-orders/combine', 'GoodsOrdersController@postCombineOrders');

    Route::get('/goods-orders/list-ncc/{id?}', 'GoodsOrdersController@listSupplier');
    Route::get('/goods-orders/edit-ncc/{supplierOrderID}/{goodsOrdersID}', 'GoodsOrdersController@editSupplier');
    Route::post('/goods-orders/edit-ncc/{supplierOrderID}/{goodsOrdersID}', 'GoodsOrdersController@postEditSupplier');
    Route::get('/goods-orders/delete-ncc/{supplierOrderID}/', 'GoodsOrdersController@deleteSupplierOrders');

    Route::get('/goods-orders/change-status/{id}/{statusId}', 'GoodsOrdersController@ChangeGoodsOrderStatus');

    Route::get('/goods-orders/lumped', 'GoodsOrdersController@lumpedOrders');

    //mua hang
    Route::get('/goods-purchase/list', 'GoodsPurchaseController@index')->name('listGoodsPuchase');
    Route::get('/goods-purchase/delete/{id}', 'GoodsPurchaseController@delete')->name('deleteGoodsPuchase');
    Route::get('/goods-purchase/edit/{id}', 'GoodsPurchaseController@edit')->name('editGoodsPuchase');
    Route::post('/goods-purchase/edit/{id}', 'GoodsPurchaseController@postEdit');
    Route::get('/goods-puchase/list-product/{gid}', 'GoodsPurchaseController@getListProduct');
    Route::get('/goods-puchase/download/{fileType}/{gpId}', 'GoodsPurchaseController@download');
    Route::get('goods-puchase/get-goods-orders/{gpId}', 'GoodsPurchaseController@getGoodsOrders');
    //gộp đơn hàng
    Route::get('goods-purchase/lumped-goods-orders/', 'GoodsPurchaseController@lumpedGoodsOrders')->name('lumpedGoodsOrders');
    Route::post('goods-purchase/lumped-goods-orders/', 'GoodsPurchaseController@postLumpedGoodsOrders');

    //bao gia
    Route::get('/bao-gia', 'NotifyPriceController@listNotifyPrice');
    Route::get('/bao-gia/delete/{id}', 'NotifyPriceController@deleteNotifyPrice');
    Route::match(['get', 'post'], '/notify-price/edit/{pid}', 'NotifyPriceController@editNotifyPrice');
    Route::get('/images/{pid}', 'ImagesController@getImageByProductId');
    Route::post('/bao-gia/delete', 'NotifyPriceController@postDelete');
    // bao gia API
    Route::get('/bao-gia/change-status/{id}/{statusId}', 'NotifyPriceController@changeNotifyPriceStatus');
    Route::get('/change-lstnotifyprice/{notifyprice}/{type}', 'NotifyPriceController@changeListNotifyPriceActive');

    // Category
    Route::get('/category/list', 'CategoryController@listCategory');
    Route::get('/category/update-position', 'CategoryController@updatePosition');
    Route::get('/category/edit/{id}', 'CategoryController@editCategory');
    Route::post('/category/edit/{id}', 'CategoryController@postEditCategory');
    Route::get('/category/delete/{id}', 'CategoryController@deleteCategory');

    //chatlieu
    Route::get('/collumn/list', 'ListPriceController@listCollumn');
    Route::get('/collumn/update-position', 'ListPriceController@updatePosition');
    Route::get('/collumn/edit/{id}', 'ListPriceController@editListPrice');
    Route::post('/collumn/edit/{id}', 'ListPriceController@postEditListPrice');
    Route::get('/collumn/delete/{id}', 'ListPriceController@deleteListPrice');

    // Matarial
    Route::get('/chat-lieu/list', 'MaterialController@listMaterial');
    Route::get('/chat-lieu/update-position', 'MaterialController@updatePosition');
    Route::get('/chat-lieu/edit/{id}', 'MaterialController@editMaterial');
    Route::post('/chat-lieu/edit/{id}', 'MaterialController@postEditMaterial');
    Route::get('/chat-lieu/delete/{id}', 'MaterialController@deleteMaterial');

    // Color
    Route::get('/color/list', 'ColorController@listColor');
    Route::get('/color/update-position', 'ColorController@updatePosition');
    Route::get('/color/edit/{id}', 'ColorController@editColor');
    Route::post('/color/edit/{id}', 'ColorController@postEditColor');
    Route::get('/color/delete/{id}', 'ColorController@deleteColor');

    // Goods orders
    Route::get('/status-orders/list', 'StatusOrdersController@listStatus');
    Route::get('/status-orders/update-position', 'StatusOrdersController@updatePosition');
    Route::get('/status-orders/edit/{id}', 'StatusOrdersController@editStatus');
    Route::post('/status-orders/edit/{id}', 'StatusOrdersController@postEditStatus');
    Route::get('/status-orders/delete/{id}', 'StatusOrdersController@deleteStatus');

    // Status price
    Route::get('/status-price/list', 'StatusPriceController@listStatus');
    Route::get('/status-price/update-position', 'StatusPriceController@updatePosition');
    Route::get('/status-price/edit/{id}', 'StatusPriceController@editStatus');
    Route::post('/status-price/edit/{id}', 'StatusPriceController@postEditStatus');
    Route::get('/status-price/delete/{id}', 'StatusPriceController@deleteStatus');

    // Status supplier
    Route::get('/status-supplier/list', 'StatusSupplierController@listStatus');
    Route::get('/status-supplier/update-position', 'StatusSupplierController@updatePosition');
    Route::get('/status-supplier/edit/{id}', 'StatusSupplierController@editStatus');
    Route::post('/status-supplier/edit/{id}', 'StatusSupplierController@postEditStatus');
    Route::get('/status-supplier/delete/{id}', 'StatusSupplierController@deleteStatus');

    //supplier
    Route::get('/supplier/list', 'SupplierController@listSupplier');
    Route::get('/supplier/delete/{id}', 'SupplierController@deleteSupplier');
    Route::get('/supplier/edit/{pid}', 'SupplierController@editSupplier');
    Route::post('/supplier/edit/{pid}', 'SupplierController@postEditSupplier');
    Route::get('/supplier/history/{id}', 'SupplierController@history');
    Route::get('/supplier/debit-history/{sId}', 'SupplierController@showDebitHistory');
    Route::get('/supplier/debit-add-exchange/{sId}', 'SupplierController@createExchangeMoney');
    Route::post('/supplier/debit-add-exchange/{sId}', 'SupplierController@storeExchangeMoney');

    Route::get('/supplier/search', 'SupplierController@searchSupplierByName');
    Route::get('/supplier/check-exist', 'SupplierController@getSupplierIDByName');

    //PackingList
    Route::get('/packing-list/list', 'PackingListController@listPackingList');
    Route::get('/packing-list/delete/{id}', 'PackingListController@deletePackingList');
    Route::get('/packing-list/edit/{pid}', 'PackingListController@editPackingList');
    Route::post('/packing-list/edit/{pid}', 'PackingListController@postEditPackingList');

    //product supplier
    Route::get('/product-supplier/list/{productID}', 'ProductSupplierController@listProductSupplierByProductID');
    Route::get('/product-supplier/delete/{id}', 'ProductSupplierController@delete');
    Route::get('/product-supplier/edit/{psID}/{productID}', 'ProductSupplierController@editProductSupplier');
    Route::post('/product-supplier/edit/{psID}/{productID}', 'ProductSupplierController@postEditProductSupplier');
});
//customer
Route::get('/customer/search', 'CustomerController@getCustomerByID');

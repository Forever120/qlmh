<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MaterialController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listMaterial(Request $request)
    {
        $user = \Auth::user();
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_CHAT_LIEU);
        //check view permission
        if ($listUserPermission === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $htmlList = \ClassMaterial::htmlListMaterial();

        $data = [
            'htmlList' => $htmlList,
            'user' => $user,
            'listUserPermission' => $listUserPermission,
            'selected_menu' => [9, 93],
        ];
        if ($request->reload == 1) {
            return view('Material/listReload', $data);
        } else {
            return view('Material/list', $data);
        }
    }

    public function postEditMaterial(Request $request, $id)
    {
        $result = \ClassMaterial::postEditMaterial($request, $id);
        return $result;
    }

    public function editMaterial(Request $request, $id)
    {
        $data = \ClassMaterial::editMaterial($request, $id);
        return view('Material/edit', $data);
    }

    public function deleteMaterial($id)
    {
        $result = \ClassMaterial::deleteById($id);
        return $result;
    }

    public function updatePosition(Request $request)
    {
        if (isset($request->json)) {
            $json_arr = json_decode($request->json, true);
            try {
                $result = \ClassMaterial::recursivelyUpdatePosition($json_arr);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result == 'success') {
                return 'Update success';
            } else {
                return 'Update error';
            }
        }
    }
}

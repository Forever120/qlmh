<?php

namespace App\Http\Controllers;

//use App\User;
use App\Http\Controllers\Controller;
use App\Model\User;
use Auth;
use Illuminate\Http\Request;

//use App\UserType;

class GroupController extends Controller
{

    public function __construct()
    {
    }

    public function listGroup(Request $request)
    {
        $user = \Auth::user();
        $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_GROUP_VIEW);
        //check view permission
        if ($result === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $data = \ClassGroup::listGroup($result);

        if ($request->reload == 1) {
            return view('Group/listReload', $data);
        } else {
            return view('Group/list', $data);
        }
    }

    public function detailGroup($id)
    {
        $data = \ClassGroup::detailGroup($id);
        return view('Group/detail', $data);
    }

    public function editGroup($id)
    {
        //get detail Group infomation
        $data = \ClassGroup::editGroup($id);
        return view('Group/edit', $data);
    }

    public function postEditGroup(request $request, $id)
    {
        $ret = \ClassGroup::postEditGroup($request, $id);
        return $ret;
    }

    public function deleteGroup($id)
    {
        $ret = \ClassGroup::delete($id);
        return $ret;
    }

    public function getPermissionById($id)
    {
        $groupPermission = \ClassGroup::getPermissionById($id);
        return $groupPermission;
    }
}

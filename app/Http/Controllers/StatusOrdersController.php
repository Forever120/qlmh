<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StatusOrdersController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listStatus(Request $request)
    {
        $user = \Auth::user();
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_STATUS_DON_DAT_HANG);
        //check view permission
        if ($listUserPermission === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $data = \ClassStatusOrder::listStatus($request);
        $data['user'] = $user;
        $data['listUserPermission'] = $listUserPermission;

        if ($request->reload == 1) {
            return view('StatusOrders/listReload', $data);
        } else {
            return view('StatusOrders/list', $data);
        }
    }

    public function postEditStatus(Request $request, $id)
    {
        $ret = \ClassStatusOrder::postEditStatus($request, $id);
        return $ret;
    }

    public function editStatus(Request $request, $id)
    {
        $data = \ClassStatusOrder::editStatus($request, $id);
        return view('StatusOrders/edit', $data);
    }

    public function deleteStatus($id)
    {
        $ret = \ClassStatusOrder::deleteById($id);
        return $ret;
    }

    public function updatePosition(Request $request)
    {
        $ret = \ClassStatusOrder::updatePosition($request);
        return $ret;
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

//import
class ProductController extends Controller
{

    public function __construct()
    {
    }

    public function export2Excel(Request $request)
    {
        if ($request->type == 'export') {
            if (count($request->option) > 0) {
                // if ($request->type == 'import') {
                //     ProductComponent::import2Excel($request->option);
                // } elseif ($request->type == 'export') {
                //get body
                $data = \ClassProduct::dataExport2Excel($request->option);
                \Excel::create('BangGia_' . date('ymd'), function ($excel) use ($data) {
                    $excel->sheet('Bảng giá', function ($sheet) use ($data) {
                        //get header
                        $header = [];
                        $header['id'] = 'Mã SP';
                        $header['name'] = 'Tên sản phẩm';
                        $header['categoryID'] = 'Nhóm sản phẩm';
                        $header['materialID'] = 'Chất liệu';
                        $header['colorID'] = 'Màu sắc';
                        $header['weight'] = 'Khối lượng';
                        $header['volume'] = 'Thể tích';
                        $listPrices = \ClassListPrice::index();
                        foreach ($listPrices as $price) {
                            $header[$price->id] = $price->name;
                        }
                        $header['description'] = 'Mô tả';
                        $header['note'] = 'Ghi chú';
                        $sheet->row(1, $header);
                        $sheet->row(1, function ($row) {
                            $row->setBackground('#448AFF');
                            $row->setFontColor('#FFFFFF');
                            $row->setFontSize(14);
                        });

                        $sheet->setHeight(array(
                            1 => 30,
                        ));

                        $sheet->row(1, $header);

                        $sheet->rows($data);
                    });
                })->export('xls');
            }
            //}
        } elseif ($request->type == 'trash') {
            $data = \ClassProduct::deleteListById($request->option);

            return redirect('/bang-gia');
        }
    }

    public function import2Excel(Request $request)
    {
        if (isset($request->popup) && $request->popup == 1) {
            return view('Product.importPopup');
        }

        // $user = Auth::user();
        // //get all list price
        // $listPrices = ListPriceModel::orderBy('id', 'asc')->get();

        // //format list price
        // $priceArr = [];
        // foreach ($listPrices as $price) {
        //     $priceArr[$price->id] = FormatDataComponent::formatText($price->name, '_');
        // }

        // return view('Product.import', [
        //     'user' => $user,
        //     'listPrices' => $listPrices,
        //     'priceArr' => $priceArr,
        // ]);
    }

    public function postImport2Excel(Request $request)
    {
        $data = \ClassProduct::postImport2Excel($request);
        return view('Product.postImport', $data);
    }

    public function listPrice(Request $request)
    {
        $user = Auth::user();
        $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_BANG_GIA_VIEW);
        //check view permission
        if ($result === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        // Get product
        $data = \ClassProduct::index($request, $result);
        if ($request->reload == 1) {
            return view('Product/list_price_reload', $data);
        } else {
            return view('Product/list_price', $data);
        }
    }

    public function updatePrice(Request $request, $productId, $lstPriceId)
    {
        \ClassProductPrice::update($request->all(), $productId, $lstPriceId);
        return '["success"]';
    }

    public function changeListPriceActive(Request $request, $priceID, $type)
    {
        $result = \ClassCommon::changeUserListActive(\AppConst::LIST_ACTIVE_PRICE_KEY, $priceID, $type);
        return $result;
    }

    public function editProduct(Request $request, $pid)
    {
        if ($request->isMethod('post')) {
            $result = '';
            if ($pid > 0) {
                $result = \ClassProduct::update($request, $pid);
            } else {
                $result = \ClassProduct::store($request);
            }
            return $result;
        }
        $data = \ClassProduct::edit($request, $pid);

        return view('Product/edit_product', $data);
    }

    public function subProduct($pid)
    {
        $user = Auth::user();
        $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_BANG_GIA_VIEW);
        //check view permission
        if ($result === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $data = \ClassProduct::subProduct($pid, $result);
        return view('Product/sub_product', $data);
    }

    public function deleteImage($id)
    {
        $result = \ClassImage::delete($id);
        return $result;
    }

    public function deleteProduct($id)
    {
        $result = \ClassProduct::delete($id);
        return $result;
    }

    public function searchProductByName(Request $request)
    {
        $mapId2Name = \ClassProduct::getMapId2NameByName($request->keyword);
        return $mapId2Name;
    }

    public function getProductIDByName(Request $request)
    {
        $result = \ClassProduct::getProductIDByName($request);
        return $result;
    }
}

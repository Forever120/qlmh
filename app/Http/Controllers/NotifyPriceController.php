<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\NotifyPriceModel;

class NotifyPriceController extends Controller
{

    public function __construct()
    {
    }

    public function listNotifyPrice(Request $request)
    {
        $user = Auth::user();
        //check all permission
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_BANG_GIA_VIEW);
        if ($listUserPermission == false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $data = \ClassNotifyPrice::index($request, $listUserPermission);
        if ($request->reload == 1) {
            return view('NotifyPrice/list_reload', $data);
        } else {
            return view('NotifyPrice/list', $data);
        }
    }

    public function editNotifyPrice(Request $request, $nid)
    {
        if ($request->isMethod('post')) {
            $result = \ClassNotifyPrice::storeOrUpdate($request, $nid);
            return $result;
        }

        $data = \ClassNotifyPrice::edit($request, $nid);
        return view('NotifyPrice/edit', $data);
    }

    public function deleteNotifyPrice($id)
    {
        if ($id > 0) {
            NotifyPriceModel::find($id)->delete();
        }
        return 'success';
    }

    public function changeNotifyPriceStatus($id, $statusId)
    {
        DB::table('zzz_notify_price')
            ->where('id', $id)
            ->update(['statusID' => $statusId]);
    }

    public function changeListNotifyPriceActive($listNotifyPrice, $type)
    {
        $result = \ClassCommon::changeUserListActive(\AppConst::LIST_ACTIVE_NOTIFY_PRICE_KEY, $listNotifyPrice, $type);
        return $result;
    }
}

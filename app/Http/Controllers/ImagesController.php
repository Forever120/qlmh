<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ImagesController extends Controller
{
    public function __construct()
    {
    }

    public function getImageByProductId($pid)
    {
        $images = \ClassImage::getByPid($pid);
        return view('Images/list_images', [
            'images' => $images,
        ]);
    }
}

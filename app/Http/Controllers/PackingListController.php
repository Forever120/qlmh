<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\PackingListModel;
use App\Models\UserPermissonModel;
use Auth;
use Illuminate\Http\Request;

class PackingListController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listPackingList(Request $request)
    {
        //read account info
        $user = Auth::user();

        //get all permission for user
        $listUserPermission = [];
        $userPermission = UserPermissonModel::where('userID', '=', $user->id)->get();
        foreach ($userPermission as $per) {
            $listUserPermission[] = $per->permissionID;
        }

        //check view permission
        if (!in_array('PACKING_LIST_VIEW', $listUserPermission)) {
            return view('element/notPermission', [
                'user' => $user,
            ]);
        }

        $packingList = PackingListModel::orderBy('id', 'desc')->paginate(30);
        $data = [
            'packingList' => $packingList,
            'listUserPermission' => $listUserPermission,
            'user' => $user,
            'selected_menu' => [4],
        ];

        if ($request->reload == 1) {
            return view('PackingList/listReload', $data);
        } else {
            return view('PackingList/list', $data);
        }
    }

    public function editPackingList(Request $request, $id)
    {
        $user = Auth::user();

        //get all permission for user
        $listUserPermission = [];
        $userPermission = UserPermissonModel::where('userID', '=', $user->id)->get();
        foreach ($userPermission as $per) {
            $listUserPermission[] = $per->permissionID;
        }

        $packingList = PackingListModel::find($id);

        return view('PackingList/edit', [
            'packingList' => $packingList,
            '$user' => $user,
            'listUserPermission' => $listUserPermission,
            'id' => $id,
        ]);
    }

    public function postEditPackingList(Request $request, $id)
    {
        $user = Auth::user();

        //get all permission for user
        $listUserPermission = [];
        $userPermission = UserPermissonModel::where('userID', '=', $user->id)->get();
        foreach ($userPermission as $per) {
            $listUserPermission[] = $per->permissionID;
        }

        if ($id > 0) {
            $packingList = PackingListModel::find($id);
        } else {
            $packingList = new PackingListModel;
        }
        $packingList->name = $request->name;
        $packingList->count = $request->count;
        $packingList->desciption = $request->desciption;
        $packingList->save();

        return '["success"]';
    }

    public function deletePackingList($id)
    {
        if ($id > 0) {
            PackingListModel::find($id)->delete();
        }
        return 'success';
    }

}

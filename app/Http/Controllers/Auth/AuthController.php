<?php 
namespace App\Http\Controllers\Auth;
//use Illuminate\Http\LoginRequest;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Auth;
//use Illuminate\Routing\Controller;
//use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


use App\User;

//use App\Http\Requests\LoginRequest;

class AuthController extends Controller {

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
//    public function authenticate()
//    {
//        if (Auth::attempt(['email' => $email, 'password' => $password]))
//        {
//            return redirect()->intended('dashboard');
//        }
//    }
    
    public function showLoginForm()
    {
        if (!Auth::check()) {
            //die(var_dump(Auth::check()));
            return view('auth/login');
        } else {
            return redirect('home');
        }
    }
    public function login(LoginRequest $request)
    {
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password]))
        {
            Auth::login(Auth::user(), true);
            return redirect('home');
        }
        return redirect()->back();
    }
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
    
    public function showRegistrationForm()
    {
        return view('auth/register');
    }
    
    public function register(RegisterRequest $request)
    {
//        var_dump($request->username);
//        var_dump($request['userType']);
//        var_dump($request['password']);
        //die;
        return User::create([
            'username' => $request['username'],
            'userType' => $request['userType'],
            'password' => bcrypt($request['password']),
        ]);
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class GoodsPurchaseController extends Controller {

    public function __construct() {
        //todo:
    }

    public function index(Request $request) {
        //read account info
        $user = Auth::user();
        $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_DMH_VIEW);

        if ($result === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }
        $data = \ClassGoodsPurchase::index($request, $result, $user);

        if ($request->reload == 1) {
            return view('GoodsPurchase/listReload', $data);
        } else {
            return view('GoodsPurchase/list', $data);
        }
    }

    public function edit(Request $request, $id) {

        $listGoodsOrders = \ClassGoodsPurchase::getListGoodsOrdersID($id);
        $data = \ClassGoodsPurchase::lumpedGoodsOrders($listGoodsOrders);

        $data['goodsPuchase'] = \ClassGoodsPurchase::findById($id);
        $data['nvmh'] = \ClassUser::findById($data['goodsPuchase']->nvmhID);
        $data['listSupplierOrders'] = \ClassSupplier::findByIdAndChangeAttributeName($data['goodsPuchase']->supplierID);
        $data['mapPIdToLumpedProduct'] = \ClassGoodsPurchase::mapId2ProductPurchase($data['goodsPuchase']->id);

        $data['action'] = '/goods-purchase/edit/' . $id;
        return view('GoodsPurchase/lumpedGoodsOrders', $data);
    }

    public function postEdit(Request $request, $id) {
        $data = \ClassGoodsPurchase::postLumpedGoodsOrders($request, $id);

        return response()->json($data);
    }

    public function delete($id) {
        if ($id > 0) {
            \ClassGoodsPurchase::deleteById($id);
            \ClassGoodsOrder::unUpdateGoodsOrderToLumpedByGpId($id);
        }
        return 'success';
    }

    public function lumpedGoodsOrders(Request $request) {
        if (empty($request->data)) {
            return "Bạn chưa chọn đơn hàng để gộp";
        }

        $data = \ClassGoodsPurchase::lumpedGoodsOrders($request->data);
        $data['action'] = route('lumpedGoodsOrders');

        return view('GoodsPurchase/lumpedGoodsOrders', $data);
    }

    public function postLumpedGoodsOrders(Request $request) {

        $data = \ClassGoodsPurchase::postLumpedGoodsOrders($request);
        return response()->json($data);
    }

    public function getListProduct(Request $request, $goodsPuchaseID) {

        $data = \ClassGoodsPurchase::getListProduct($request, $goodsPuchaseID);

        return view('GoodsPurchase/listProduct', $data);
    }

    public function download($fileType, $gpID) {
        $goodsPurchase = \ClassGoodsPurchase::findById($gpID);
        $file = public_path() . '/' . $goodsPurchase->$fileType;
        return response()->download($file);
    }
    
    public function getGoodsOrders($gpID) {
        $data = \ClassGoodsOrder::getGoodsOrders($gpID);
        return view('GoodsPurchase/listGoodsOrders', $data);
    }
}

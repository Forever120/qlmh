<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\GoodsOrdersModel;
use App\Models\StatusOrdersModel;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GoodsOrdersController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listGoodsOrders(Request $request)
    {
        //read account info
        $user = \Auth::user();

        if (!empty($request->viewType) && $request->viewType == \AppConst::VIEW_TYPE_ALL) {
            // If user have permisson view newest
            $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_DDH_VIEW_ALL);
        } else {
            $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_DDH_VIEW);
        }
        // if (!empty($request->viewType) && $request->viewType == \AppConst::VIEW_TYPE_ALL) {
        //     // If user have permisson view newest
        //     $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_DDH_VIEW_ALL);
        // } else {
        $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_DDH_VIEW);
        // }
        
        if ($result === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }
        
        $data = \ClassGoodsOrder::index($request, $result, $user);
        if ($request->reload == 1) {
            return view('GoodsOrder/listReload', $data);
        } else {
            return view('GoodsOrder/list', $data);
        }
    }

    public function postEditGoodsOrders(Request $request, $id)
    {
        $result = \ClassGoodsOrder::postEditGoodsOrders($request, $id);
        return response()->json($result);
    }

    public function editGoodsOrders(Request $request, $id)
    {
        $data = \ClassGoodsOrder::editGoodsOrders($request, $id);
        return view('GoodsOrder/edit', $data);
    }

    public function addGoodsOrders(Request $request, $id)
    {
    }

    public function listSupplier($goodsOrdersID = 0)
    {
        $data = \ClassGoodsOrder::listSupplier($goodsOrdersID);
        if (!is_array($data)) {
            return $data;
        }
        return view('GoodsOrder/listSupplier', $data);
    }

    public function editSupplier(Request $request, $supplierOrderID = 0, $goodsOrdersID = 0)
    {
        $data = \ClassGoodsOrder::editSupplier($request, $supplierOrderID, $goodsOrdersID);
        return view('GoodsOrder/editSupplier', $data);
    }

    public function postEditSupplier(Request $request, $supplierOrderID, $goodsOrdersID)
    {
        $result = \ClassGoodsOrder::postEditSupplier($request, $supplierOrderID, $goodsOrdersID);
        return response()->json($result);
    }

    public function deleteSupplierOrders($id)
    {
        $ret = \ClassGoodsOrder::deleteSupplierOrders($id);
        return $ret;
    }

    //list product
    public function listProduct($goodsOrdersID)
    {
        $data = \ClassGoodsOrder::listProduct($goodsOrdersID);
        return view('GoodsOrder/listProduct', $data);
    }

    public function editProduct(Request $request, $goodsOrdersID, $productOrderID)
    {
        $data = \ClassGoodsOrder::editProduct($request, $goodsOrdersID, $productOrderID);
        if (!is_array($data)) {
            return $data;
        }
        return view('GoodsOrder/editProduct', $data);
    }

    public function postEditProduct(Request $request, $goodsOrdersID, $productOrderID)
    {
        //validation
        $result = \ClassGoodsOrder::postEditProduct($request, $goodsOrdersID, $productOrderID);
        return response()->json($result);
    }

    public function deleteGoodsOrders($id)
    {
        $ret = \ClassGoodsOrder::deleteById($id);
        return $ret;
    }

    public function deleteProduct($id)
    {
        $ret = \ClassGoodsOrder::deleteProductOrder($id);
        return $ret;
    }

    public function combineOrders()
    {

        $listStatus = StatusOrdersModel::all();
        return view('GoodsOrder/combineOrders', [
            'listStatus' => $listStatus,
        ]);
    }

    public function postCombineOrders()
    {

        return view('GoodsOrder/combineOrders', [
            'productOrder' => $productOrder,
        ]);
    }

    public function getGoodsOrdersByID(Request $request)
    {
        //read account info
        $user = \Auth::user();

        $result = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_DDH_VIEW);
        // if ($result === false) {
        //     return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
        //         'user' => $user,
        //     ]);
        // }

        $result = \ClassGoodsOrder::getGoodsOrdersByID($request);
        return response()->json($result);
    }

    public function ChangeGoodsOrderStatus($id, $statusId)
    {
        DB::table('zzz_goods_orders')
            ->where('id', $id)
            ->update(['statusID' => $statusId]);
    }

    public function lumpedOrders()
    {
        if ($request->suplierDeliveryDate01 == '') {
            return '["error","Bạn chưa chọn ngày nhận hàng từ nhà cung cấp"]';
        }
        if ($request->deliveryToUserDate01 == '') {
            return '["error","Bạn chưa chọn ngày dao hàng cho khách"]';
        }

        //save goods orders
        $parent = new GoodsOrdersModel;
        $parent->statusID = $request->statusID;
        $parent->note = $request->note;
        $parent->deliveryToUserDate = $request->deliveryToUserDate01;
        $parent->suplierDeliveryDate = $request->suplierDeliveryDate01;
        return '["success"]';
    }
}

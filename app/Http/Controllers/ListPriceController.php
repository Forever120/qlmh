<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ListPriceController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listCollumn(Request $request)
    {
        $user = \Auth::user();
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_COLLUMN);
        //check view permission
        if ($listUserPermission === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $data = \ClassListPrice::listCollumn($request);
        $data['user'] = $user;
        $data['listUserPermission'] = $listUserPermission;
        if ($request->reload == 1) {
            return view('ListPrice/listReload', $data);
        } else {
            return view('ListPrice/list', $data);
        }
    }

    public function postEditListPrice(Request $request, $id)
    {
        $ret = \ClassListPrice::postEditListPrice($request, $id);
        return $ret;
    }

    public function editListPrice(Request $request, $id)
    {
        $data = \ClassListPrice::editListPrice($request, $id);
        return view('ListPrice/edit', $data);
    }

    public function deleteListPrice($id)
    {
        $result = \ClassListPrice::deleteById($id);
        return $result;
    }

    public function updatePosition(Request $request)
    {
        if (isset($request->json)) {
            $json_arr = json_decode($request->json, true);
            try {
                $result = \ClassListPrice::recursivelyUpdatePosition($json_arr);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result == 'success') {
                return 'Update success';
            } else {
                return 'Update error';
            }
        }
    }
}

<?php
// Just for test Ajax
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AjaxController extends Controller
{

    public function __construct()
    {
    }

    public function getAjax()
    {
        return ['a' => 1];
    }

    public function getAjaxForm()
    {
        return view('Color\ajax_form');
    }

    public function updateColor(Request $request)
    {
        //return $request->input('userName');
        return $request->all();
    }
}

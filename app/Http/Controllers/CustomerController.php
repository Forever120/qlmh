<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function getCustomerByID(Request $request)
    {
        // $user = \Auth::user();
        // $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_CUSTOMER);
        // //check view permission
        // if ($listUserPermission === false) {
        //     return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
        //         'user' => $user,
        //     ]);
        // }

        $result = \ClassCustomer::getCustomerByID($request->cid);

        return $result;
    }
}

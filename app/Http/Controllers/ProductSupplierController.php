<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\PackingListModel;
use Illuminate\Http\Request;

class ProductSupplierController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listProductSupplierByProductID(Request $request, $productID)
    {
        $data = \ClassProductSupplier::listProductSupplierByProductID($request, $productID);
        return view('ProductSupplier/list', $data);
    }

    public function editProductSupplier(Request $request, $psID, $productID)
    {
        $data = \ClassProductSupplier::editProductSupplier($request, $psID, $productID);
        return view('ProductSupplier/edit', $data);
    }

    public function postEditProductSupplier(Request $request, $psID, $productID)
    {
        $ret = \ClassProductSupplier::postEditProductSupplier($request, $psID, $productID);
        return $ret;
    }

    public function deletePackingList($id)
    {
        if ($id > 0) {
            PackingListModel::find($id)->delete();
        }
        return 'success';
    }
}

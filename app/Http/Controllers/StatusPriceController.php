<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class StatusPriceController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listStatus(Request $request)
    {
        $user = \Auth::user();
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_STATUS_BAO_GIA);
        //check view permission
        if ($listUserPermission === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $data = \ClassStatusPrice::listStatus($request);
        $data['user'] = $user;
        $data['listUserPermission'] = $listUserPermission;
        if ($request->reload == 1) {
            return view('StatusPrice/listReload', $data);
        } else {
            return view('StatusPrice/list', $data);
        }
    }

    public function postEditStatus(Request $request, $id)
    {
        $ret = \ClassStatusPrice::postEditStatus($request, $id);
        return $ret;
    }

    public function editStatus(Request $request, $id)
    {
        $data = \ClassStatusPrice::editStatus($request, $id);
        return view('StatusPrice/edit', $data);
    }

    public function deleteStatus($id)
    {
        $result = \ClassStatusPrice::deleteById($id);
        return $result;
    }

    public function updatePosition(Request $request)
    {
        if (isset($request->json)) {
            $json_arr = json_decode($request->json, true);
            try {
                $result = \ClassStatusPrice::recursivelyUpdatePosition($json_arr);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result == 'success') {
                return 'Update success';
            } else {
                return 'Update error';
            }
        }
    }
}

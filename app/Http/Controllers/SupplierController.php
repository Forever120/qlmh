<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listSupplier(Request $request)
    {
        $user = \Auth::user();
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_DOI_TAC_VIEW);
        //check view permission
        if ($listUserPermission === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $data = \ClassSupplier::index($request);
        $data['listUserPermission'] = $listUserPermission;
        if ($request->reload == 1) {
            return view('Supplier/listReload', $data);
        } else {
            return view('Supplier/list', $data);
        }
    }

    public function postEditSupplier(Request $request, $id)
    {
        $ret = \ClassSupplier::postEditSupplier($request, $id);
        return $ret;
    }

    public function editSupplier(Request $request, $id)
    {
        $data = \ClassSupplier::editSupplier($request, $id);
        return view('Supplier/edit', $data);
    }

    public function deleteSupplier($id)
    {
        $ret = \ClassSupplier::deleteById($id);
        return $ret;
    }

    public function history($sid)
    {
        $productPriceOfSupplier = DB::table('zzz_product_price_of_supplier')
            ->select(array(
                'zzz_product_price_of_supplier.id as ppsID',
                'zzz_product_price_of_supplier.*',
                'zzz_status_supplier.name as sName',
                'zzz_list_price.name as lpName',
                'zzz_product.name as pName',
                'zzz_product.id as pid',
            ))
            ->leftJoin('zzz_product', 'zzz_product_price_of_supplier.productID', '=', 'zzz_product.id')
            ->leftJoin('zzz_list_price', 'zzz_product_price_of_supplier.listPriceID', '=', 'zzz_list_price.id')
            ->leftJoin('zzz_status_supplier', 'zzz_product_price_of_supplier.statusSupplierID', '=', 'zzz_status_supplier.id')
            ->where('supplierID', '=', $sid)
            ->get();

        return view('Supplier/history', [
            'productPriceOfSupplier' => $productPriceOfSupplier,
        ]);
    }

    public function searchSupplierByName(Request $request)
    {
        $suppliers = \ClassSupplier::searchSupplierByName($request);
        return json_encode($suppliers);
    }

    public function getSupplierIDByName(Request $request)
    {
        $result = \ClassSupplier::getSupplierIDByName($request);
        return $result;
    }

    public function showDebitHistory(Request $request, $sId)
    {
        $data = \ClassSupplier::showDebitHistory($request, $sId);
        return view('Supplier/debitNoteHistory', $data);
    }

    public function createExchangeMoney(Request $request, $sId)
    {
        $data = \ClassSupplier::createExchangeMoney($request, $sId);
        return view('Supplier/addNewDebitExchangeMoney', $data);
    }
    public function storeExchangeMoney(Request $request, $sId)
    {
        $data = \ClassSupplier::storeExchangeMoney($request, $sId);
        return response()->json($data);
    }
}

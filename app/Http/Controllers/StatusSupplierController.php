<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class StatusSupplierController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listStatus(Request $request)
    {
        $user = Auth::user();
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_STATUS_NCC);
        //check view permission
        if ($listUserPermission === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $data = \ClassStatusSupplier::listStatus($request);
        $data['user'] = $user;
        $data['listUserPermission'] = $listUserPermission;

        if ($request->reload == 1) {
            return view('StatusSupplier/listReload', $data);
        } else {
            return view('StatusSupplier/list', $data);
        }
    }

    public function postEditStatus(Request $request, $id)
    {
        $ret = \ClassStatusSupplier::postEditStatus($request, $id);
        return $ret;
    }

    public function editStatus(Request $request, $id)
    {
        $data = \ClassStatusSupplier::editStatus($request, $id);
        return view('StatusSupplier/edit', $data);
    }

    public function deleteStatus($id)
    {
        $ret = \ClassStatusSupplier::deleteById($id);
        return $ret;
    }

    public function updatePosition(Request $request)
    {
        if (isset($request->json)) {
            $json_arr = json_decode($request->json, true);
            try {
                $result = \ClassStatusSupplier::recursivelyUpdatePosition($json_arr);
            } catch (\Exception $e) {
                return $e->getMessage();
            }

            if ($result == 'success') {
                return 'Update success';
            } else {
                return 'Update error';
            }
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listCategory(Request $request)
    {
        $user = \Auth::user();
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_CATEGORY);
        //check view permission
        if ($listUserPermission === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $htmlList = \ClassCategory::htmlListCategory(0);
        $data = [
            'htmlList' => $htmlList,
            'user' => $user,
            'listUserPermission' => $listUserPermission,
            'selected_menu' => [9, 91],
        ];

        if ($request->reload == 1) {
            return view('Category/listReload', $data);
        } else {
            return view('Category/list', $data);
        }
    }

    public function postEditCategory(Request $request, $id)
    {
        $result = \ClassCategory::postEditCategory($request, $id);
        return $result;
    }

    public function editCategory(Request $request, $id)
    {
        $data = \ClassCategory::editCategory($request, $id);
        return view('/Category/edit', $data);
    }

    public function deleteCategory($id)
    {
        $result = \ClassCategory::deleteById($id);
        return $result;
    }

    public function updatePosition(Request $request)
    {
        if (isset($request->json)) {
            $json_arr = json_decode($request->json, true);
            try {
                $result = \ClassCategory::recursivelyUpdatePosition($json_arr);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result == 'success') {
                return 'Update success';
            } else {
                return 'Update error';
            }
        }
    }
}

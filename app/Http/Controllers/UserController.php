<?php

namespace App\Http\Controllers;

//use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//use App\UserType;

class UserController extends Controller {

    public function __construct() {
        
    }

    public function listUser(Request $request) {
        $user = \Auth::user();
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_USER_VIEW);
        //check view permission
        if ($listUserPermission === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $data = \ClassUser::index($request);
        $data['user'] = $user;
        $data['listUserPermission'] = $listUserPermission;

        if ($request->reload == 1) {
            return view('User/listReload', $data);
        } else {
            return view('User/list', $data);
        }
    }

    public function detailUser($id) {
        $user = \ClassUser::detailUser($id);
        return view('User/detail', [
            'user' => $user,
        ]);
    }

    public function editUser($id) {
        $data = \ClassUser::editUser($id);
        return view('User/edit', $data);
    }

    public function postEditUser(request $request, $id) {
        $ret = \ClassUser::postEditUser($request, $id);
        return $ret;
    }

    public function deleteUser($id) {
        $ret = \ClassUser::deleteById($id);
        return $ret;
    }

    public function changeProfile() {
        $data = \ClassUser::changeProfile();
        return view('User/changeProfile', $data);
    }

    public function postChangeProfile(request $request) {
        $data = \ClassUser::postChangeProfile($request);
        return view('User/changeProfile', $data);
    }

    public function searchUser(Request $request) {
        $listUser = \ClassUser::searchUser($request);

        return json_encode($listUser);
    }

}

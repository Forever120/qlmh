<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class ColorController extends Controller
{

    public function __construct()
    {
        //todo:
    }

    public function listColor(Request $request)
    {
        $user = Auth::user();
        $listUserPermission = \ClassUserPermisson::checkUserPermisson(\AppConst::PERMISSON_MAU_SAC);
        //check view permission
        if ($listUserPermission === false) {
            return view(\AppConst::VIEW_NOT_HAVE_PERMISSON, [
                'user' => $user,
            ]);
        }

        $htmlList = \ClassColor::htmlListColor();

        $data = [
            'htmlList' => $htmlList,
            'user' => $user,
            'listUserPermission' => $listUserPermission,
            'selected_menu' => [9, 92],
        ];

        if ($request->reload == 1) {
            return view('Color/listReload', $data);
        } else {
            return view('Color/list', $data);
        }
    }

    public function postEditColor(Request $request, $id)
    {
        $result = \ClassColor::postEditColor($request, $id);
        return $result;
    }

    public function editColor(Request $request, $id)
    {
        $data = \ClassColor::editColor($request, $id);
        return view('Color/edit', $data);
    }

    public function deleteColor($id)
    {
        $result = \ClassColor::deleteById($id);
        return $result;
    }

    public function updatePosition(Request $request)
    {
        if (isset($request->json)) {
            $json_arr = json_decode($request->json, true);
            try {
                $result = \ClassColor::recursivelyUpdatePosition($json_arr);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            if ($result == 'success') {
                return 'Update success';
            } else {
                return 'Update error';
            }
        }
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class StatusSupplierServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            'ClassStatusSupplier', function () {
                return new \App\Services\ClassStatusSupplier\ClassStatusSupplier;
            }
        );
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MaterialServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            'ClassMaterial', function () {
                return new \App\Services\ClassMaterial\ClassMaterial;
            }
        );
    }
}

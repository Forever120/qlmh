<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class StatusOrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            'ClassStatusOrder', function () {
                return new \App\Services\ClassStatusOrder\ClassStatusOrder;
            }
        );
    }
}

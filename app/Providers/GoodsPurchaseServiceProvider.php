<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GoodsPurchaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            'ClassGoodsPurchase', function () {
                return new \App\Services\ClassGoodsPurchase\ClassGoodsPurchase;
            }
        );
    }
}

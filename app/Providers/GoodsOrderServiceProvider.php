<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GoodsOrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            'ClassGoodsOrder', function () {
                return new \App\Services\ClassGoodsOrder\ClassGoodsOrder;
            }
        );
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UserPermissonServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            'ClassUserPermisson', function () {
                return new \App\Services\ClassUserPermisson\ClassUserPermisson;
            }
        );
    }
}

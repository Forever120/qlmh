<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PackingListServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            'ClassPackingList', function () {
                return new \App\Services\ClassPackingList\ClassPackingList;
            }
        );
    }
}

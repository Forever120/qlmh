<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ListPriceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            'ClassListPrice', function () {
                return new \App\Services\ClassListPrice\ClassListPrice;
            }
        );
    }
}

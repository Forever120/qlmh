<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'zzz_user';
    protected $fillable = [
        'username', 
        'userType', 
        'password',
        'firstName',
        'lastName',
        'email',
        'phoneNumber',
        'skypeAccount',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

@extends('Layouts.main')

@section('title')
Pavico - Phần mềm quản lý bán hàng 
@stop

@section('content')

<div class="content_left">
    <div class="item_left">
        <div class="title_left"> Lọc dữ liệu </div>
        <ul class="">
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            </li>
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            </li>
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            </li>
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            </li>
        </ul>
    </div>

    <div class="item_left">
        <div class="title_left"> Lọc dữ liệu </div>
        <ul class="">
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            </li>
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            </li>
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            </li>
            <li>
                <input type="checkbox">
                <label>Nhóm sản phẩm</label>
            </li>
        </ul>
    </div>
</div>
<div class="content_right">
    <div class="custom">
        <div class="_left"> QUẢN LÝ SẢN PHẨM</div>
        <div class="_right">
            <input type="button" value="export">
            <input type="button" value="export">
            <input type="button" value="export">
        </div>
    </div>
    <div class="data_table">
        <table class="table_quanly">
            <thead>
                <tr>
                    <th><b><a>STT</a></b></th>
                    <th><b>Tên sản phẩm</b></th>
                    <th><b>Đóng gói</b></th>
                    <th><b>Bán lẻ</b></th>
                    <th><b>Giá nhập(1 SP)</b></th>
                    <th><b>50.000</b></th>
                    <th><b>20.000</b></th>
                </tr>
            </thead>
            <tbody>
                <tr class="tr_0">
                    <td>1</td>
                    <td>
                        xxxx
                    </td>
                    <td>
                        xxxx                    
                    </td>
                    <td>
                        xxx
                    </td>
                    <td >
                        xxx
                    </td>
                    <td class="price_1 pointer" >1249000</td>
                    <td class="price_default_1 pointer" >6</td>
                </tr>
                <tr class="tr_0">
                    <td>1</td>
                    <td>
                        xxxx
                    </td>
                    <td>
                        xxxx                    
                    </td>
                    <td>
                        xxx
                    </td>
                    <td >
                        xxx
                    </td>
                    <td class="price_1 pointer" >1249000</td>
                    <td class="price_default_1 pointer" >6</td>
                </tr>
                <tr>
                    <td colspan="11"> 
                        <div style="width:100%; float:left; margin-left:10px">
                            <span class="disabled">&lt;&lt;</span>
                            <span class="current">1</span> 
                            | 
                            <span><a href="">2</a></span>
                        </div>
                    </td>
                </tr>
            </tbody> 
        </table>
    </div>

</div>

@stop
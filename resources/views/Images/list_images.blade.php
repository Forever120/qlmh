<?php
$imgIdx = 0;
?>
@if(count($images) > 0)
@foreach($images as $img)
<?php $imgIdx++; ?>
<img class="{{ $imgIdx == 1 ? 'img_large' : 'img_thumb' }}" 
     src="{{ url($img->path) }}">
@endforeach
@else
<img class="no_img" src="/imgs/no_image.jpg">
@endif
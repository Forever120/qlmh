<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Họ tên</th>
            <th>Tên đăng nhập</th>
            <th>Điện thoại</th>
            <th>Email</th>
            <th>Skyper</th>
            <th>Tùy chọn</th>
        </tr>
    </thead>.
    <tbody>
        <?php $idx = 0 ?>
        @foreach($listUser as $user)
        <?php $idx++ ?>
        <tr class="_pointer">
            <td onclick="loadData('.modal-content02', '/user/detail/{{ $user->id }}')"
                data-toggle="modal" 
                data-target="#myModal">NV-{{ $user->id }}</td>
            <td onclick="loadData('.modal-content02', '/user/detail/{{ $user->id }}')"
                data-toggle="modal" 
                data-target="#myModal">{{ $user->fullname }}</td>
            <td onclick="loadData('.modal-content02', '/user/detail/{{ $user->id }}')"
                data-toggle="modal" 
                data-target="#myModal">{{ $user->username }}</td>
            <td onclick="loadData('.modal-content02', '/user/detail/{{ $user->id }}')"
                data-toggle="modal" 
                data-target="#myModal">{{ $user->phone }}</td>
            <td onclick="loadData('.modal-content02', '/user/detail/{{ $user->id }}')"
                data-toggle="modal" 
                data-target="#myModal">{{ $user->email }}</td>
            <td onclick="loadData('.modal-content02', '/user/detail/{{ $user->id }}')"
                data-toggle="modal" 
                data-target="#myModal">{{ $user->skype }}</td>
            <td>
                @if(in_array('USER_EDIT', $listUserPermission))
                <a onclick="loadData('.modal-content02', '/user/edit/{{ $user->id }}')"
                   data-toggle="modal" 
                   data-target="#myModal">
                    <i data-pack="default" class="ion-edit"></i>
                </a>
                @endif

                &nbsp;

                @if(in_array('USER_DELETE', $listUserPermission))
                <a onclick="deleteRow('/user/delete/{{ $user->id }}', '/user/list')">
                    <i data-pack="default" class="ion-trash-a"></i>
                </a>
                @endif
            </td>
        </tr>
        @endforeach
        <tr>
            <td colspan="7">{!! $listUser ->render() !!}</td>
        </tr>
    </tbody>
</table>
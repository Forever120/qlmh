@if(!isset($_GET['tab'])) 
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Thông tin chi tiết nhân viên</h4>
</div>
@else 
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#tab" 
           aria-controls="home" 
           role="tab" 
           data-toggle="tab">Thông tin nhân viên</a>
    </li>
</ul>
@endif
<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <div class="card-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td><a class="ion-person ion-person mr"></a>Họ tên</td>
                                <td>{{ $user->fullname or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-person ion-person mr"></a>Tên đăng nhập</td>
                                <td>{{ $user->username or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-email mr"></a>Email</td>
                                <td>{{ $user->email or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-ios-telephone mr"></a>Điện thoại</td>
                                <td>{{ $user->phoneNumber or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-ios-telephone mr"></a>Skyper</td>
                                <td>{{ $user->skypeAccount or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-android-person mr"></a>Ngày sinh</td>
                                <td>{{ $user->birthday or '' }}</td>
                            </tr>
                            <tr>
                                <td><a class="ion-location icon-fw mr"></a>Địa chỉ</td>
                                <td><span class="is-editable text-inherit">{{ $user->address or '' }}</span></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
</div>
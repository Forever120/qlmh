<?php
$perJson = file_get_contents(JSON_PERMISSION_FILE);
$permission = json_decode($perJson, true);
?>
<div class="row">
  <div class="col-sm-6">
    <div class="mda-form-control">
      <select name="userType"
        required=""
        tabindex="0"
        aria-required="true"
        aria-invalid="true"
        class="form-control input-sm">
        <option value="2" {{ (isset($user->userType) && $user->userType == 2) ? 'selected' : '' }}> Nhân viên kinh doanh </option>
        <option value="3" {{ (isset($user->userType) && $user->userType == 3) ? 'selected' : '' }}> Nhân viên mua hàng </option>
        <option value="1" {{ (isset($user->userType) && $user->userType == 1) ? 'selected' : '' }}> Admin </option>
      </select>
      <div class="mda-form-control-line"></div>
      <label>Chọn loại nhân viên</label>
    </div>
  </div>
    <div class="col-sm-6">
    <div class="mda-form-control">
        <select name="groupID"
                required=""
                tabindex="0"
                aria-required="true"
                aria-invalid="true"
                onchange="changePermisson(this, '{{ url('group/get-permission') }}')"
                class="form-control input-sm">
            <option value="0">Chọn nhóm quyền</option>
            @foreach($listGroup as $group)
                @php
                if (isset($user->groupID) && $group->id == $user->groupID) {
                    $selected = ' selected ';
                } else {
                    $selected = '';
                }
                @endphp
                <option {{ $selected }} value="{{ $group->id }}">{{ $group->name }}</option>
            @endforeach
        </select>
        <div class="mda-form-control-line"></div>
        <label>Chọn nhóm quyền</label>
    </div>
    </div>
</div>
<br>
@foreach($permission['Permission'] as $per)
<div class="row">
    <div class="mda-form-control">
        <label>{{ $per['name'] }}</label>
    </div>
</div>
<div class="row">
    @foreach($per['sub'] as $sub)
    <?php
//    print_r($listPermission);die;
if (in_array($sub['id'], $listPermission)) {
    $checked = " checked ";
} else {
    $checked = '';
}
?>
    <div class="col-sm-4">
        <div class="checkbox c-checkbox">
            <label class="small">
                <input {{ $checked }} name="permission[]" type="checkbox" value="{{ $sub['id'] }}">
                <span class="ion-checkmark-round"></span>
                {{ $sub['name'] }}
            </label>
        </div>
    </div>
    @endforeach
</div>
@endforeach

<!--list price-->
<div class="row">
    <div class="mda-form-control">
        <label>Quyền sửa collumn bảng giá</label>
    </div>
</div>
<div class="row">
    @foreach($listPrice as $lp)
    <?php
if (in_array('COLLUMN_' . $lp->id, $listPermission)) {
    $checked = " checked ";
} else {
    $checked = '';
}
?>
    <div class="col-sm-4">
        <div class="checkbox c-checkbox">
            <label class="small">
                <input {{ $checked }} name="permission[]" type="checkbox" value="COLLUMN_{{ $lp->id }}">
                <span class="ion-checkmark-round"></span>
                {{ $lp->name }}
            </label>
        </div>
    </div>
    @endforeach
</div>

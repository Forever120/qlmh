@extends('Layouts.main')

@section('title')
Thay đổi thông tin cá nhân
@stop

@section('breadcrumb')
Thay đổi thông tin cá nhân
@stop

@section('avatar')
@if(isset($user->avatar))
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('content')
<div class="container-fluid">
    <div class="card">

        <!-- Tab panes -->
        <div class="modal-body">
            <div class="card">
                <form id="form-register" 
                      action=""
                      name="registerForm" 
                      novalidate="" 
                      class="form-validate form-edit" 
                      method="post" 
                      enctype="multipart/form-data">
                    {{ csrf_field()}}
                    <div class="card-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab-01">
                                @include('User.formEdit')
                            </div>
                            <div class="modal-footer">
                                <button type="submit" 
                                        class="btn btn-primary" >Cập nhật</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Close</button>
                                <div class="loading"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script>
                    $('#datepicker-1').datepicker({
            container: '#example-datepicker-container-5'
            });

        </script>

    </div>
</div>

@stop




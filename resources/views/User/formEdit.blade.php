<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="fullName"
                       value="{{ $user->fullname or '' }}"
                       required="" 
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Họ tên</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="username"
                       value="{{ $user->username or '' }}"
                       required="" 
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Tên đăng nhập</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="password"
                       type="password"
                       value=""
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       placeholder="Bỏ trống nếu không thay đổi"
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Mật khẩu</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="passwordConfirm"
                       type="password"
                       value=""
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Xác nhận lại mật khẩu</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="email"
                       value="{{ $user->email or '' }}"
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Email</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <label class="label-title01">Ngày sinh</label>
            <div class="mda-form-control rel-wrapper ui-datepicker ui-datepicker-popup dp-theme-success" 
                 id="example-datepicker-container-5">
                <input name="birthday"
                       value="{{ $user->birthday or '' }}"
                       tabindex="0" 
                       data-date-format="yyyy-mm-dd"
                       id="datepicker-1"
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control datepicker">
                <div class="mda-form-control-line"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="skype"
                       value="{{ $user->skype or '' }}"
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Skyper</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <input name="phone"
                       type="number"
                       value="{{ $user->phone or '' }}"
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control input-sm">
                <div class="mda-form-control-line"></div>
                <label>Điện thoại</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <textarea name="address"
                          tabindex="0" 
                          class="form-control input-sm">{{ $user->address or '' }}</textarea>
                <div class="mda-form-control-line"></div>
                <label>Địa chỉ</label>
            </div>
        </div>
    </div>
</div>
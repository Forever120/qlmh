<?php $disableCustomerForm = ' disabled '?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{!! (($id > 0) ? 'Sửa thông tin nhân viên' : 'Tạo mới nhân viên') !!}</h4>
</div>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab-01" aria-controls="home" role="tab" data-toggle="tab">{!! (($id > 0) ? 'Sửa thông tin nhân viên' : 'Tạo mới nhân viên') !!}</a></li>
    <li role="presentation"><a href="#tab-02" aria-controls="profile" role="tab" data-toggle="tab">Phân quyền</a></li>
</ul>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register"
              action="{{ url('user/edit/'.$id) }}"
              name="registerForm"
              novalidate=""
              class="form-validate form-edit"
              method="post"
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-01">
                        @include('User.formEdit')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-02">
                        @include('User.changePermission')
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-primary"
                                onclick="submitForm('.form-edit', '.loading', '.list-user', '{{ url('user/list') }}')">Cập nhật</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Close</button>
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
            $('#datepicker-1').datepicker({
    container: '#example-datepicker-container-5'
    });

</script>

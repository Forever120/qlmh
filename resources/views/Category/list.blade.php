@extends('Layouts.main')

@section('title')
Quản lý danh mục
@stop

@section('breadcrumb')
Quản lý danh mục
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('content')
<div class="container-fluid">
    <div class="js-nestable-action">
        <a data-action="expand-all" class="btn btn-default btn-sm mr-sm">Mở rộng</a>
        <a data-action="collapse-all" class="btn btn-default btn-sm">Thu gọn</a>
        <a class="btn btn-primary btn-sm" onclick="updatePosition('{{ url('category/update-position') }}')">Cập nhật số thứ tự</a>
        <a class="btn btn-primary btn-sm" onclick="loadPopupLarge('{{ url('category/edit/0') }}')" data-toggle="modal" data-target=".bs-modal-lg">Thêm mới</a>
        <!--<a class="btn btn-primary btn-sm" onclick="loadPopupLarge('/admin/category/edit/132')">Add new</a>-->
        <a class="btn btn-primary btn-sm" onclick="reload('#nestable', '{{ url('category/list') }}')">Reload</a>
        <div class="loader-primary _success status-update"></div>
        <div class="row">
            <div class="col-md-8">
                <div id="nestable" class="dd">
                    {!! $htmlList !!}
                </div>
                <div id="nestable-output" class="well hidden"></div>
            </div>
        </div>
    </div>
</div>
@stop
@extends('Layouts.main')

@section('content')
<div class="container-fluid">
    <?php $colorIdx = 0 ?>
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <b>Nhóm sản phẩm</b>
            </div>
            <div class="row">
                <div class="container-fluid">

                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="row">
                <b>Màu sắc</b>
            </div>
            <div class="row">
                <div class="container-fluid">
                    @foreach($listColor as $color)
                    <div class="row">
                        <!--<div class="col-md-9">
                            {{ $colorIdx ++ }}: <a href="/sua-mau-sac?id={{ $color->colorID }}"> {{ $color->name }} </a>
                        </div>-->
                        <div class="col-md-9">
                            <button type="button" class="btn btn-info btn-xs editColor" value="{{ $color->colorID }}">
                                <strong>{{ $color->name }}</strong>
                            </button>
                        </div>
                        <div class="col-md-3">
                            <!--<a href="/xoa-mau-sac?id={{ $color->colorID }}">-->
                            <button type="button" class="btn btn-danger btn-xs deleteColor" value="{{ $color->colorID }}">
                                <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                            </button>
                            <!--                            </a>-->
                        </div>
                    </div>
                    <?php $colorIdx ++ ?>
                    @endforeach
                    <div class="row">
                        <div class="col-md-1">
                            <!--<a href="/them-mau-sac">-->
                            <button type="button" id="addColor" class="btn btn-success btn-sm">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                            <!--</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <div class="row">
                <b>Chất liệu</b>
            </div>
            <div class="row">
                <div class="container-fluid">
                    @foreach($listMaterial as $material)
                    <div class="row">
                        <div class="col-md-9">
                            <button type="button" class="btn btn-info btn-xs editMaterial" value="{{ $material->materialID }}">
                                <strong>{{ $material->name }}</strong>
                            </button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-danger btn-xs deleteMaterial" value="{{ $material->materialID }}">
                                <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <div class="col-md-1">
                            <button type="button" id="addMaterial" class="btn btn-success btn-sm">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        $("#addColor").click(function () {
            swal({
                title: "Thêm màu sắc!",
                text: "Tên màu sắc mới",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Tên màu sắc mới."
            },
                    function (inputValue) {
                        if (inputValue === false)
                            return false;
                        if (inputValue === "") {
                            swal.showInputError("Hãy nhập tên màu sắc mới.");
                            return false
                        }
                        $.ajax({
                            url: "http://www.localhost:172/them-mau-sac",
                            type: "GET",
                            data: {'value': inputValue},
                            dataType: "JSON",
                            success: function (result) {
                                //$("#div1").html(result['a']);
                                console.log("Success");
                            },
                            error: function (result) {
                                //$("#div1").html(result['a']);
                                console.log("Error");
                            }
                        });
                        window.location.reload();

                    });
        });
        $(".deleteColor").click(function () {
            console.log("aaa " + this.value);
            var colorID = this.value;
            swal({
                title: "Bạn có thật sự muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Có, tôi muốn xóa!",
                cancelButtonText: "Không",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "http://www.localhost:172/xoa-mau-sac" + "/" + colorID,
                                type: "GET",
                                data: [],
                                //dataType: "JSON",
                                success: function (result) {
                                    //$("#div1").html(result['a']);
                                    console.log("success " + result);
                                },
                                error: function (result) {
                                    //$("#div1").html(result['a']);
                                    console.log("Error" + result);
                                }
                            });
                            setTimeout(
                                    function () {
                                        location.reload();
                                    }, 0001);
                            swal("Xóa màu sắc!", "Bạn đã xóa 1 màu sắc thành công.", "success");
                        }
                    });
        });
        $(".editColor").click(function () {
            var colorID = this.value;
            swal({
                title: "Sửa màu sắc!",
                text: "Tên màu sắc mới",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Tên màu sắc mới."
            },
                    function (inputValue) {
                        if (inputValue === false)
                            return false;
                        if (inputValue === "")
                        {
                            swal.showInputError("Hãy nhập tên màu sắc mới.");
                            return false
                        }
                        $.ajax({
                            url: "http://www.localhost:172/sua-mau-sac" + "?id=" + colorID,
                            type: "GET",
                            data: {'value': inputValue},
                            dataType: "JSON",
                            success: function (result) {
                                //$("#div1").html(result['a']);
                                console.log("Success");
                            },
                            error: function (result) {
                                //$("#div1").html(result['a']);
                                console.log("Error");
                            }
                        });
                        swal("Thành công!", "Bạn đã đổi màu sắc sang " + inputValue + "thành công", "success");
                    });
        });
        $("#addMaterial").click(function () {
            swal({
                title: "Thêm chất liệu mới!",
                text: "Tên chất liệu mới",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Tên chất liệu mới."
            },
                    function (inputValue) {
                        if (inputValue === false)
                            return false;
                        if (inputValue === "") {
                            swal.showInputError("Hãy nhập tên màu chất liệu.");
                            return false
                        }
                        $.ajax({
                            url: "http://www.localhost:172/them-chat-lieu",
                            type: "GET",
                            data: {'value': inputValue},
                            dataType: "JSON",
                            success: function (result) {
                                //$("#div1").html(result['a']);
                                console.log("Success");
                            },
                            error: function (result) {
                                //$("#div1").html(result['a']);
                                console.log("Error");
                            }
                        });
                        swal("Thành công!", "Bạn đã thêm thành công chất liệu mới");
                    });
        });
        $(".deleteMaterial").click(function () {
            var materialID = this.value;
            swal({
                title: "Bạn có thật sự muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Có, tôi muốn xóa!",
                cancelButtonText: "Không",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "http://www.localhost:172/xoa-chat-lieu" + "?id=" + materialID,
                                type: "GET",
                                data: [],
                                //dataType: "JSON",
                                success: function (result) {
                                    //$("#div1").html(result['a']);
                                    console.log("Success");
                                },
                                error: function (result) {
                                    //$("#div1").html(result['a']);
                                    console.log("Error");
                                }
                            });
                            swal("Xóa chât liệu!", "Bạn dã xóa 1 chât liệu thành công.", "success");
                        }
                    });
        });
        $(".editMaterial").click(function () {
            var materialID = this.value;
            swal({
                title: "Sửa chất liệu!",
                text: "Tên chất liệu mới",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Tên chất liệu mới."
            },
                    function (inputValue) {
                        if (inputValue === false)
                            return false;
                        if (inputValue === "")
                        {
                            swal.showInputError("Hãy nhập tên chất liệu mới.");
                            return false
                        }
                        $.ajax({
                            url: "http://www.localhost:172/sua-chat-lieu" + "?id=" + materialID,
                            type: "GET",
                            data: {'value': inputValue},
                            dataType: "JSON",
                            success: function (result) {
                                //$("#div1").html(result['a']);
                                console.log("Success");
                            },
                            error: function (result) {
                                //$("#div1").html(result['a']);
                                console.log("Error");
                            }
                        });
                        swal("Thành công!", "Bạn đã đổi chất liệu sang " + inputValue + "thành công", "success");
                    });
        });
    });
</script>

<!--@if
@endif
@foreach
@endforeach-->

@stop
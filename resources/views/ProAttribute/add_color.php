@extends('Layouts.main')

@section('title')
bảng giá
@stop

@section('content')
<div class="form_edit">
    <form id="form_edit" action=""  method="post">
        <table class="table_quanly">
            <tbody>
                <tr class="tr_title_total">
                    <td class="tr_link_11"><b>Form nhâp màu sắc</b></td>
                </tr>
                <tr>
                    <td>
                        <b>Màu sắc</b>
                    </td>
                    <td>
                        <input type="text" value="{{$color->name or ''}}" name="name">
                    </td>
                </tr>
                <tr>
                    <td class="form_td_1" colspan="2">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" 
                               name="{{isset($color) ? 'submit_update_color' : 'submit_add_color'}}" 
                               value="{{isset($color) ? 'Sửa Color' : 'Thêm color'}}">
                    </td>
                </tr>
            </tbody>
        </table>
    </form>    
</div>

@stop

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{!! (($id > 0) ? 'Sửa thông tin đối tác' : 'Thêm đối tác') !!}</h4>
</div>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#tab-01" 
           aria-controls="home" 
           role="tab" 
           data-toggle="tab">Thông tin NCC</a></li>
    <li role="present ation">
        <a href="#tab-02" aria-controls="profile" role="tab" data-toggle="tab">Thông tin liên hệ</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register" 
              action="/supplier/edit/{{ $id }}"
              name="registerForm" 
              novalidate="" 
              class="form-edit"
              class="form-validate form-edit" 
              method="post" 
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-01">
                        @include('Supplier/formEdit')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-02">
                        <div class="row">
                            @include('Supplier/formContact')
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" 
                                onclick="submitForm('.form-edit', '.loading', '.list-table', '{{ url('/supplier/list') }}')"
                                class="btn btn-primary">
                            Cập nhật
                        </button>
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
                    $('#datepicker-1')
                    .datepicker({
                    container: '#example-datepicker-container-5'
                    });
                    $('#datepicker-2')
                    .datepicker({
                    container: '#example-datepicker-container-5'
                    });
                    var cpInput = $('.clockpicker').clockpicker();
                    // auto close picker on scroll
                    $('main').scroll(function() {
            cpInput.clockpicker('hide');
            });
                    $(function() {
                    $('#demo4').typeahead({
                    ajax: '/product/search',
                            onSelect: function (data) {
                            $('.productID').val(data.value)
                            }
                    });
                    });
                    checkFormCustomer
</script>
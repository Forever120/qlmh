@if(count($debitNotes) == 0)
<h5>Chưa có đơn đặt hàng nào</h5>
{{ die() }}
@endif

<div class=" pre-scrollable">
<a class="btn btn-primary btn-sm"
   onclick="loadData('.modal-content02', '/supplier/debit-add-exchange/{{ $sId }}')"
   data-toggle="modal"
   data-target="#myModal">
    Thêm ghi nợ
</a>
<div class="table-responsive list-table">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Mã đơn mua hàng </th>
                <th>Nvmh</th>
                <th>Số tiền trao đổi</th>
                <th>Số tiền đã trả</th>
                <th>Số tiền còn lại</th>
                <th>Tổng tiền</th>
                <th>Ngày giao dịch</th>
                <th>Trạng thái</th>
                <th>Ghi chú</th>
            </tr>
        </thead>
        <tbody>
            <?php $idx = 0?>
            @foreach($debitNotes as $debitNote)
            <?php $idx++?>
            <tr>
                <td>{{ $idx }}</td>
                <td>{{ $debitNote->goodsPurchaseID or '' }}</td>
                <td>{{ $debitNote->nvmhID or '' }}</td>
                <td>{{ $debitNote->exchangeMonney or '' }}</td>
                <td>{{ $debitNote->desposited or '' }}</td>
                <td>{{ $debitNote->remain or '' }}</td>
                <td>{{ $debitNote->totalDebitMoney or '' }}</td>
                <td>{{ $debitNote->date or '' }}</td>
                <td>{{ isset($statusDebits[empty($debitNote->status)]) ? $statusDebits[$debitNote->status] : $statusDebits[0]}}</td>
                <td>{{ $debitNote->note or '' }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
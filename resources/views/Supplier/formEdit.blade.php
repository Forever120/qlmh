<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input type="text" 
                       name="name"
                       value="{{ $supplier->name or '' }}"
                       required="" 
                       class="form-control" 
                       aria-required="true" 
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label>Tên NCC</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input type="text" 
                       name="supplierAddress"
                       value="{{ $supplier->supplierAddress or '' }}"
                       required="" 
                       class="form-control" 
                       aria-required="true" 
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label> Địa chỉ </label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input type="text" 
                       name="supplierWebsite"
                       value="{{ $supplier->supplierWebsite or '' }}"
                       required="" 
                       class="form-control" 
                       aria-required="true" 
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label> Website </label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <select name="status"
                        required="" 
                        class="form-control" 
                        aria-required="true" 
                        aria-invalid="true">
                    <option value="">Chọn trạng thái</option>
                    @foreach($listStatus as $status)
                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                    @endforeach
                </select>
                <div class="mda-form-control-line"></div>
                <label> Trạng thái </label>
            </div>
        </div>
    </div>
</div> 
<div class="row">
    <div class="col-sm-8">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <textarea name="supplierBankInfo" class="form-control">{{ $supplier->supplierBankInfo or '' }}</textarea>
                <div class="mda-form-control-line"></div>
                <label>Thông tin ngân hàng</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <textarea name="note" class="form-control">{{ $supplier->note or '' }}</textarea>
                <div class="mda-form-control-line"></div>
                <label>Ghi chú</label>
            </div>
        </div>
    </div>
</div>

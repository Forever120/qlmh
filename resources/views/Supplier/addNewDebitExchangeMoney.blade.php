<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{!! 'Thêm mới ghi nợ' !!}</h4>
</div>
<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register"
              action="/supplier/debit-add-exchange/{{ $sId }}"
              name="registerForm"
              novalidate=""
              class="form-edit"
              class="form-validate form-edit"
              method="post"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card-body">
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab-01">
                    <div class="row">
                        <div data-autoclose="true" class="mda-form-group mda-input-group col-sm-6">
                            <label class="label-title01">Mã đơn mua hàng</label>
                            <div class="mda-form-control">
                                <input name="goodsPurchaseID"
                                       type="text"
                                       class="form-control goodsPurchase-id">
                                <div class="mda-form-control-line"></div>
                            </div>
                            <span class="mda-input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;MH -</span>
                            <label id="candidate-error"
                                   class="error productInvalid"
                                   for="candidate">Mã đơn mua hàng không tồn tại.</label>
                        </div>
                        <div class="col-sm-6">
                            <div class="mda-form-group mb">
                              <label  class="label-title01"> Số tiền ghi nợ </label>
                                  <div class="mda-form-control">
                                      <input name='exchangeMonney'
                                                type="number"
                                                id="demo4"
                                                class="form-control exchangeMoney">
                                      <div class="mda-form-control-line"></div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-sm-6">
                              <div class="mda-form-group mb">
                                 <label  class="label-title01"> Trạng thái </label>
                                  <div class="mda-form-control">
                                      <select class="form-control">
                                          <option value="2" selected>
                                              {!! $statusDebits[2] !!}
                                          </option>
                                    </select>
                                      <div class="mda-form-control-line"></div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="mda-form-group mb">
                                <div class="mda-form-control">
                                    <textarea type="text"
                                              class="textarea01 note"
                                              name="note">
                                    </textarea>
                                    <label> Ghi chú </label>
                                 </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button"
                        onclick="submitForm('.form-edit', '.loading', '.debitNodeHistory{{ $sId }}', '{{ url('supplier/debit-history'). '/' . $sId }}', 'sAddDebit')"
                        class="btn btn-primary">
                        Lưu
                    </button>
                      <div class="loading"></div>
                  </div>
        </form>
    </div>
</div>

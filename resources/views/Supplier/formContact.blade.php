<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input type="text" 
                       name="supplierNvkdName"
                       value="{{ $supplier->supplierNvkdName or '' }}"
                       required="" 
                       class="form-control" 
                       aria-required="true" 
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label>Tên người liên hệ</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input type="email" 
                       name="supplierNvkdEmail"
                       value="{{ $supplier->supplierNvkdEmail or '' }}"
                       required="" 
                       class="form-control" 
                       aria-required="true" 
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label> Email </label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input type="number" 
                       name="supplierNvkdPhoneNumber"
                       value="{{ $supplier->supplierNvkdPhoneNumber or '' }}"
                       required="" 
                       class="form-control" 
                       aria-required="true" 
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label> Điện thoại </label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input type="text" 
                       name="supplierNvkdSkyAccount"
                       value="{{ $supplier->supplierNvkdSkyAccount or '' }}"
                       required="" 
                       class="form-control" 
                       aria-required="true" 
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label> Skyper </label>
            </div>
        </div>
    </div>
</div> 
<table class="table table-hover">
<thead>
    <tr>
        <th>Mã NCC</th>
        <th>Tên NCC</th>
        <th>Địa chỉ</th>
        <th>Website</th>
        <th>Trạng thái</th>
    </tr>
</thead>
<tbody>
    <?php $idx = 0;?>
    @foreach($supplier as $s)
    <?php $idx++?>
    <tr class="pointer tr_{{ $s->id }}" class="_pointer" onclick="showDetail('.tr_{{ $s->id }}', '.pdetail{{ $s->id }}', '.chitietsp{{ $s->id }}')">
        <td>NCC-{{ $s->id }}</td>
        <td>
            {{ $s->name }}
        </td>
        <td>{{ $s->supplierAddress }}</td>
        <td>{{ $s->supplierWebsite }}</td>
        <td>{{ $status[$s->status] or '' }}</td>
    </tr>
    <tr>
        <td colspan="9" style="display: none" class="pdetail pdetail{{ $s->id }}">
            <div class="_tab">
                &nbsp;
                <span onclick="loadContentTab(this, '.chitietsp{{ $s->id }}', '.chitietsp{{ $s->id }}', '')"
                      class="tab_active"> Thông tin NCC </span>
                <span onclick="loadContentTab(this, '.debit_note_history{{ $s->id }}', '.debitNodeHistory{{ $s->id }}','{{ url('supplier/debit-history'). '/' . $s->id }}')">
                    Lịch sử đặt hàng
                </span>
                @if(in_array('DOI_TAC_DELETE', $listUserPermission))
                <a onclick="deleteRow('/supplier/delete/{{ $s->id }}', '/supplier/list')">
                    <i data-pack="default" class="ion-trash-a"></i>
                </a>
                @endif

                @if(in_array('DOI_TAC_EDIT', $listUserPermission))
                <a onclick="loadData('.modal-content02', '/supplier/edit/{{ $s->id }}')"
                   data-toggle="modal"
                   data-target="#myModal"><i data-pack="default" class="ion-edit"></i></a>
                @endif

            </div>
            <div class="tab_content chitietsp{{ $s->id }}">
                <div class="row">
                    <div class="col-sm-6 table-responsive psummary">
                        <h6><b>Thông tin NCC</b></h6>
                        <ul>
                            <li><b>Tên NCC: </b><span>{{ $s->name or '' }}</span></li>
                            <li><b>Website: </b><span>{{ $s->supplierWebsite or '' }}</span></li>
                            <li><b>Address: </b><span>{{ $s->supplierAddress or '' }}</span></li>
                            <li><b>Thông tin ngân hàng: </b><span>{{ $s->supplierBankInfo or '' }}</span></li>
                            <li><b>Trạng thái: </b><span>{{ $s->status or '' }}</span></li>
                            <li><b>Ghi chú: </b><span>{{ $s->note or '' }}</span></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 table-responsive psummary">
                        <h6><b>Thông tin NVKD</b></h6>
                        <ul>
                            <li><b>Họ tên: </b><span>{{ $s->supplierNvkdName or '' }}</span></li>
                            <li><b>Email: </b><span>{{ $s->supplierNvkdEmail or '' }}</span></li>
                            <li><b>Điện thoại: </b><span>{{ $s->supplierNvkdPhoneNumber or '' }}</span></li>
                            <li><b>Skype: </b><span>{{ $s->supplierNvkdSkyAccount or '' }}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="_hidden tab_content debit_note_history{{ $s->id }}">
                <div class="psummary debitNodeHistory{{ $s->id }}"></div>
            </div>
            <div class="_hidden tab_content thanhphan{{ $s->id }}"></div>
        </td>
    </tr>
    @endforeach
</tbody>

</table>
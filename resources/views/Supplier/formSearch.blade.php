<div class="col-sm-12 actionBar">
    <div class="search form-group">
      <form action="" method="get">
        <div class="input-group">
            <span class="icon input-group-addon ion-search">
                <button type="submit" class="icon input-group-addon ion-search"></button>
            </span>
            <input name="supplierCodeOrName" type="text" class="form-control" value="{{ empty($request->supplierCodeOrName) ? '' : $request->supplierCodeOrName }}" placeholder="Tìm theo Mã, Tên đối tác">
        </div>
      </form>
    </div>
    <div class="actions btn-group">
        <div class="dropdown btn-group">
            <a class="btn btn-primary btn-sm"
                  data-toggle="modal" data-target="#myModal"
                  onclick="loadData('.modal-content02', '/supplier/edit/0')">
                <span class="ion-android-add-circle"></span>
                Thêm mới
            </a>
<!--                <span title="Tìm kiếm nâng cao" class="btn btn-default" onclick="showDetail(this, '.main_search', '')">
                <a class="ion-search"></a>
            </span>-->
        </div>
    </div>
</div>

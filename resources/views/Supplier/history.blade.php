@if(count($productPriceOfSupplier) == 0)
<p>Chưa có đơn đặt hàng nào</p>
{{ die() }}
@endif
<div class="table-responsive list-table">
    <table class="table table-hover"> 
        <thead>
            <tr>
                <th>Tên SP </th>
                <th>Số lượng</th>
                <th>Giá</th>
                <th>Ngày đặt hàng</th>
                <th>Ngày nhận hàng</th>
                <th>Trạng thái</th>
                <th>Ghi chú</th>
            </tr>
        </thead>
        <tbody>
            <?php $idx = 0 ?>
            @foreach($productPriceOfSupplier as $s)
            <?php $idx++ ?>
            <tr class="pointer">
                <td>{{ $s->pName or '' }}</td>
                <td>{{ $s->lpName or '' }}</td>
                <td>{{ $s->price or '' }}</td>
                <td>{{ $s->dateOfOrders or '' }}</td>
                <td>{{ $s->dateOfReceipt or '' }}</td>
                <td>{{ $s->sName or '' }}</td>
                <td>{{ $s->note or '' }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
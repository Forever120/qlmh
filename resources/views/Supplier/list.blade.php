@extends('Layouts.main')

@section('title')
Quản lý nhà cung cấp
@stop

@section('breadcrumb')
Quản lý nhà cung cấp
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop


@section('content')
<style>
    .datepicker-days {
        z-index: 1000;
    }
</style>

<div class="container-fluid">
    <div class="card">
        <div id="bootgrid-basic-header" class="bootgrid-header container-fluid">
            <div class="row">
                @include('Supplier.formSearch')
            </div>
        </div>

        <div class="table-responsive list-table" id="nestable">
            @include('Supplier.listReload')
        </div>
    </div>
</div>

@stop

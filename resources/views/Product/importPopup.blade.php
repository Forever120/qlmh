
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Import dữ liệu vào bảng giá</h4>
</div>
<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register" 
              action="/bang-gia/import"
              name="registerForm" 
              novalidate="" 
              class="form-validate form-edit" 
              method="post" 
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mda-form-group mb">
                                <div class="mda-form-control">
                                     <input type="file" name="import"">
                                    <div class="mda-form-control-line"></div>
                                    <label>chọn file import</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">
                            Import
                        </button>
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<form action="" method="get">

    <div class="col-sm-12 _top10 main_search">
        <input name="keyword" type="text" class="form-control" value="{{ isset($getRequest['keyword']) ? $getRequest['keyword'] : '' }}" placeholder="Tìm theo tên hoặc mã sản phẩm">
        <br/>
        <div class="grid-item item-search">
            <h6 class="mt0">
                <a class="glyphicon glyphicon-cog pointer"
                   href="{{ url('category/list') }}"
                   aria-hidden="true"></a>
                <span>Chọn danh mục</span>
            </h6>
            <ul class="list-unstyled content-item-search">
                @foreach($lstCate as $cateID => $cate)
                <li>
                    <div class="checkbox c-checkbox">
                        <label>
                            <input type="checkbox"
                                   name="category[]"
                                   @if(in_array($cateID, $getRequest['category']))
                                   checked
                                   @endif
                                   value="{{ $cateID }}" />
                                   <span class="ion-checkmark-round"></span>
                            {{ $cate }}
                        </label>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="grid-item item-search">
            <h6 class="mt0">
                <a class="glyphicon glyphicon-cog pointer"
                   href="{{ url('color/list') }}"
                   aria-hidden="true"></a>
                <span>Chọn Màu sắc</span>
            </h6>
            <ul name="color" class="list-unstyled content-item-search">
                @foreach($lstColor as $colorID => $color)
                <li>
                    <div class="checkbox c-checkbox">
                        <label>
                            <input type="checkbox"
                                   name="color[]"
                                   @if(in_array($colorID, $getRequest['color']))
                                   checked
                                   @endif
                                   value="{{ $colorID }}" />
                                   <span class="ion-checkmark-round"></span>
                            {{ $color }}
                        </label>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="grid-item item-search">
            <h6 class="mt0">
                <a class="glyphicon glyphicon-cog pointer"
                   href="{{ url('chat-lieu/list') }}"
                   aria-hidden="true"></a>
                <span>Chọn chất liệu</span>
            </h6>
            <ul name="material" class="list-unstyled content-item-search">
                @foreach($lstMaterial as $materialID => $material)
                <li>
                    <div class="checkbox c-checkbox">
                        <label>
                            <input type="checkbox"
                                   name="material[]"
                                   @if(in_array($materialID, $getRequest['material']))
                                   checked
                                   @endif
                                   value="{{ $materialID }}" />
                                   <span class="ion-checkmark-round"></span>
                            {{ $material }}
                        </label>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="grid-item item-search">
            <h6 class="mt0">Thể tích</h6>
            <ul class="list-unstyled ul_search02">
                <li>
                    <input type="number"
                           class="form-control input-sm"
                           name="volume1"
                           placeholder="Từ"
                           value="{{ $getRequest['volume1'] }}" />
                </li>
                <li>
                    <input type="number"
                           class="form-control input-sm"
                           name="volume2"
                           placeholder="Đến"
                           value="{{ $getRequest['volume2'] }}" />
                </li>
            </ul>
            <h6 class="mt0">Khối lượng</h6>
            <ul name="material" class="list-unstyled ul_search02">
                <li>
                    <input type="number"
                           class="form-control input-sm"
                           name="weight1"
                           placeholder="Từ"
                           value="{{ $getRequest['weight1'] }}" />
                </li>
                <li>
                    <input type="number"
                           class="form-control input-sm"
                           name="weight2"
                           placeholder="Đến"
                           value="{{ $getRequest['weight2'] }}" />
                </li>
            </ul>
        </div>
        <div class="grid-item">
            <input type="submit" class="btn btn-success" value="Tìm kiếm"/>
        </div>

    </div>
</form>

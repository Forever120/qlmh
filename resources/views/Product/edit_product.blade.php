
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{!! (($id > 0) ? 'Sửa thông tin sản phẩm trong bảng giá' : 'Tạo sản phẩm trong bảng giá') !!}</h4>
</div>
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#form-edit-pro" aria-controls="home" role="tab" data-toggle="tab">Chi tiết sản phẩm</a></li>
<!--         <li role="presentation"><a href="#up-image" aria-controls="profile" role="tab" data-toggle="tab">Hình ảnh</a></li> -->
    </ul>

    <!-- Tab panes -->
    <div class="modal-body">
        <div class="card">
            <form id="form-register" 
                  action="/edit-product/{{ $id }}"
                  name="registerForm" 
                  novalidate="" 
                  class="form-validate form-edit" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field()}}
                <input type="hidden" name="parentID" value="{{ $parentID }}">
                <div class="card-body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="form-edit-pro">
                            @include('element.form_edit_product')
                        </div>
<!--                         <div role="tabpanel" class="tab-pane" id="up-image">
                            @include('element.upload_image_product')
                        </div> -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" 
                                    onclick="submitForm('.form-edit', '.loading', '.detail-list-product', '/bang-gia')"
                                    class="btn btn-primary">
                                Cập nhật
                            </button>
                            <div class="loading"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@extends('Layouts.main')

@section('title')
Bảng giá
@stop

@section('breadcrumb')
Bảng giá
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('scriptCustom')

@stop

@section('content')
<div class="container-fluid">
    <div class="card">

        <div class="table-responsive detail-list-product" id="nestable">
            <form action="" class="form-export" method="post">
                {{ csrf_field()}}
                &nbsp;&nbsp;
                <br/>
                <!--<div class="row">-->
                <em class="_success">&nbsp;&nbsp; {{ $message or '' }}</em>
                    <br>
                    &nbsp;&nbsp;&nbsp;
                    <a class="btn btn-sm btn-success" href="/bang-gia">Quay về trang bảng giá</a>
                <!--</div>-->
                <hr/>
                <div class="lst-import">
                    <table class="table table-hover"> 
                        <thead>
                            <tr>
                                <th>
                                    <input onclick="checkAllProduct(this)" 
                                           checked=""
                                           type="checkbox"/>
                                    All
                                </th>
                                <th>Tên SP</th>
                                <th>Nhóm SP</th>
                                <th>Chất liệu</th>
                                <th>Màu sắc</th>
                                <th>Khối lượng</th>
                                <th>Thể tích</th>
                                @foreach ($listPrices as $priceName)
                                <th class="price_{{ $priceName->id }}">{{ $priceName->name }}</th>
                                @endforeach
                                <th>Mô tả</th>
                                <th>Ghi chú</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($excelData) > 0)
                            @foreach($excelData as $data)
                            <tr>
                                <td><input name="option[]" type="checkbox" checked="" /></td>
                                <td>
                                    {{ $data->ten_san_pham  or '' }}
                                    <input type="hidden" name="productName[]"/>
                                </td>
                                <td>
                                    {{ $data->nhom_san_pham  or '' }}
                                    <input type="hidden" name="categoryName[]"/>
                                </td>
                                <td>
                                    {{ $data->chat_lieu  or '' }}
                                    <input type="hidden" name="materialName[]"/>
                                </td>
                                <td>
                                    {{ $data->mau_sac  or '' }}
                                    <input type="hidden" name="colorName[]"/>
                                </td>
                                <td>
                                    {{ $data->khoi_luong  or '' }}
                                    <input type="hidden" name="weight[]"/>
                                </td>
                                <td>
                                    {{ $data->the_tich  or '' }}
                                    <input type="hidden" name="volume[]"/>
                                </td>
                                @foreach ($priceArr as $priceID => $priceName)
                                <td>
                                    {{ $data[$priceName]  or '' }}
                                    <input type="hidden" name="list_price{{ $priceID }}[]"/>
                                </td>
                                @endforeach
                                <td>
                                    {{ $data->mo_ta  or 'null' }}
                                    <input type="hidden" name="description[]"/>
                                </td>
                                <td>
                                    {{ $data->ghi_chu or 'null' }}
                                    <input type="hidden" name="note[]"/>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>

@stop


@extends('Layouts.main')

@section('title')
Bảng giá
@stop

@section('breadcrumb')
Bảng giá
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('scriptCustom')
<script>
    $(document).ready(function() {
        loadCollumnPrice();
    });</script>
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div id="bootgrid-basic-header" class="bootgrid-header container-fluid">
            <div class="row">
                <p class="_success">{{ $message or '' }}</p>
                <form method="post" action="">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mda-form-group mb">
                                <div class="mda-form-control">
                                    <input type="file" name="import"">
                                    <div class="mda-form-control-line"></div>
                                    <label>chọn file import</label>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-primary">Import</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop


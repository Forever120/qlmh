<div class="container-fluid">
    <div class="card">
        <div class="table-responsive">
            @if (count($listProduct) > 0)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        @foreach ($listPrices as $priceName)
                        <th  {!! ((array_search($priceName->id, $listPriceActive) === false) ? ' style="display: none" ' : '' ) !!}
                            class="price_{{ $priceName->id }}">{{ $priceName->name }}</th>
                        @endforeach
                    </tr>
                </thead>.
                <tbody>

                    <?php $idx = 0?>
                    @foreach($listProduct as $product)
                    <?php $idx++?>
                    <tr class="pointer tr_{{ $idx }}">
                        <td>{{ $idx }}</td>
                        <td> {{ $product->name }} </td>
                        @foreach ($product->listPrice as $price)
                        <td  {!! ((array_search($price['listPriceId'], $listPriceActive) === false) ? ' style="display: none" ' : '') !!}>
                            <span
                            @if($price['editPermission'] == 1)
                            <?php $active = '_enable_edit'?>
                            onclick="clickToEdit(this, '/update-price/{{ $product->id }}/{{ $price['listPriceId'] }}')"
                            @else
                            <?php $active = '_disable_edit'?>
                            @endif
                            class="{{ $active }}">
                            {{ $price['prodPrice'] }}
                        </span>
                        </td>
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                <h5>Sản phẩm này không có thành phần</h5>
            @endif
        </div>
    </div>
</div>



@extends('Layouts.main')

@section('title')
Bảng giá
@stop

@section('breadcrumb')
Bảng giá
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('scriptCustom')
<script>
//     $(document).ready(function() {
//         loadCollumnPrice();
//     });
</script>
@stop

@section('content')

<div class="container-fluid">
    <div class="card">
        <div id="bootgrid-basic-header" class="bootgrid-header container-fluid">
            <div class="row">
                @include('Product.form_sreach_product')
            </div>
        </div>

        <div class="table-responsive detail-list-product" id="nestable">
            @include('Product.list_price_reload')
            <center> {!! $listProduct->render() !!} </center>
        </div>
    </div>
</div>

@stop

<form action="{{ url('bang-gia/export') }}" class="form-export" method="post">
        {{ csrf_field()}}
        @include('Product.btn')
        <table class="table table-hover table-total">
            <thead class="table-header">
                <tr>
                    <th width="20px">
                        <input onclick="checkAllProduct(this)" type="checkbox"/>
                    </th>
                    <th width="80px" class="pro_cd" {!! ((array_search('pro_cd', $listPriceActive) === false) ? ' style="display: none" ' : '' ) !!}>Mã SP</th>
                    <th width="250px">Tên sản phẩm</th>
                    @foreach ($listPrices as $priceName)
                    <th width="100px" class="price_{{ $priceName->id }}" {!! ((array_search($priceName->id, $listPriceActive) === false) ? ' style="display: none" ' : '' ) !!} >{{ $priceName->name }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody class="table-body">

                <?php $idx = 0?>
                @foreach($listProduct as $product)
                <?php $idx++?>
                <tr class="pointer tr_{{ $idx }}">
                    <td  width="20px">
                        <input name="option[]"
                               onclick="checkExport()"
                               value="{{ $product->id }}"
                               type="checkbox"/>
                    </td>
                    <td width="80px" class="pro_cd" {!! ((array_search('pro_cd', $listPriceActive) === false) ? ' style="display: none" ' : '' ) !!}>SP-{{ $product->id }}</td>
                    <td width="250px" class="_pointer"
                        onclick="showDetail('.tr_{{ $idx }}', '.pdetail{{ $idx }}', '.chitietsp{{ $idx }}')">
                        {{ $product->name }}
                    </td>
                    @foreach ($product->listPrice as $price)
                    <td width="100px" class="price_{{ $price['listPriceId'] }}" {!! ((array_search($price['listPriceId'], $listPriceActive) === false) ? ' style="display: none" ' : '') !!}>
                        <span
                            @if($price['editPermission'] == 1)
                            <?php $active = '_enable_edit'?>
                            onclick="clickToEdit(this, '/update-price/{{ $product->id }}/{{ $price['listPriceId'] }}')"
                            @else
                            <?php $active = '_disable_edit'?>
                            @endif
                            class="{{ $active }}">
                            {{ (($price['prodPrice'] == 'N/A') ? $price['prodPrice'] : number_format($price['prodPrice'])) }}
                    </span>
                </td>
                @endforeach
            </tr>
            <tr>
                <td colspan="{{ 5 + count($product->listPrice) }}" style="display: none" class="pdetail pdetail{{ $idx }}">
                    <div class="_tab">
                        &nbsp;
                        <span class="tab_active" onclick="loadContentTab(this, '.chitietsp{{ $idx }}', '.chitietsp{{ $idx }}', '')"> Thông tin chi tiết </span>
                        <span onclick="loadContentTab(this, '.thanhphan{{ $idx }}', '.thanhphan{{ $idx }}','{{ url('sub-product') }}/{{ $product->id }}')"> Thành phần </span>
                        <span onclick="loadContentTab(this, '.nhacungcap{{ $product->id }}', '.nhacungcap{{ $product->id }}', '{{ url('product-supplier/list/' . $product->id) }}')"> Nhà cung cấp </span>

                        @if(in_array('BANG_GIA_DELETE', $listUserPermission))
                        <a onclick="deleteRow('/product/delete/{{ $product->id }}', '/bang-gia', '#nestable', 'reload')">Xóa SP</a>
                        @endif

                        @if(in_array('BANG_GIA_EDIT', $listUserPermission))
                            <a onclick="loadData('.modal-content02', '/edit-product/0?pid={{ $product->id }}')"
                               data-toggle="modal"
                               data-target="#myModal">Nhân bản</a>

                            <a onclick="loadData('.modal-content02', '/edit-product/0?parentID={{ $product->id }}')"
                               data-toggle="modal"
                               data-target="#myModal">Thêm thành phần</a>

                            <a onclick="loadData('.modal-content02', '/product-supplier/edit/0/{{ $product->id }}')"
                               data-toggle="modal"
                               data-target="#myModal">Thêm NCC</a>

                            <a onclick="loadData('.modal-content02', '/edit-product/{{ $product->id }}')"
                               data-toggle="modal"
                               data-target="#myModal">Chỉnh sửa</a>
                        @endif
                        <!--<a href="">Đưa vào đơn hàng</a>-->
                    </div>
                    <div class="tab_content chitietsp{{ $idx }}">
                        <div class="img_product thumbnail">
                            <?php
$imgIdx = 0;
$countPImg = count($product->images);
?>
                            @if($countPImg > 0)
                            @foreach($product['images'] as $img)
                            <?php $imgIdx++;?>
                            <img class="{{ $imgIdx == 1 ? 'img_large' : 'img_thumb' }}"
                                 src="{{ $img->path }}">
                            @endforeach
                            @else
                            <img class="no_img" src="/imgs/no_image.jpg">
                            @endif
                        </div>
                        <div class="psummary">
                            <ul>
                                <li><b>Tên sản phẩm:</b><span>{{ $product->name }}</span></li>
                                <li><b>Mã sản phẩm:</b><span>{{ $product->id }}</span></li>
                                <li><b>Nhóm sản phẩm:</b><span>{{ $lstCate[$product->categoryID] or '' }}</span></li>
                                <li><b>Chất liệu:</b><span>{{ $lstMaterial[$product->materialID] or '' }}</span></li>
                                <li><b>Màu sắc:</b><span>{{ $lstColor[$product->colorID] or '' }}</span></li>
                                <li><b>Khối lượng:</b><span>{{ $product->weight }}</span></li>
                                <li><b>Thể tích:</b><span>{{ $product->volume }}</span></li>
                                <li><b>Mô tả:</b><span>{{ $product->description }}</span></li>
                                <!-- <li><b>Ghi chú:</b><span>{{ $product->note }}</span></li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="_hidden tab_content thanhphan{{ $idx }}"></div>
                    <div class="_hidden tab_content nhacungcap{{ $product->id }}"></div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</form>
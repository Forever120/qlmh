<div class="col-sm-12 actionBar main-btn">
    @if(in_array('BANG_GIA_EDIT', $listUserPermission))
    <a class="btn btn-primary btn-sm"
       onclick="loadData('.modal-content02', '/edit-product/0')"
       data-toggle="modal"
       data-target="#myModal">
        <span class="ion-android-add-circle"></span>
        Thêm mới
    </a>
    @endif

    <button class="btn btn-primary btn-sm btn-export"
            name="type"
            value="export"
            disabled="">
        <span class="ion-arrow-up-a"></span>
        Xuất ra file excel
    </button>

    <a class="btn btn-primary btn-sm"
       onclick="loadData('.modal-content02', 'bang-gia/import?popup=1')"
       data-toggle="modal"
       data-target="#myModal">
        <span class="ion-arrow-down-a"></span>
        Nhập từ file excel
    </a>

    <span title="Tìm kiếm nâng cao" class="btn btn-primary btn-sm" onclick="showDetail(this, '.main_search', '')">
        <span class="ion-search"></span>
        Tìm kiếm nâng cao
    </span>

<!--     <button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">
        <span class="dropdown-text">hiển thị</span>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu pull-right" role="menu">
        <?php
if (isset($_GET['show'])) {
    $countShow = $_GET['show'];
} else {
    $countShow = 20;
}
?>
        <li class="{{ $countShow == 10 ? 'active':'' }}" aria-selected="false">
            <a data-action="10"
               href="?show=10"
               class="dropdown-item dropdown-item-button">10</a>
        </li>
        <li class="{{ $countShow == 20 ? 'active':'' }}" aria-selected="false">
            <a data-action="20"
               href="?show=20"
               class="dropdown-item dropdown-item-button">20</a>
        </li>
        <li class="{{ $countShow == 30 ? 'active':'' }}" aria-selected="true">
            <a data-action="30"
               href="?show=30"
               class="dropdown-item dropdown-item-button">30</a>
        </li>
        <li class="{{ $countShow == 40 ? 'active':'' }}" aria-selected="false">
            <a data-action="40"
               href="?show=40"
               class="dropdown-item dropdown-item-button">40</a></li>
        <li class="{{ $countShow == 50 ? 'active':'' }}" aria-selected="false">
            <a data-action="-1"
               href="?show=50"
               class="dropdown-item dropdown-item-button">50</a>
        </li>
    </ul> -->
    <div class="actions btn-group">
        @if(isset($listPrices))
        <div class="dropdown btn-group">
            <button class="btn btn-default dropdown-toggle  btn btn-primary  btn-sm" type="button" data-toggle="dropdown">
                <span class="dropdown-text">Tùy chọn hiển thị collumn <span class="icon ion-ios-list-outline"></span></span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right custom_collumn" role="menu">
                <li>
                    <label class="dropdown-item">
                        <input name="pro_cd"
                               type="checkbox"
                               id="pro_cd"
                               value="pro_cd"
                               class="dropdown-item-checkbox"
                               {{ array_search('pro_cd', $listPriceActive) !== false ? ' checked ' : '' }}
                               onclick="changeCollumnPrice(this, 'pro_cd')">
                               {{ 'Mã SP' }}
                    </label>
                </li>
                @foreach ($listPrices as $priceName)
                <li>
                    <label class="dropdown-item">
                        <input name="price[]"
                               type="checkbox"
                               id="lstPrice{{ $priceName->id }}"
                               value="price_{{ $priceName->id }}"
                               class="dropdown-item-checkbox"
                               {{ array_search($priceName->id, $listPriceActive) !== false ? ' checked ' : '' }}
                               onclick="changeCollumnPrice(this, '{{ $priceName->id }}')">
                               {{ $priceName->name }}
                    </label>
                </li>
                @endforeach
                <li>
                    <a href="/collumn/list" class="collumn_link">Quản lý collumn</a></li>
            </ul>
        </div>
        @endif
    </div>
    <button class="btn btn-primary btn-sm btn-export"
            name="type"
            value="trash"
            disabled="">
        <span class="ion-trash-a"></span>
        Xóa
    </button>
</div>


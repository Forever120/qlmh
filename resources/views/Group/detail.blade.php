<?php
$perJson = file_get_contents(JSON_PERMISSION_FILE);
$permission = json_decode($perJson, true);
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Danh sách quyền: {{$group->name}}</h4>
</div>
<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <div class="card-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab">
                    <table class="table table-striped">
                        <tbody>
                            @foreach($permission['Permission'] as $per)
                            @foreach($per['sub'] as $sub)

                            @if (in_array($sub['id'], $permissionUsing)) 
                            <tr>
                                <td><a class="ion-android-done mr"></a>{{ $sub['name'] }}</td>
                            </tr>
                            @endif
                            @endforeach
                            @endforeach
                            
                            <!--check per collumn-->
                            @foreach($listPrice as $lp)
                                @if (in_array('COLLUMN_' . $lp->id, $permissionUsing))
                                    <tr>
                                        <td><a class="ion-android-done mr"></a>Sửa cột "{{ $lp->name }}" trong bảng giá</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>

                </div>

                <div class = "modal-footer">
                    <button type = "button" class = "btn btn-primary" data-dismiss = "modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
</div>
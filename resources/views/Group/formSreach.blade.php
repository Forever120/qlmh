<form action="" method="get">
    <div class="col-sm-12 actionBar">
        <div class="search form-group">
            <div class="input-group">
                <span class="icon input-group-addon ion-search"> 
                    <button type="submit" class="icon input-group-addon ion-search"></button>
                </span> 
                <input type="text" class="form-control" placeholder="Tìm theo tên nhóm quyền">
            </div>
        </div>
        <div class="actions btn-group">
            <div class="dropdown btn-group">
                <span title="Thêm mới nhân viên" class="btn btn-default" 
                      data-toggle="modal" data-target="#myModal"
                      onclick="loadData('.modal-content02', '{{ url('group/edit/0') }}')">
                    <a class="ion-android-add-circle"></a>
                </span>
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class="dropdown-text">hiển thị</span> 
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <?php
                    if (isset($_GET['show'])) {
                        $countShow = $_GET['show'];
                    } else {
                        $countShow = 20;
                    }
                    ?>
                    <li class="{{ $countShow == 20 ? 'active':'' }}" aria-selected="false">
                        <a data-action="20" 
                           href="?show=20"
                           class="dropdown-item dropdown-item-button">20</a>
                    </li>
                    <li class="{{ $countShow == 30 ? 'active':'' }}" aria-selected="true">
                        <a data-action="30" 
                           href="?show=30"
                           class="dropdown-item dropdown-item-button">30</a>
                    </li>
                    <li class="{{ $countShow == 40 ? 'active':'' }}" aria-selected="false">
                        <a data-action="40" 
                           href="?show=40"
                           class="dropdown-item dropdown-item-button">40</a></li>
                    <li class="{{ $countShow == 50 ? 'active':'' }}" aria-selected="false">
                        <a data-action="-1" 
                           href="?show=50"
                           class="dropdown-item dropdown-item-button">50</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
</form>
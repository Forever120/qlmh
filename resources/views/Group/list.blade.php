@extends('Layouts.main')

@section('title')
Quản lý nhóm quyền
@stop

@section('breadcrumb')
Quản lý nhóm quyền
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div id="bootgrid-basic-header" class="bootgrid-header container-fluid">
            <div class="row">
                @include('Group.formSreach')
            </div>
        </div>
        <!--<div class="card-heading">Product management</div>-->
        <div class="table-responsive list-group" id="nestable">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên nhóm quyền</th>
                        <th>Tùy chọn</th>
                    </tr>
                </thead>.
                <tbody>
                    <?php $idx = 0 ?>
                    @foreach($listGroup as $g)
                    <?php $idx++ ?>
                    <tr class="_pointer">
                        <td onclick="loadData('.modal-content02', '/group/detail/{{ $g->id }}')"
                            data-toggle="modal" 
                            data-target="#myModal">G-{{ $g->id }}</td>
                        <td onclick="loadData('.modal-content02', '/group/detail/{{ $g->id }}')"
                            data-toggle="modal" 
                            data-target="#myModal">{{ $g->name }}</td>
                        <td>
                            <a onclick="loadData('.modal-content02', '/group/edit/{{ $g->id }}')"
                               data-toggle="modal" 
                               data-target="#myModal">
                                <i data-pack="default" class="ion-edit"></i>
                            </a>
                            &nbsp;
                            <a onclick="deleteRow('/group/delete/{{ $g->id }}', '/group/list')">
                                <i data-pack="default" class="ion-trash-a"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="7">{!! $listGroup ->render() !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END row-->



@stop
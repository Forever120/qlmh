<?php
$perJson = file_get_contents(JSON_PERMISSION_FILE);
$permission = json_decode($perJson, true);
?>
<div class="row">
    <div class="mda-form-group">
        <div class="mda-form-control">
            <input name="name"
                   value="{{ $group->name or '' }}"
                   required="" 
                   tabindex="0" 
                   aria-required="true" 
                   aria-invalid="true" 
                   class="form-control input-sm">
            <div class="mda-form-control-line"></div>
            <label>Tên nhóm quyền</label>
        </div>
    </div>
</div>
<br>
@foreach($permission['Permission'] as $per)
<div class="row">
    <div class="mda-form-control">
        <label>{{ $per['name'] }}</label>
    </div>
</div>
<div class="row">
    @foreach($per['sub'] as $sub)
    <?php
//    print_r($sub['id']);die('xx');
    if (count($permissionUsing) > 0 && in_array($sub['id'], $permissionUsing)) {
        
            $checked = " checked ";
        
    } else {
        $checked = '';
    }
    ?>
    <div class="col-sm-4">
        <div class="checkbox c-checkbox">
            <label class="small">
                <input {{ $checked }} name="permission[]" type="checkbox" value="{{ $sub['id'] }}">
                <span class="ion-checkmark-round"></span> 
                {{ $sub['name'] }}
            </label>
        </div>
    </div>
    @endforeach
</div>
@endforeach

<!--list price-->
<div class="row">
    <div class="mda-form-control">
        <label>Quyền sửa collumn bảng giá</label>
    </div>
</div>
<div class="row">
    @foreach($listPrice as $lp)
    <?php
    if (count($permissionUsing) > 0 &&  in_array('COLLUMN_' . $lp->id, $permissionUsing)) {
        $checked = " checked ";
    } else {
        $checked = '';
    }
    ?>
    <div class="col-sm-4">
        <div class="checkbox c-checkbox">
            <label class="small">
                <input {{ $checked }} name="permission[]" type="checkbox" value="COLLUMN_{{ $lp->id }}">
                <span class="ion-checkmark-round"></span> 
                {{ $lp->name }}
            </label>
        </div>
    </div>
    @endforeach
</div>

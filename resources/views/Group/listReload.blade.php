<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Tên nhóm quyền</th>
            <th>Tùy chọn</th>
        </tr>
    </thead>.
    <tbody>
        <?php $idx = 0 ?>
        @foreach($listGroup as $g)
        <?php $idx++ ?>
        <tr class="_pointer">
            <td onclick="loadData('.modal-content02', '/group/detail/{{ $g->id }}')"
                data-toggle="modal" 
                data-target="#myModal">G-{{ $g->id }}</td>
            <td onclick="loadData('.modal-content02', '/group/detail/{{ $g->id }}')"
                data-toggle="modal" 
                data-target="#myModal">{{ $g->name }}</td>
            <td>
                <a onclick="loadData('.modal-content02', '/group/edit/{{ $g->id }}')"
                   data-toggle="modal" 
                   data-target="#myModal">
                    <i data-pack="default" class="ion-edit"></i>
                </a>
                &nbsp;
                <a onclick="deleteRow('/group/delete/{{ $g->id }}', '.list-group', '/group/list')">
                    <i data-pack="default" class="ion-trash-a"></i>
                </a>
            </td>
        </tr>
        @endforeach
        <tr>
            <td colspan="7">{!! $listGroup ->render() !!}</td>
        </tr>
    </tbody>
</table>
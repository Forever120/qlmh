<table class="table table-hover">
    <thead>
        <tr>
            <th>Mã packing list</th>
            <th>Tên</th>
            <th>Số lượng</th>
            <th>Mô tô</th>
            <th>Ngày tạo</th>
            <th>Thao tác</th>
        </tr>
    </thead>.
    <tbody>
        <?php $idx = 0;?>
        @foreach($packingList as $pack)
        <?php $idx++?>
        <tr class="tr_{{ $idx }}" >
            <td>
                PK-{{ $pack->id }}
            </td>
            <td>
                {{ $pack->name }}
            </td>
            <td>{{$pack->count}}</td>
            <td>{{$pack->desciption}}</td>
            <td>{{ $pack->created_at }} </td>
            <td>
                <div class="option-menu">
                    <a onclick="loadData('.modal-content02', '{{ url('packing-list/edit/'.$pack->id ) }}')"
                       data-toggle="modal"
                       data-target="#myModal">
                        <i data-pack="default" class="ion-edit"></i>
                    </a>
                    &nbsp;&nbsp;
                    <a onclick="deleteRow('/packing-list/delete/{{ $pack->id }}', '/packing-list/list')" tabindex="-1">
                        <i data-pack="default" class="ion-trash-a"></i>
                    </a>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
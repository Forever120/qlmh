
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Sửa thông tin packing list</h4>
</div>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register" 
              action="/packing-list/edit/{{ $id }}"
              name="registerForm" 
              class="form-validate form-edit" 
              method="post" 
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="count">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <input type="text" 
                                               name="name"
                                               value="{{ $packingList->name or '' }}"
                                               required="" 
                                               class="form-control customer customer-name" 
                                               aria-required="true" 
                                               aria-invalid="true">
                                        <div class="mda-form-control-line"></div>
                                        <label>Tên packing list</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group mb ">
                                    <div class="mda-form-control">
                                        <input name="count" 
                                               type="number" 
                                               value="{{ $packingList->count or '' }}"
                                               required="" 
                                               tabindex="0" 
                                               aria-required="false" 
                                               aria-invalid="false" 
                                               class="form-control customer customer-phone">
                                        <div class="mda-form-control-line"></div>
                                        <label>Số lượng</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-group ">
                                <div class="mda-form-control">
                                    <textarea type="text" 
                                              name="desciption"
                                              required="" 
                                              class="form-control customer customer-name" 
                                              aria-required="true" 
                                              aria-invalid="true">{{ $packingList->desciption or '' }}</textarea>
                                    <div class="mda-form-control-line"></div>
                                    <label>Mô tả</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">  
                        <button class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" 
                                onclick="submitForm('.form-edit', '.loading', '#nestable', '/packing-list/list', 'reload')"
                                class="btn btn-success">
                            Cập nhật
                        </button>
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
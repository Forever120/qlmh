<div class="col-sm-12 actionBar main-btn">
    @if(in_array('PACKING_LIST_EDIT', $listUserPermission))
    <a class="btn btn-primary btn-sm" 
       onclick="loadData('.modal-content02', '{{ url('packing-list/edit/0') }}')" 
       data-toggle="modal" 
       data-target="#myModal">
        <span class="ion-android-add-circle"></span>
        Thêm mới
    </a>
    @endif

    <button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">
        <span class="dropdown-text">hiển thị</span> 
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu pull-right" role="menu">
        <?php
        if (isset($_GET['show'])) {
            $countShow = $_GET['show'];
        } else {
            $countShow = 20;
        }
        ?>
        <li class="{{ $countShow == 20 ? 'active':'' }}" aria-selected="false">
            <a data-action="20" 
               href="?show=20"
               class="dropdown-item dropdown-item-button">20</a>
        </li>
        <li class="{{ $countShow == 30 ? 'active':'' }}" aria-selected="true">
            <a data-action="30" 
               href="?show=30"
               class="dropdown-item dropdown-item-button">30</a>
        </li>
        <li class="{{ $countShow == 40 ? 'active':'' }}" aria-selected="false">
            <a data-action="40" 
               href="?show=40"
               class="dropdown-item dropdown-item-button">40</a></li>
        <li class="{{ $countShow == 50 ? 'active':'' }}" aria-selected="false">
            <a data-action="-1" 
               href="?show=50"
               class="dropdown-item dropdown-item-button">50</a>
        </li>
    </ul>
</div>


@extends('Layouts.main')

@section('title')
Packing list
@stop

@section('breadcrumb')
Packing list
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('scriptCustom')
<script>
    $(document).ready(function() {
    loadCollumnPrice();
    });</script>
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <form class="deleteTable" method="get">
            <form action="{{ url('bao-gia/export') }}" class="form-export" method="post">
                <div class="table-responsive list-notify-price">
                    {{ csrf_field()}}
                    &nbsp;&nbsp;
                    @include('PackingList.btn')
                    <div id="nestable">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Mã packing list</th>
                                    <th>Tên</th>
                                    <th>Số lượng</th>
                                    <th>Mô tô</th>
                                    <th>Ngày tạo</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>.
                            <tbody>
                                <?php $idx = 0;?>
                                @foreach($packingList as $pack)
                                <?php $idx++?>
                                <tr class="tr_{{ $idx }}" >
                                    <td>
                                        PK-{{ $pack->id }}
                                    </td>
                                    <td>
                                        {{ $pack->name }}
                                    </td>
                                    <td>{{$pack->count}}</td>
                                    <td>{{$pack->desciption}}</td>
                                    <td>{{ $pack->created_at }} </td>
                                    <td>
                                        <div class="option-menu">
                                            <a onclick="loadData('.modal-content02', '{{ url('packing-list/edit/'.$pack->id ) }}')"
                                                data-toggle="modal"
                                                data-target="#myModal">
                                                <i data-pack="default" class="ion-edit"></i>
                                            </a>
                                            &nbsp;&nbsp;
                                            <a onclick="deleteRow('/packing-list/delete/{{ $pack->id }}', '/packing-list/list')" tabindex="-1">
                                                <i data-pack="default" class="ion-trash-a"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </form>
        {!! $packingList->links() !!}
    </div>
</div>

@stop


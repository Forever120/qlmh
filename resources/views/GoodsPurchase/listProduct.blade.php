<table class="table table-hover">
    <thead>
        <tr>
            <th>Tên sản phẩm</th>
            <th>SL sản phẩm</th>
            <th>Giá</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($productsPurchase))
            @foreach($productsPurchase as $pp)
                <tr>
                    <td>{{ $pp->pName . ' - ' . $pp->pName }}</td>
                    <td>{{ $pp->countProduct }}</td>
                    <td>{{ $pp->price }}</td>
                </tr>
            @endforeach
        @endif
    </tbody>

</table>

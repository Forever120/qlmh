<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Gộp đơn hàng</h4>
</div>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-lumped-orders"
              action="{{ $action }}"
              name="registerForm"
              novalidate=""
              class="form-lumped-orders"
              class="form-validate form-edit"
              method="post"
              enctype="multipart/form-data">
            {{ csrf_field()}}
            @foreach($listGoodsOrderID as $goodsOrderID)
                <input type='hidden' name='goodsOrderIDs[]' value="{{ $goodsOrderID }}">
            @endforeach
            <div class="card-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" >
                        <div class="row"><b>Chỉ định nhân viên mua hàng:</b></div>
                        <br>
                        <div class="main-item-pic">
                            <textarea placeholder="Tìm kiếm theo họ tên, tên đăng nhập, hoặc email của user"
                                      onchange="checkExistUser(this, '.picID')"
                                      class="textarea01 pic-value pic"
                                      name="picValue">{{ $nvmh->fullname or '' }}</textarea>
                            <input type="hidden" name="picID" value="{{ $nvmh->id or '' }}" class="picID"/>
                        </div>

                        <br>
                        <div class="row"><b>Danh sách NCC, vui lòng chọn 1 NCC để đặt hàng:</b></div>
                        <br>
                        @foreach($listSupplierOrders as $supplier)
                            <div class="row">
                                <div class="mb">
                                    <label class="mda-checkbox fnormal">
                                        <input  value="{{ $supplier->supplierID }}"
                                                {{ (isset($goodsPuchase->supplierID) && $goodsPuchase->supplierID == $supplier->supplierID) ? ' checked ':'' }}
                                                type="radio"
                                                name="supplierID"
                                                onclick="hideElement('.search-ncc')"/>
                                        <em class="bg-teal-500"></em>
                                        {{ $supplier->sName }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                        <div class="row">
                            <div class="mb">
                                <label class="mda-checkbox fnormal">
                                    <input value="0" onclick="showElement('.search-ncc')" name="supplierID" type="radio"/>
                                    <em class="bg-teal-500"></em>
                                    Chọn nhà cung cấp khác
                                </label>
                            </div>
                        </div>

                        <div class="row _hidden search-ncc">
                            <div data-autoclose="true" class="mda-form-group mda-input-group col-sm-6">
                                <label class="label-title01">Tìm theo mã NCC</label>
                                <div class="mda-form-control">
                                    <input name="supplierIDOther" type="number" value="" onchange="checkExistSupplier(this, 'id')" class="form-control supplierID">
                                    <div class="mda-form-control-line"></div>
                                </div>
                                <span class="mda-input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;SP -</span>
                                <label id="candidate-error" class="error productInvalid">Mã NCC không tồn tại.</label>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <textarea type="text" onchange="checkExistSupplier(this, 'name')" id="demo4" class="textarea01 product-name" name="name"></textarea><ul class="typeahead dropdown-menu"></ul>
                                        <div class="mda-form-control-line"></div>
                                        <label> Hoặc tìm kiếm theo tên NCC </label>
                                    </div>
                                    <label id="candidate-error" class="error productInvalid">Tên NCC không tồn tại.</label>
                                </div>
                            </div>
                        </div>

                         <br>
                        <div class="row"><b>Số lượng & giá sản phẩm sau khi gộp:</b></div>
                        <br>
                        <?php $totalPrice = 0?>
                        @foreach($mapPIdToLumpedProduct as $pro)
                            <?php $totalPrice += $pro['price']?>
                            <div class="row"><em>SP-{{ $pro['productID'] }}: {{ $pro['pName'] }}:</em></div>
                            <div class="row">
                                <div class="mda-form-group col-sm-6">
                                    <div class="mda-form-control">
                                        <input type="number" class="form-control" name="countProducts[]" value="{{ $pro['countProduct'] or '' }}" />
                                        <input type="hidden" value="{{ $pro['productID'] or '' }}" name="productIDs[]" />
                                        <div class="mda-form-control-line"></div>
                                        <label>Số lượng</label>
                                    </div>
                                </div>
                                <div class="mda-form-group col-sm-6">
                                    <div class="mda-form-control">
                                        <input type="number" class="form-control" onchange="updatePriceTotal()" name="prices[]" value="{{ $pro['price'] or '' }}" />
                                        <div class="mda-form-control-line"></div>
                                        <label>Giá bán</label>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <div class="row"><b>Thông tin đơn mua hàng:</b></div>
                        <br>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <input type="number" name="price" value="{{ $totalPrice }}" class="form-control price-total" readonly="" />
                                        <div class="mda-form-control-line"></div>
                                        <label>Giá đơn hàng (Tự động tính)</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <select name="currencyUnit"
                                                class="optionCustomer">
                                                <option value="vnd" {{ (isset($goodsPuchase->currencyUnit) && $goodsPuchase->currencyUnit == 'vnd' ? 'selected' : '') }}>VND</option>
                                                <option value="USD" {{ (isset($goodsPuchase->currencyUnit) && $goodsPuchase->currencyUnit == 'USD' ? 'selected' : '') }}>USD</option>
                                        </select>
                                        <div class="mda-form-control-line"></div>
                                        <label>Đơn vị</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <input type="number" name="exchangeMoney" value="{{ '0' }}"/>
                                        <div class="mda-form-control-line"></div>
                                        <label>Số tiền trả lần này</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <input type="number" name="remain" value="{{ $goodsPuchase->remain or '' }}" readonly=""/>
                                        <div class="mda-form-control-line"></div>
                                        <label>Số tiền còn lại</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
<!--                            <div class="col-sm-6">-->
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <select name="statusID" class="statusID">
                                            @foreach($listStatusOrder as $status)
                                                <option value="{{ $status->id }}" {{ (isset($goodsPuchase->statusID) && $goodsPuchase->statusID == $status->id) ? 'selected' : '' }}>{{ $status->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="mda-form-control-line"></div>
                                        <label>Trạng thái đơn hàng</label>
                                    </div>
                                </div>
                            <!--</div>-->
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <input type="date" name="orderDate" value="{{ $goodsPuchase->orderDate or '' }}"/>
                                        <div class="mda-form-control-line"></div>
                                        <label>Ngày đặt đặt hàng</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                       <input type="date" name="produceDate" value="{{ $goodsPuchase->produceDate or '' }}"/>
                                        <div class="mda-form-control-line"></div>
                                        <label>Ngày sản suất</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <input type="date" name="deliverDate" value="{{ $goodsPuchase->deliverDate or '' }}"/>
                                        <div class="mda-form-control-line"></div>
                                        <label>Ngày chuyển hàng</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                       <input type="date" name="arriveDate" value="{{ $goodsPuchase->arriveDate or '' }}"/>
                                        <div class="mda-form-control-line"></div>
                                        <label>Ngày nhận hàng</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="mda-form-group ">
                                <div class="mda-form-control">
                                    <input type="file" name="invoiceFilePath" value="{{ $goodsPuchase->invoiceFilePath or '' }}"/>
                                    <div class="mda-form-control-line"></div>
                                    <label>Hóa đơn</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-group ">
                                <div class="mda-form-control">
                                    <input type="file" name="packingListFilePath" value="{{ $goodsPuchase->packingListFilePath or '' }}"/>
                                    <div class="mda-form-control-line"></div>
                                    <label>packing list</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-group ">
                                <div class="mda-form-control">
                                    <input type="file" name="billOfLadingFilePath" value="{{ $goodsPuchase->billOfLadingFilePath or '' }}"/>
                                    <div class="mda-form-control-line"></div>
                                    <label>Bill</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-group ">
                                <div class="mda-form-control">
                                    <textarea name="note" class="form-control">{{ $goodsPuchase->note or '' }}</textarea>
                                    <div class="mda-form-control-line"></div>
                                    <label>Ghi chú</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <a class="btn btn-warning" data-dismiss="modal">Hủy</a>
                        <button type="button"
                                onclick="submitForm('.form-lumped-orders', '.loading', '.list-goods-orders', '{{ url('/goods-orders/list') }}')"
                                class="btn btn-primary btn-orders">
                            Cập nhật
                        </button>
                        <input type="submit">
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(function() {
        $('#demo4').typeahead({
        ajax: '/supplier/search',
                onSelect: function (data) {
                $('.supplierID').val(data.value)
                }
        });
    });

    $(function() {
        $('.pic-value').typeahead({
        ajax: '/user/search',
            onSelect: function(data) {
                $('.picID').val(data.value);
            }
        });
    });
</script>



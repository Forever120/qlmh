<table class="table table-hover">
    <thead>
        <tr>
            <th>Mã Mua hàng</th>
            <th>NCC</th>
            <th>NVMH</th>
            <th>Giá</th>
            <th>Tiền đã gửi</th>
            <th>Tiền còn lại</th>
            <th>Ngày đặt hàng</th>
            <th>Ngày sản suất</th>
            <th>Ngày chuyển hàng</th>
            <th>Ngày nhận hàng</th>
            <th>Ghi chú</th>
        </tr>
    </thead>
    <tbody>
        @foreach($listGoodsPuchase as $puchase)
        <tr class="pointer tr_{{ $puchase->id }}"
            onclick="showDetail('.tr_{{ $puchase->id }}', '.pdetail{{ $puchase->id }}', '.chitietsp{{ $puchase->id }}', '#pro_tab_{{ $puchase->id }}')">
            <td>
                MH-{{ $puchase->id }}
            </td>
            <td>{{ $puchase->sName }}</td>
            <td>{{ $puchase->uFullname }}</td>
            <td>{{ $puchase->price . '(' . $puchase->currencyUnit . ')' }}</td>
            <td>{{ $puchase->desposits }}</td>
            <td>{{ $puchase->remain }}</td>
            <td>{{ $puchase->orderDate }}</td>
            <td>{{ $puchase->produceDate }}</td>
            <td>{{ $puchase->deliverDate }}</td>
            <td>{{ $puchase->arriveDate }}</td>
            <td>{{ $puchase->note }}</td>
        </tr>
        <tr>
            <td colspan="9" style="display: none" class="pdetail pdetail{{ $puchase->id }}">
                <div class="_tab">
                    &nbsp;
                    <span id="pro_tab_{{ $puchase->id }}" onclick="loadContentTab(this, '.product{{ $puchase->id }}', '.product{{ $puchase->id }}','{{ url('goods-puchase/list-product'). '/' . $puchase->id }}')" class="tab_active">
                        Sản phẩm
                    </span>

                    <span id="lumped{{ $puchase->id }}" onclick="loadContentTab(this, '.lumped{{ $puchase->id }}', '.lumped{{ $puchase->id }}','{{ url('goods-puchase/get-goods-orders'). '/' . $puchase->id }}')" class="tab_active">
                        Đơn hàng được gộp
                    </span>

                    <span id="download{{ $puchase->id }}" onclick="loadContentTab(this, '.download{{ $puchase->id }}', '.product{{ $puchase->id }}','')" class="tab_active">
                        download file
                    </span>

                    @if(in_array('DMH_DELETE', $listUserPermission) || $puchase->userID == $user->id)
                        <a onclick="deleteRow('/goods-purchase/delete/{{ $puchase->id }}', '/goods-purchase/list', '.list-goods-purchase', 'reload')">Xóa ĐH</a>
                    @endif

                    @if(in_array('DMH_EDIT', $listUserPermission) || $puchase->userID == $user->id)

                        <a onclick="loadData('.modal-content02', '/goods-purchase/edit/{{ $puchase->id }}')"
                           data-toggle="modal"
                           data-target="#myModal">Sửa ĐH</a>
                    @endif
                </div>
                <div class="_hidden tab_content product{{ $puchase->id }}"></div>
                <div class="tab_content chitietsp{{ $puchase->id }}">
                    <div class="table-responsive psummary">
                        <ul>
                            <li><b>Mã số khách hàng:</b><span>{{ $puchase->customerID }}</span></li>
                            <li><b>Họ tên khách hàng:</b><span>{{ $puchase->fullname }}</span></li>
                            <li><b>Điện thoại:</b><span>{{ $puchase->phone }}</span></li>
                            <li><b>Skype:</b><span>{{ $puchase->skype }}</span></li>
                            <li><b>Email:</b><span>{{ $puchase->email }}</span></li>
                            <li><b>Địa chỉ:</b><span>{{ $puchase->address }}</span></li>
                        </ul>
                    </div>
                </div>
                <div class="_hidden tab_content lumped{{ $puchase->id }}"></div>
                <div class="_hidden tab_content download{{ $puchase->id }}">
                    <ul>
                        <li>
                            invoiceFilePath -
                            @if($puchase->invoiceFilePath != '')
                                <a href="/goods-puchase/download/invoiceFilePath/{{ $puchase->id }}">Download</a>
                            @else
                                <em>File không tồn tại</em>
                            @endif
                        </li>
                        <li>
                            packingListFilePath -
                            @if($puchase->invoiceFilePath != '')
                                <a href="/goods-puchase/download/packingListFilePath/{{ $puchase->id }}">Download</a>
                            @else
                                <em>File không tồn tại</em>
                            @endif
                        </li>
                        <li>
                            billOfLadingFilePath -
                            @if($puchase->invoiceFilePath != '')
                                <a href="/goods-puchase/download/billOfLadingFilePath/{{ $puchase->id }}">Download</a>
                            @else
                                <em>File không tồn tại</em>
                            @endif
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{!! $pagination !!}

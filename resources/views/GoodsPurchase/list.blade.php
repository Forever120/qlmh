@extends('Layouts.main')

@section('title')
Quản lý đơn mua hàng
@stop

@section('breadcrumb')
Quản lý đơn mua hàng
@stop

@section('avatar')
    @if(isset($user->avatar) && $user->avatar != '')
        <a href="">
            <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
        </a>
    @endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop


@section('content')
<style>
    .datepicker-days {
        z-index: 1000;
    }
</style>

<div class="container-fluid">
    <div class="card">
        <div id="bootgrid-basic-header" class="bootgrid-header container-fluid">
            <div class="row">
                <!--include('element.form_sreach_goods_orders')-->
            </div>
        </div>

        <div class="table-responsive list-table">
            @include('GoodsPurchase.btn')
            <br/>
            <br/>
            <div class="list-goods-purchase">
                @include('GoodsPurchase.listReload')
            </div>
        </div>
    </div>
</div>

@stop

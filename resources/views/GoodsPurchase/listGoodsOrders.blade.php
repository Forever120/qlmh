<table class="table table-hover">
    <thead>
        <tr>
            <th></th>
            <th>Mã đơn hàng</th>
            <th>NVKD</th>
            <th>Ngày nhận hàng NCC</th>
            <th>Ngày dao hàng cho khách</th>
            <th>Trạng thái</th>
        </tr>
    </thead>
    <tbody>
        @foreach($lstGoodsOrders as $orders)
        <tr class="pointer tr_{{ $orders->goID }}">
            <td><input onclick="addTmpOrders(this)" value="{{ $orders->goID }}" type="checkbox" name="orderID[]"/></td>
            <td onclick="showDetail('.tr_{{ $orders->goID }}', '.pdetail{{ $orders->goID }}', '.chitietsp{{ $orders->goID }}', '#pro_tab_{{ $orders->goID }}')">
                DH{{ $orders->goID }}
            </td>
            <td>
                <a onclick="loadData('.modal-content02', '/user/detail/{{ $orders->nvkdID }}')"
                   data-toggle="modal"
                   data-target="#myModal">
                    {{ $orders->fullname }}
                </a>
            </td>
            <td onclick="showDetail('.tr_{{ $orders->goID }}', '.pdetail{{ $orders->goID }}', '.chitietsp{{ $orders->goID }}', '#pro_tab_{{ $orders->goID }}')">
                {{ $orders->suplierDeliveryDate }}
            </td>
            <td onclick="showDetail('.tr_{{ $orders->goID }}', '.pdetail{{ $orders->goID }}', '.chitietsp{{ $orders->goID }}', '#pro_tab_{{ $orders->goID }}')">
                {{ $orders->deliveryToUserDate }}
            </td>
            <td>
                
                <select class="form-control" onchange="ChangeGoodsOrderStatus(this, '/goods-orders/change-status/{{ $orders->goID }}', 'Thay đổi trạng thái đơn đặt hàng lỗi, hãy làm lại')">
                    @foreach ($listStatus as $status)
                    <option value="{!! $status->id !!}" {!! (($orders->goStatusID == $status->id) ? 'selected' : '') !!}>
                        {!! $status->name !!}
                    </option>
                    @endforeach
                </select>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
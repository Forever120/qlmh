
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{!! (($id > 0) ? 'Sửa thông tin báo giá' : 'Tạo báo giá mới') !!}</h4>
</div>
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#form-edit-pro" aria-controls="home" role="tab" data-toggle="tab">Thông tin sản phẩm</a></li>
        <li role="presentation"><a href="#form-edit-notify" aria-controls="home" role="tab" data-toggle="tab">Thông tin báo giá</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="modal-body">
        <div class="card">
            <form id="form-register" 
                  action="/notify-price/edit/{{ $id }}"
                  name="registerForm" 
                  class="form-validate form-edit" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field()}}
                <div class="card-body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="form-edit-pro">   
                            @include('element.form_edit_product')
                            
                            <span onclick="addRow()" class="btn btn-primary">Thêm hàng</span>
                            <span onclick="addCollumn()" class="btn btn-primary">Thêm cột</span>
                            <div class="table-responsive">
                                @if(isset($notifyPrice->countOrders))
                                <?php $countOrders = json_decode($notifyPrice->countOrders, true) ?>
                                <table class="table table-hover tbl-thanhphan">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            @foreach($countOrders['listCollumn'] as $collumn)
                                            <th><input value="{{ $collumn }}" 
                                                       name="collumn[]"
                                                       class="form-control form-thanhphan" 
                                                       type="text" /></th>
                                            @endforeach
                                        </tr>
                                    </thead>.
                                    <tbody>
                                        @foreach($countOrders['row'] as $row)
                                        <tr>
                                            @foreach($row as $value)
                                            <td><input value="{{ $value }}" 
                                                       name="row[]"
                                                       class="form-control form-thanhphan" 
                                                       type="text" /></td>
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                <table class="table table-hover tbl-thanhphan">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><input value="" 
                                                       name="collumn[]"
                                                       class="form-control form-thanhphan" 
                                                       type="text" /></th>
                                        </tr>
                                    </thead>.
                                    <tbody>
                                        <tr>
                                            <td><input value="" 
                                                       name="row[]"
                                                       class="form-control form-thanhphan" 
                                                       type="text" /></td>
                                            <td><input value="" 
                                                       name="row[]"
                                                       class="form-control form-thanhphan" 
                                                       type="text" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                                @endif

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="form-edit-notify">
                            @include('element.form_edit_notify_price')
                        </div>
                        <div class="modal-footer">  
                            <button class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" 
                                    onclick="submitForm('.form-edit', '.loading', '.list-notify-price', '/bao-gia')"
                                    class="btn btn-success">
                                Cập nhật
                            </button>
                            <div class="loading"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
            $('#datepicker-1').datepicker({
    container: '#example-datepicker-container-5'
    });

</script>
@extends('Layouts.main')

@section('title')
BÁO GIÁ
@stop

@section('breadcrumb')
Quản lý Báo giá
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('scriptCustom')
<script>
    $(document).ready(function() {
    loadCollumnPrice();
    });</script>
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <div id="bootgrid-basic-header" class="bootgrid-header container-fluid">
            <div class="row">
                @include('NotifyPrice.formSreachNotifyPrice')
            </div>
        </div>
        <div class="table-responsive list-notify-price" id="nestable">
            @include('NotifyPrice.list_reload')
        </div>
        <center> {!! $listNotifyPrice->links() !!} </center>

    </div>
</div>

@stop

<form action="" method="get">
    <div class="col-sm-12 actionBar">
        <div class="search form-group">
            <div class="input-group">
                <span class="icon input-group-addon ion-search"> 
                    <button type="submit" class="icon input-group-addon ion-search"></button>
                </span> 
                <input type="text" class="form-control" placeholder="Tìm theo tên hoặc mã sản phẩm">
            </div>
        </div>
        <div class="actions btn-group">
            <div class="dropdown btn-group">
                @if(in_array('BANG_GIA_EDIT', $listUserPermission))
                <span title="Thêm mới bảng giá" class="btn btn-default" 
                      data-toggle="modal" data-target="#myModal"
                      onclick="loadData('.modal-content02', '{{ url('notify-price/edit/0') }}')">
                    <a class="ion-android-add-circle"></a>
                </span>
                @endif
                
                <span title="xuất dữ liệu ra file excel" class="btn btn-default" onclick="">
                    <a class="ion-arrow-up-a"></a>
                </span>
                <span title="nhập dữ liệu từ file excel" class="btn btn-default" onclick="">
                    <a class="ion-arrow-down-a"></a>
                </span>
                <span title="Tìm kiếm nâng cao" class="btn btn-default" onclick="showDetail(this, '.main_search', '')">
                    <a class="ion-search"></a>
                </span>
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class="dropdown-text">hiển thị</span> 
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <?php
                    if (isset($_GET['show'])) {
                        $countShow = $_GET['show'];
                    } else {
                        $countShow = 20;
                    }
                    ?>
                    <li class="{{ $countShow == 20 ? 'active':'' }}" aria-selected="false">
                        <a data-action="20" 
                           href="?show=20"
                           class="dropdown-item dropdown-item-button">20</a>
                    </li>
                    <li class="{{ $countShow == 30 ? 'active':'' }}" aria-selected="true">
                        <a data-action="30" 
                           href="?show=30"
                           class="dropdown-item dropdown-item-button">30</a>
                    </li>
                    <li class="{{ $countShow == 40 ? 'active':'' }}" aria-selected="false">
                        <a data-action="40" 
                           href="?show=40"
                           class="dropdown-item dropdown-item-button">40</a></li>
                    <li class="{{ $countShow == 50 ? 'active':'' }}" aria-selected="false">
                        <a data-action="-1" 
                           href="?show=50"
                           class="dropdown-item dropdown-item-button">50</a>
                    </li>
                </ul>
            </div>
            @if(isset($listPrices)){
            <div class="dropdown btn-group">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class="dropdown-text"><span class="icon ion-ios-list-outline"></span></span> 
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu pull-right custom_collumn" role="menu">
                    @foreach ($listPrices as $priceName)
                    <li>
                        <label class="dropdown-item">
                            <input name="price[]"
                                   type="checkbox"
                                   value="price_{{ $priceName->id }}"
                                   class="dropdown-item-checkbox" 
                                   onclick="changeCollumnPrice(this, '{{ $priceName->id }}')"
                                   {{ array_search($priceName->id, $listPriceActive) != false ? ' checked ' : '' }}> 
                                   {{ $priceName->name }}
                        </label>
                    </li>
                    @endforeach
                    <li><a href="/collumn/list" class="dropdown-item">Quản lý collumn</a></li>
                </ul>
            </div>
            @endif
        </div>
    </div>
    <div class="col-sm-12 _top10 main_search">
        <br/>
        <div class="grid-item">
            <h6 class="mt0">
                <a class="glyphicon glyphicon-cog pointer" 
                   href="{{ url('category/list') }}"
                   aria-hidden="true"></a>
                <span>Chọn danh mục</span>
            </h6>
            <ul class="list-unstyled content-item-search">
                @foreach($lstCate as $cateID => $cate)
                <li>
                    <div class="checkbox c-checkbox">
                        <label>
                            <input type="checkbox" 
                                   name="category[]" 
                                   @if(in_array($cateID, $getRequest['category']))
                                   checked 
                                   @endif
                                   value="{{ $cateID }}" />  
                                   <span class="ion-checkmark-round"></span>
                            {{ $cate }}
                        </label>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="grid-item">
            <h6 class="mt0">
                <a class="glyphicon glyphicon-cog pointer" 
                   href="{{ url('color/list') }}"
                   aria-hidden="true"></a>
                <span>Chọn Màu sắc</span>
            </h6>
            <ul name="color" class="list-unstyled content-item-search">
                @foreach($lstColor as $colorID => $color)
                <li>
                    <div class="checkbox c-checkbox">
                        <label>
                            <input type="checkbox" 
                                   name="color[]" 
                                   @if(in_array($colorID, $getRequest['color']))
                                   checked 
                                   @endif
                                   value="{{ $colorID }}" />  
                                   <span class="ion-checkmark-round"></span>
                            {{ $color }}
                        </label>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="grid-item">
            <h6 class="mt0">
                <a class="glyphicon glyphicon-cog pointer"
                   href="{{ url('chat-lieu/list') }}"
                   aria-hidden="true"></a>
                <span>Chọn chất liệu</span>
            </h6>
            <ul name="material" class="list-unstyled content-item-search">
                @foreach($lstMaterial as $materialID => $material)
                <li>
                    <div class="checkbox c-checkbox">
                        <label>
                            <input type="checkbox" 
                                   name="material[]" 
                                   @if(in_array($materialID, $getRequest['material']))
                                   checked 
                                   @endif
                                   value="{{ $materialID }}" /> 
                                   <span class="ion-checkmark-round"></span>
                            {{ $material }}
                        </label>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="grid-item">
            <h6 class="mt0">Thể tích</h6>
            <ul class="list-unstyled ul_search02">
                <li>
                    <input type="number" 
                           class="form-control input-sm"
                           name="volume1" 
                           placeholder="Từ"
                           value="{{ $getRequest['volume1'] }}" /> 
                </li>
                <li>
                    <input type="number" 
                           class="form-control input-sm"
                           name="volume2" 
                           placeholder="Đến"
                           value="{{ $getRequest['volume2'] }}" /> 
                </li>
            </ul>
            <h6 class="mt0">Khối lượng</h6>
            <ul name="material" class="list-unstyled ul_search02">
                <li>
                    <input type="number" 
                           class="form-control input-sm"
                           name="weight1" 
                           placeholder="Từ"
                           value="{{ $getRequest['weight1'] }}" /> 
                </li>
                <li>
                    <input type="number" 
                           class="form-control input-sm"
                           name="weight2" 
                           placeholder="Đến"
                           value="{{ $getRequest['weight2'] }}" /> 
                </li>
            </ul>
        </div>
    </div>
</form>
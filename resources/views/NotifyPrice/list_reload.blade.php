<form action="{{ url('bao-gia/export') }}" class="form-export" method="post">
    {{ csrf_field()}}
    @include('NotifyPrice.btn')
    <table class="table table-hover">
        <thead>
            <tr>
                <th class="np_code" {!! ((array_search('1', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>Mã YCBG</th>
                <th class="np_pro_name" {!! ((array_search('2', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>Tên sản phẩm</th>
                <th class="np_nvkd_name" {!! ((array_search('3', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>NVKD</th>
                <th class="np_created_at" {!! ((array_search('4', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>Ngày tạo</th>
                <th class="np_deadlnie" {!! ((array_search('5', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>Deadline</th>
                <th class="np_status" {!! ((array_search('6', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>Trạng thái</th>
                <th class="np_note" {!! ((array_search('7', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>Ghi chú</th>
            </tr>
        </thead>.
        <tbody>
            <?php $idx = 0;?>
            @foreach($listNotifyPrice as $notifyPrice)
            <?php $idx++?>
            <tr class="pointer">
                <td class="np_code" width="100px" {!! ((array_search('1', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>
                    YCBG-{{ $notifyPrice->npID }}
                </td>
                <td class="np_pro_name" width="250px" class=" td_{{ $notifyPrice->npID }}" onclick="showDetail('.td_{{ $notifyPrice->npID }}', '.pdetail{{ $notifyPrice->npID }}', '.chitietsp{{ $notifyPrice->npID }}')" {!! ((array_search('2', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>
                    {{ $notifyPrice->name }}
                </td>
                <td class="np_nvkd_name" width="200px" class=" td_{{ $notifyPrice->npID }}" onclick="showDetail('.td_{{ $notifyPrice->npID }}', '.pdetail{{ $notifyPrice->npID }}', '.chitietsp{{ $notifyPrice->npID }}')" {!! ((array_search('3', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}> {{ $notifyPrice->nvkd_name }} </td>
                <td class="np_created_at" width="120px" class=" td_{{ $notifyPrice->npID }}" onclick="showDetail('.td_{{ $notifyPrice->npID }}', '.pdetail{{ $notifyPrice->npID }}', '.chitietsp{{ $notifyPrice->npID }}')" {!! ((array_search('4', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>{{substr($notifyPrice->np_created_at, 0, 10)}}</td>
                <td class="np_deadlnie" width="120px" class=" td_{{ $notifyPrice->npID }}" onclick="showDetail('.td_{{ $notifyPrice->npID }}', '.pdetail{{ $notifyPrice->npID }}', '.chitietsp{{ $notifyPrice->npID }}')" {!! ((array_search('5', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>{{substr($notifyPrice->deadline, 0, 10)}}</td>
                <td class="np_status" width="200px" class=" td_{{ $notifyPrice->npID }}" onclick="showDetail('.td_{{ $notifyPrice->npID }}', '.pdetail{{ $notifyPrice->npID }}', '.chitietsp{{ $notifyPrice->npID }}')" {!! ((array_search('6', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>
                    <select onchange="ChangeNotifyPriceStatus(this, '/bao-gia/change-status/{{ $notifyPrice->npID }}', 'Thay đổi trạng thái báo giá lỗi, hãy làm lại')" name="statusID" class="form-control">
                        @foreach ($status as $st)
                            <option {{ $st->id == $notifyPrice->statusID ? 'selected':'' }} value="{{ $st->id }}">{{ $st->name }}</option>
                        @endforeach
                    </select>
                </td>
                <td class="np_note" width="200px" class=" td_{{ $notifyPrice->npID }}" onclick="showDetail('.td_{{ $notifyPrice->npID }}', '.pdetail{{ $notifyPrice->npID }}', '.chitietsp{{ $notifyPrice->npID }}')" {!! ((array_search('7', $listNotifyPriceActive) === false) ? ' style="display: none" ' : '') !!}>{{ $notifyPrice->npnote }} </td>-
            </tr>
            <tr>
                <td colspan="9" style="display: none" class="pdetail pdetail{{ $notifyPrice->npID }}">
                    <div class="_tab">
                        &nbsp;
                        <span onclick="loadContentTab(this, '.chitietsp{{ $notifyPrice->npID }}', '.chitietsp{{ $notifyPrice->npID }}', '')"
                              class="tab_active"> Số lượng </span>
                        <span onclick="loadContentTab(this, '.product{{ $notifyPrice->npID }}', '.img_product{{ $notifyPrice->npID }}','{{ url('images'). '/' . $notifyPrice->id }}')">
                            Sản phẩm
                        </span>
                        <span  onclick="loadContentTab(this, '.thanhphan{{ $notifyPrice->npID }}', '.thanhphan{{ $notifyPrice->npID }}','{{  url('sub-product'). '/' . $notifyPrice->id }}')">
                            Sản phẩm - Thành phần
                        </span>


                        @if(in_array('BAO_GIA_DELETE', $listUserPermission))
                        <a onclick="deleteRow('{{ url('bao-gia/delete') . '/' . $notifyPrice->npID }}', '{{url('bao-gia')}}', '#nestable', 'reload')">Xóa</a>
                        @endif

                        @if(in_array('BAO_GIA_EDIT', $listUserPermission))
                        <a onclick="loadData('.modal-content02', '/goods-orders/edit/0?add2order=1&pid={{ $notifyPrice->id }}')"
                           data-toggle="modal"
                           data-target="#myModal">Đưa vào đơn hàng</a>
                        @endif

                        @if(in_array('BAO_GIA_EDIT', $listUserPermission))
                        <a onclick="loadData('.modal-content02', '/notify-price/edit/0?pid={{ $notifyPrice->npID }}')"
                           data-toggle="modal"
                           data-target="#myModal">Nhân bản</a>
                        @endif

                        @if(in_array('BAO_GIA_EDIT', $listUserPermission) || $notifyPrice->userID == $user->id)
                        <a onclick="loadData('.modal-content02', '/notify-price/edit/{{ $notifyPrice->npID }}')"
                           data-toggle="modal"
                           data-target="#myModal">Chỉnh sửa</a>
                        @endif

                        @if(in_array('BAO_GIA_EDIT', $listUserPermission) || $notifyPrice->userID == $user->id)
                        <a onclick="loadData('.modal-content02', '/edit-product/0?parentID={{ $notifyPrice->productID }}')"
                           data-toggle="modal"
                           data-target="#myModal">Thêm thành phần</a>
                        @endif

                    </div>
                    <div class="tab_content chitietsp{{ $notifyPrice->npID }}">
                        <?php $countOrders = json_decode($notifyPrice->countOrders, true)?>
                        <div class="table-responsive">
                        @php
                            $haveOrder = false;
                            foreach($countOrders['row'] as $row) {
                                foreach($row as $value) {
                                    if (!empty($value)) {
                                        $haveOrder = true;
                                        break;
                                    }
                                }
                            }
                        @endphp
                            @if($haveOrder)
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        @foreach($countOrders['listCollumn'] as $collumn)
                                        <th>{{ $collumn }}</th>
                                        @endforeach
                                    </tr>
                                </thead>.
                                <tbody>
                                    @foreach($countOrders['row'] as $row)
                                    <tr>
                                        @foreach($row as $value)
                                        <td>{{ $value }}</td>
                                        @endforeach
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <h5>Dữ liệu về số lượng trống</h5>
                            @endif
                        </div>
                    </div>
                    <div class="_hidden tab_content product{{ $notifyPrice->npID }}">
                        <div class="img_product{{ $notifyPrice->npID }} thumbnail img_product"></div>
                        <div class="psummary">
                            <ul>
                                <li><b>Tên sản phẩm:</b><span>{{ $notifyPrice->name }}</span></li>
                                <li><b>Mã sản phẩm:</b><span>{{ $notifyPrice->id }}</span></li>
                                <li><b>Nhóm sản phẩm:</b><span>{{ $lstCate[$notifyPrice->categoryID] or '' }}</span></li>
                                <li><b>Chất liệu:</b><span>{{ $lstMaterial[$notifyPrice->materialID] or '' }}</span></li>
                                <li><b>Màu sắc:</b><span>{{ $lstColor[$notifyPrice->colorID] or '' }}</span></li>
                                <li><b>Khối lượng:</b><span>{{ $notifyPrice->weight }}</span></li>
                                <li><b>Thể tích:</b><span>{{ $notifyPrice->volume }}</span></li>
                                <li><b>Mô tả:</b><span>{{ $notifyPrice->description }}</span></li>
                                <!-- <li><b>Ghi chú:</b><span>{{ $notifyPrice->note }}</span></li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="_hidden tab_content thanhphan{{ $notifyPrice->npID }}"></div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</form>
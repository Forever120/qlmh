<div class="panel panel-primary">
    <div class="panel panel-heading">
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myLargeModalLabel" class=" panel-title">{!! (($id > 0) ? 'Sửa color' : 'Tạo color') !!}</h4>
    </div>
    <div class="modal-body ">
        <div class="card">
            <form id="form-register" 
                  action="{{ url('color/edit/'.$id) }}"
                  name="registerForm" 
                  novalidate="" 
                  class="form-validate edit-form" 
                  method="post" 
                  enctype="multipart/form-data">
                {{ csrf_field()}}
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="mda-form-group">
                                <div class="mda-form-control">
                                    <input name="name"
                                           value="{{ $color->name or '' }}"
                                           required="" 
                                           tabindex="0" 
                                           aria-required="true" 
                                           aria-invalid="true" 
                                           class="form-control">
                                    <div class="mda-form-control-line"></div>
                                    <label>Nhập tên color</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="clearfix">
                        <div class="pull-left">
                            <button type="button" onclick="editForm('{{ url('color/list') }}')" class="btn btn-primary">Cập nhật</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" >Close</button>
                        </div> 

                    </div>
                    <div class="pull-left edit-result _success"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<input type="file" name="images[]" multiple="5">
<hr>
<ul class="main_img_edit">
    @foreach($images as $img)
    <li class="img_{{ $img->id }}">
        <div><span onclick="deleteImage('{{ $img->id }}')">X</span></div>
        <img src="{{URL::to('/')}}/{{ $img->path }}" >
    </li>
    @endforeach
</ul>
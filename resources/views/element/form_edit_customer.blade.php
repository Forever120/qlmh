<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group ">
            <div class="mda-form-control">
                <input type="text" 
                       name="fullname"
                       value="{{ $customer->fullname or '' }}"
                       required="" 
                       class="form-control customer customer-name" 
                       aria-required="true" 
                       {{ $disableCustomerForm or '' }}
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label>Họ tên</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group mb ">
            <div class="mda-form-control">
                <input name="phone" 
                       type="number" 
                       value="{{ $customer->phone or '' }}"
                       required="" 
                       tabindex="0" 
                       {{ $disableCustomerForm }}
                       aria-required="false" 
                       aria-invalid="false" 
                       class="form-control customer customer-phone">
                <div class="mda-form-control-line"></div>
                <label>Số điện thoại</label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb ">
            <div class="mda-form-control">
                <input name="skype" 
                       type="text" 
                       value="{{ $customer->skype or '' }}"
                       required="" 
                       tabindex="0" 
                       {{ $disableCustomerForm }}
                       aria-required="false" 
                       aria-invalid="false" 
                       class="form-control customer customer-skype">
                <div class="mda-form-control-line"></div>
                <label>Skype</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group ">
            <div class="mda-form-control customer">
                <input type="text" 
                       name="email"
                       value="{{ $customer->email or '' }}"
                       required="" 
                       class="form-control customer  customer-email" 
                       aria-required="true" 
                       {{ $disableCustomerForm }}
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label>Email</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-9">
        <div class="mda-form-group mb ">
            <div class="mda-form-control">
                <textarea {{ $disableCustomerForm or '' }} 
                    name="address" 
                    class="form-control customer customer-address">{{ $customer->address or '' }}</textarea>
                <div class="mda-form-control-line"></div>
                <label>Địa chỉ</label>
            </div>
        </div>
    </div>
</div>

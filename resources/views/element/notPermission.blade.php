@extends('Layouts.main')

@section('title')
Không có quyền truy cập
@stop

@section('breadcrumb')
Không có quyền truy cập
@stop

@section('avatar')
<a href="">
    <img src="{{ $user->avatar != '' ? url($user->avatar):'/be/img/user/01.jpg' }}" alt="Profile" class="img-circle thumb64">
</a>
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('content')
<div class="container-fluid">
    <div class="card">
        <h3>Bạn không có quyền xem trang này</h3>
    </div>
</div>

@stop


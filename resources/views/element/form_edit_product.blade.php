<div class="row" hidden>
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <select name="ngungKD">
                    <option value="0">Đang kinh doanh</option>
                    <option value="1">Ngừng kinh doanh</option>
                </select>
                <div class="mda-form-control-line"></div>
                <label>Trạng thái sản phẩm</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input name="name"
                       value="{{ $product->name or '' }}"
                       tabindex="0"
                       aria-required="true"
                       aria-invalid="true"
                       class="form-control">
                <div class="mda-form-control-line"></div>
                <label>Tên sản phẩm</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <select name="categoryID" class="form-control">
                    <option value="">--- Chọn danh mục ---</option>
                    @foreach($listCategory as $cate)
                    <option
                        @if(isset($product->categoryID) && $product->categoryID == $cate->id)
                        {{ ' selected ' }}
                        @endif
                        value="{{ $cate->id }}">{{ $cate->name }}
                </option>
                @endforeach
            </select>
            <div class="mda-form-control-line"></div>
            <label>Nhóm sản phẩm</label>
        </div>
    </div>
</div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb {{ isset($product->id) ? '' : 'float-label' }}">
            <div class="mda-form-control">
                <select class="form-control" name="colorID">
                    <option value="">--- Chọn màu sắc ---</option>
                    @foreach($listColors as $color)
                    <option
                        @if(isset($product->colorID) && $product->colorID == $color->id)
                        {{ ' selected ' }}
                        @endif
                        value="{{ $color->id }}">

                        {{ $color->name }}</option>
                    @endforeach
                </select>
                <div class="mda-form-control-line"></div>
                <label>Màu sắc</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-control">
                <select class="form-control" name="materialID">
                    <option {{ isset($cate->id) ? 'checked' : '' }} value="">--- Chọn chất liệu ---</option>
                    @foreach($listMaterial as $material)
                    <option
                        @if(isset($product->materialID) && $product->materialID == $material->id)
                        {{ ' selected ' }}
                        @endif
                        value="{{ $material->id }}">
                        {{ $material->name }}
                </option>
                @endforeach
            </select>
            <div class="mda-form-control-line"></div>
            <label>Chất liệu</label>
        </div>
    </div>
</div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group ">
            <div class="mda-form-control">
                <input type="number"
                       name="volume"
                       value="{{ $product->volume or '' }}"
                       class="form-control"
                       aria-required="true"
                       aria-invalid="true">
                <div class="mda-form-control-line"></div>
                <label>Thể tích (ml)</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group mb ">
            <div class="mda-form-control">
                <input name="weight"
                       type="number"
                       value="{{ $product->weight or '' }}"
                       tabindex="0"
                       aria-required="false"
                       aria-invalid="false"
                       class="form-control">
                <div class="mda-form-control-line"></div>
                <label>Khối lượng (g)</label>
            </div>
        </div>
    </div>
</div>

<div class="row">
<!--     <div class="col-sm-6">
        <div class="mda-form-group mb {{ isset($product->note) ? '' : 'float-label' }}">
            <div class="mda-form-control">
                <textarea name="note"
                          rows="4"
                          aria-multiline="true"
                          tabindex="0"
                          aria-invalid="false"
                          class="form-control">{{ $product->note or '' }}</textarea>
                <div class="mda-form-control-line"></div>
                <label>Ghi chú</label>
            </div>
            <!--<span class="mda-form-msg">Keyword should be a maximum of 155 characters long.</span>
        </div>
    </div> -->
    <div class="col-sm-12">
        <div class="mda-form-group mb {{ isset($product->id) ? '' : 'float-label' }}">
            <div class="mda-form-control">
                <textarea name="description"
                          rows="4"
                          aria-multiline="true"
                          tabindex="0"
                          aria-invalid="false"
                          class="form-control">{{ $product->description or '' }}
                </textarea>
                <div class="mda-form-control-line"></div>
                <label>Mô tả</label>
            </div>
        </div>
    </div>
</div>
@include('element.upload_image_product')

<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control rel-wrapper ui-datepicker ui-datepicker-popup dp-theme-success " 
                 id="example-datepicker-container-5">
                <input name="deadline"
                       value="{{ $notifyPrice->deadline or '' }}"
                       tabindex="0" 
                       data-date-format="yyyy-mm-dd"
                       id="datepicker-1"
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control datepicker">
                <div class="mda-form-control-line"></div>
                <label>Ngày cần admin báo giá</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-group mb">
                <div class="mda-form-control">
                    <select name="statusID" class="form-control">
                        <option value="">--- Trạng thái ---</option>
                        @foreach ($status as $st)
                            <option {{ $st->id == $notifyPrice->statusID ? 'selected':'' }} value="{{ $st->id }}">{{ $st->name }}</option>
                        @endforeach
                    </select>
                    <div class="mda-form-control-line"></div>
                    <label>Trạng thái</label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input name="name"
                       value="{{ $user->username or '' }}"
                       disabled=""
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control">
                <div class="mda-form-control-line"></div>
                <label>Nhân viên tạo báo giá</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-group mb">
                <div class="mda-form-control">
                    <input name="name"
                           value="{{ $notifyPrice->id ? 'YCBG-'.$notifyPrice->id : '' }}"
                           tabindex="0" 
                           disabled=""
                           aria-required="true" 
                           aria-invalid="true" 
                           class="form-control">
                    <div class="mda-form-control-line"></div>
                    <label>Mã yêu cầu báo giá</label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <input name="name"
                       value="{{ $notifyPrice->created_at or 'auto' }}"
                       disabled=""
                       tabindex="0" 
                       aria-required="true" 
                       aria-invalid="true" 
                       class="form-control">
                <div class="mda-form-control-line"></div>
                <label>Ngày tạo</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group">
            <div class="mda-form-group mb">
                <div class="mda-form-control">
                    <input name="name"
                           value="{{ $notifyPrice->updated_at or 'auto' }}"
                           tabindex="0" 
                           disabled=""
                           aria-required="true" 
                           aria-invalid="true" 
                           class="form-control">
                    <div class="mda-form-control-line"></div>
                    <label>Ngày update cuối</label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="mda-form-group mb">
        <div class="mda-form-control">
            <textarea name="noteNotify"
                      tabindex="0" 
                      class="form-control">{{ $notifyPrice->note or '' }}</textarea>
            <div class="mda-form-control-line"></div>
            <label>Ghi chú thêm</label>
        </div>
    </div>
</div>
<!-- Google Maps API-->
<!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBNs42Rt_CyxAqdbIBK0a5Ut83QiauESPA"></script>-->
<!-- build:js(../app) js/vendor.js-->
<!-- Modernizr-->
<script src="{{ url('be/vendor/modernizr/modernizr.custom.js') }}"></script>
<!-- jQuery-->
<script src="{{ url('be/vendor/jquery/dist/jquery.js') }}"></script>
<!-- Bootstrap-->
<script src="{{ url('be/vendor/bootstrap/dist/js/bootstrap.js') }}"></script>
<!-- jQuery Browser-->
<script src="{{ url('be/vendor/jquery.browser/dist/jquery.browser.js') }}"></script>
<!-- Material Colors-->
<script src="{{ url('be/vendor/material-colors/dist/colors.js') }}"></script>
<!-- Bootstrap Filestyle-->
<script src="{{ url('be/vendor/bootstrap-filestyle/src/bootstrap-filestyle.js') }}"></script>
<!-- Flot charts-->
<script src="{{ url('be/vendor/flot/jquery.flot.js') }}"></script>
<script src="{{ url('be/vendor/flot/jquery.flot.categories.js') }}"></script>
<script src="{{ url('be/vendor/flot-spline/js/jquery.flot.spline.js') }}"></script>
<script src="{{ url('be/vendor/flot.tooltip/js/jquery.flot.tooltip.js') }}"></script>
<script src="{{ url('be/vendor/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ url('be/vendor/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ url('be/vendor/flot/jquery.flot.time.js') }}"></script>
<script src="{{ url('be/vendor/sidebysideimproved/jquery.flot.orderBars.js') }}"></script>
<!-- jVector Maps-->
<script src="{{ url('be/vendor/ika.jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ url('be/vendor/ika.jvectormap/jquery-jvectormap-us-mill-en.js') }}"></script>
<script src="{{ url('be/vendor/ika.jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- Easypie Charts-->
<script src="{{ url('be/vendor/jquery.easy-pie-chart/dist/jquery.easypiechart.js') }}"></script>
<!-- Screenfull-->
<script src="{{ url('be/vendor/screenfull/dist/screenfull.js') }}"></script>
<!-- Sparkline-->
<script src="{{ url('be/vendor/sparkline/index.js') }}"></script>
<!-- Datepicker-->
<script src="{{ url('be/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<!-- jQuery Knob charts-->
<script src="{{ url('be/vendor/jquery-knob/js/jquery.knob.js') }}"></script>
<!-- Rickshaw-->
<script src="{{ url('be/vendor/d3/d3.js') }}"></script>
<script src="{{ url('be/vendor/rickshaw/rickshaw.js') }}"></script>
<!-- jQuery Form Validation-->
<script src="{{ url('be/vendor/jquery-validation/dist/jquery.validate.js') }}"></script>
<script src="{{ url('be/vendor/jquery-validation/dist/additional-methods.js') }}"></script>
<!-- JQUERY STEPS-->
<script src="{{ url('be/vendor/jquery.steps/build/jquery.steps.js') }}"></script>
<!--jquery form-->
<script src="{{ url('be/vendor/jquery-form/jquery-form.js') }}"></script>
<!-- Select2-->
<script src="{{ url('be/vendor/select2/dist/js/select2.js') }}"></script>
<!-- Clockpicker-->
<script src="{{ url('be/vendor/clockpicker/dist/bootstrap-clockpicker.js') }}"></script>
<!-- Range Slider-->
<script src="{{ url('be/vendor/nouislider/distribute/nouislider.js') }}"></script>
<!-- ColorPicker-->
<script src="{{ url('be/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js') }}"></script>
<!-- Summernote-->
<script src="{{ url('be/vendor/summernote/dist/summernote.js') }}"></script>
<!-- Dropzone-->
<script src="{{ url('be/vendor/dropzone/dist/dropzone.js') }}"></script>
<!-- Xeditable-->
<script src="{{ url('be/vendor/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js') }}"></script>
<!-- Momentjs-->
<script src="{{ url('be/vendor/moment/min/moment-with-locales.js') }}"></script>
<!-- Google Maps-->
<script src="{{ url('be/vendor/gmaps/gmaps.js') }}"></script>
<!-- Bootgrid-->
<script src="{{ url('be/vendor/jquery.bootgrid/dist/jquery.bootgrid.js') }}"></script>
<script src="{{ url('be/vendor/jquery.bootgrid/dist/jquery.bootgrid.fa.js') }}"></script>
<!-- Datatables-->
<script src="{{ url('be/vendor/datatables/media/js/jquery.dataTables.js') }}"></script>
<!-- Nestable-->
<script src="{{ url('be/vendor/nestable/jquery.nestable.js') }}"></script>
<!-- Sweet Alert-->
<script src="{{ url('be/vendor/sweetalert/dist/sweetalert-dev.js') }}"></script>
<!-- Masonry-->
<script src="{{ url('be/vendor/masonry/dist/masonry.pkgd.js') }}"></script>
<!-- Images Loaded-->
<script src="{{ url('be/vendor/imagesloaded/imagesloaded.pkgd.js') }}"></script>
<!-- Loaders.CSS-->
<script src="{{ url('be/vendor/loaders.css/loaders.css.js') }}"></script>
<!-- jQuery Localize-->
<script src="{{ url('be/vendor/jquery-localize-i18n/dist/jquery.localize.js') }}"></script>
<!-- Blueimp Gallery-->
<script src="{{ url('be/vendor/blueimp-gallery/js/blueimp-helper.js') }}"></script>
<script src="{{ url('be/vendor/blueimp-gallery/js/blueimp-gallery.js') }}"></script>
<script src="{{ url('be/vendor/blueimp-gallery/js/blueimp-gallery-fullscreen.js') }}"></script>
<script src="{{ url('be/vendor/blueimp-gallery/js/blueimp-gallery-indicator.js') }}"></script>
<script src="{{ url('be/vendor/blueimp-gallery/js/blueimp-gallery-video.js') }}"></script>
<script src="{{ url('be/vendor/blueimp-gallery/js/blueimp-gallery-vimeo.js') }}"></script>
<script src="{{ url('be/vendor/blueimp-gallery/js/blueimp-gallery-youtube.js') }}"></script>
<script src="{{ url('be/vendor/blueimp-gallery/js/jquery.blueimp-gallery.js') }}"></script>
<!-- Datamaps-->
<script src="{{ url('be/vendor/topojson/topojson.min.js') }}"></script>
<script src="{{ url('be/vendor/datamaps/dist/datamaps.all.js') }}"></script>
<!-- endbuild-->
<!-- App script-->
<script src="{{ url('be/js/app.js') }}"></script>
<!-- custom script-->
<script src="{{ url('be/js/function.js') }}"></script>

<script src="{{ url('be/vendor/auto-complete/jquery.mockjax.js') }}"></script>
<script src="{{ url('be/vendor/auto-complete/bootstrap-typeahead.js') }}"></script>
<link rel="stylesheet" href="{{ url('be/vendor/animate.css/animate.css') }}">
<!-- Bootstrap-->
<link rel="stylesheet" href="{{ url('be/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
<!-- Ionicons-->
<link rel="stylesheet" href="{{ url('be/vendor/ionicons/css/ionicons.css') }}">
<!-- Bluimp Gallery-->
<link rel="stylesheet" href="{{ url('be/vendor/blueimp-gallery/css/blueimp-gallery.css') }}">
<link rel="stylesheet" href="{{ url('be/vendor/blueimp-gallery/css/blueimp-gallery-indicator.css') }}">
<link rel="stylesheet" href="{{ url('be/vendor/blueimp-gallery/css/blueimp-gallery-video.css') }}">
<!-- Datepicker-->
<link rel="stylesheet" href="{{ url('be/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
<!-- Rickshaw-->
<link rel="stylesheet" href="{{ url('be/vendor/rickshaw/rickshaw.css') }}">
<!-- Select2-->
<link rel="stylesheet" href="{{ url('be/vendor/select2/dist/css/select2.css') }}">
<!-- Clockpicker-->
<link rel="stylesheet" href="{{ url('be/vendor/clockpicker/dist/bootstrap-clockpicker.css') }}">
<!-- Range Slider-->
<link rel="stylesheet" href="{{ url('be/vendor/nouislider/distribute/nouislider.min.css') }}">
<!-- ColorPicker-->
<link rel="stylesheet" href="{{ url('be/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css') }}">
<!-- Summernote-->
<link rel="stylesheet" href="{{ url('be/vendor/summernote/dist/summernote.css') }}">
<!-- Dropzone-->
<link rel="stylesheet" href="{{ url('be/vendor/dropzone/dist/basic.css') }}">
<link rel="stylesheet" href="{{ url('be/vendor/dropzone/dist/dropzone.css') }}">
<!-- Xeditable-->
<link rel="stylesheet" href="{{ url('be/vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') }}">
<!-- Bootgrid-->
<link rel="stylesheet" href="{{ url('be/vendor/jquery.bootgrid/dist/jquery.bootgrid.css') }}">
<!-- Datatables-->
<link rel="stylesheet" href="{{ url('be/vendor/datatables/media/css/jquery.dataTables.css') }}">
<!-- Sweet Alert-->
<link rel="stylesheet" href="{{ url('be/vendor/sweetalert/dist/sweetalert.css') }}">
<!-- Loaders.CSS-->
<link rel="stylesheet" href="{{ url('be/vendor/loaders.css/loaders.css') }}">
<!-- Material Floating Button-->
<link rel="stylesheet" href="{{ url('be/vendor/ng-material-floating-button/mfb/dist/mfb.css') }}">
<!-- Material Colors-->
<link rel="stylesheet" href="{{ url('be/vendor/material-colors/dist/colors.css') }}">
<!-- endbuild-->
<!-- Application styles-->
<link rel="stylesheet" href="{{ url('be/css/app.css') }}">
<!-- custom styles-->
<link rel="stylesheet" href="{{ url('be/css/custom.css') }}">
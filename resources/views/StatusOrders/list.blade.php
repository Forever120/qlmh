@extends('Layouts.main')

@section('title')
    Quản lý trạng thái đơn đặt hàng
@stop

@section('breadcrumb')
    Quản lý trạng thái đơn đặt hàng
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('content')
<div class="container-fluid">
    <div class="js-nestable-action">
        <a class="btn btn-primary btn-sm" onclick="updatePosition('{{ url('status-orders/update-position') }}')">Cập nhật số thứ tự</a>
        <a class="btn btn-primary btn-sm" onclick="loadPopupLarge('{{ url('status-orders/edit/0') }}')" data-toggle="modal" data-target=".bs-modal-lg">Thêm mới</a>
        <a class="btn btn-primary btn-sm" onclick="reload('#nestable', '{{ url('status-orders/list') }}')">refresh</a>
        <div class="loader-primary _success status-update"></div>
        <div class="row">
            <div class="col-md-8">
                <div id="nestable" class="dd">
                    {!! $htmlList !!}
                </div>
                <div id="nestable-output" class="well hidden"></div>
            </div>
        </div>
    </div>
</div>
@stop
<table class="table table-hover">
    <thead>
        <tr>
            <th></th>
            <th>Mã đơn hàng</th>
            <th>NVKD</th>
            <th>Ngày nhận hàng NCC</th>
            <th>Ngày dao hàng cho khách</th>
            <th>Trạng thái</th>
            <th>Giá</th>
        </tr>
    </thead>
    <tbody>
        @foreach($lstGoodsOrders as $orders)
        <tr class="pointer tr_{{ $orders->goID }}">
            <td><input onclick="addTmpOrders(this)" value="{{ $orders->goID }}" type="checkbox" name="orderID[]"/></td>
            <td onclick="showDetail('.tr_{{ $orders->goID }}', '.pdetail{{ $orders->goID }}', '.chitietsp{{ $orders->goID }}', '#pro_tab_{{ $orders->goID }}')">
                DH{{ $orders->goID }}
            </td>
            <td>
                <a onclick="loadData('.modal-content02', '/user/detail/{{ $orders->nvkdID }}')"
                   data-toggle="modal"
                   data-target="#myModal">
                    {{ $orders->fullname }}
                </a>
            </td>
            <td onclick="showDetail('.tr_{{ $orders->goID }}', '.pdetail{{ $orders->goID }}', '.chitietsp{{ $orders->goID }}', '#pro_tab_{{ $orders->goID }}')">
                {{ $orders->suplierDeliveryDate }}
            </td>
            <td onclick="showDetail('.tr_{{ $orders->goID }}', '.pdetail{{ $orders->goID }}', '.chitietsp{{ $orders->goID }}', '#pro_tab_{{ $orders->goID }}')">
                {{ $orders->deliveryToUserDate }}
            </td>
            <td>
                <select class="form-control" onchange="ChangeGoodsOrderStatus(this, '/goods-orders/change-status/{{ $orders->goID }}', 'Thay đổi trạng thái đơn đặt hàng lỗi, hãy làm lại')">
                    @foreach ($listStatus as $status)
                        <option value="{!! $status->id !!}" {!! (($orders->goStatusID == $status->id) ? 'selected' : '') !!}>
                            {!! $status->name !!}
                        </option>
                    @endforeach
                </select>
            </td>
            <td onclick="showDetail('.tr_{{ $orders->goID }}', '.pdetail{{ $orders->goID }}', '.chitietsp{{ $orders->goID }}', '#pro_tab_{{ $orders->goID }}')">
                {{ $orders->price }}
            </td>
        </tr>
        <tr>
            <td colspan="9" style="display: none" class="pdetail pdetail{{ $orders->goID }}">
                <div class="_tab">
                    &nbsp;
                    <span id="pro_tab_{{ $orders->goID }}" onclick="loadContentTab(this, '.product{{ $orders->goID }}', '.product{{ $orders->goID }}','{{ url('goods-orders/list-product'). '/' . $orders->goID }}')" class="tab_active">
                        Sản phẩm
                    </span>
                    <span onclick="loadContentTab(this, '.chitietsp{{ $orders->goID }}', '.chitietsp{{ $orders->goID }}', '')" >
                        Thông tin khách hàng
                    </span>
                    <span onclick="loadContentTab(this, '.ncc{{ $orders->goID }}', '.ncc{{ $orders->goID }}','{{ url('goods-orders/list-ncc'). '/' . $orders->goID }}')">
                        NCC
                    </span>

                    @if(in_array('DDH_DELETE', $listUserPermission) || $orders->userID == $user->id)
                    <a onclick="deleteRow('/goods-orders/delete/{{ $orders->goID }}', '/goods-orders/list', '.list-goods-orders')">Xóa ĐH</a>
                    @endif

                    @if(in_array('DDH_EDIT', $listUserPermission) || $orders->userID == $user->id)
                    <a onclick="loadData('.modal-content02', '/goods-orders/edit-ncc/0/{{ $orders->goID }}?isOldGoodsOrder=1')"
                       data-toggle="modal"
                       data-target="#myModal">Thêm NCC</a>

                    <a onclick="loadData('.modal-content02', '/goods-orders/edit-product/{{ $orders->goID }}/0?isOldGoodsOrder=1')"
                       data-toggle="modal"
                       data-target="#myModal">Thêm SP</a>

                    <a onclick="loadData('.modal-content02', '/goods-orders/edit/{{ $orders->goID }}')"
                       data-toggle="modal"
                       data-target="#myModal">Sửa ĐH</a>
                    @endif
                </div>
                <div class="_hidden tab_content product{{ $orders->goID }}"></div>
                <div class="tab_content chitietsp{{ $orders->goID }}">
                    <div class="table-responsive psummary">
                        <ul>
                            <li><b>Mã số khách hàng:</b><span>{{ $orders->customerID }}</span></li>
                            <li><b>Họ tên khách hàng:</b><span>{{ $orders->fullname }}</span></li>
                            <li><b>Điện thoại:</b><span>{{ $orders->phone }}</span></li>
                            <li><b>Skype:</b><span>{{ $orders->skype }}</span></li>
                            <li><b>Email:</b><span>{{ $orders->email }}</span></li>
                            <li><b>Địa chỉ:</b><span>{{ $orders->address }}</span></li>
                        </ul>
                    </div>
                </div>
                <div class="_hidden tab_content thanhphan{{ $orders->goID }}"></div>
                <div class="_hidden tab_content ncc{{ $orders->goID }}"></div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{!! $pagination !!}

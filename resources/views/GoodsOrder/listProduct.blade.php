@if(count($productOrder) > 0)
<table class="table table-hover">
    <thead>
        <tr>
            <th>Tên sản phẩm</th>
            <th>SL sản phẩm</th>
            <th>SSL package</th>
            <th>SL SP trong 1 package</th>
            <th>Giá</th>
            <th>Thao tác</th>
        </tr>
    </thead>
    <tbody>
        @foreach($productOrder as $po)
        <tr>
            <td>{{ $po->name }}</td>
            <td>{{ $po->countProduct }}</td>
            <td>{{ $po->CountPackage }}</td>
            <td>{{ $po->countProductToPackage }}</td>
            <td>{{ $po->price }}</td>
            <td>
                <a onclick="loadData('.modal-content02', '/goods-orders/edit-product/{{ $po->goodsOrdersID }}/{{ $po->poID }}?isOldGoodsOrder=1')"
                       data-toggle="modal"
                       data-target="#myModal"><i data-pack="default" class="ion-edit"></i>
                </a>
                &nbsp;&nbsp;
                <a onclick="deleteRow('/goods-orders/deleteProduct/{{ $po->poID }}', '/goods-orders/list-product/{{ $po->goodsOrdersID }}', '.product{{$po->goodsOrdersID}}')" tabindex="-1">
                    <i data-pack="default" class="ion-trash-a"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
    <h5>Chưa có sản phẩm nào trong đơn đặt hàng</h5>
@endif

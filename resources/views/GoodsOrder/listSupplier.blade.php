@if (count($supplier) > 0)
<form method="get" action="">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Tên NCC</th>
                <th>SĐT</th>
                <th>Email</th>
                <th>Website</th>
                <th>Mô tả</th>
                <th>NCC được chọn</th>
                <th>Xóa</th>
            </tr>
        </thead>
        <tbody>
            @foreach($supplier as $po)
            <tr>
                <td>{{ $po->sname }}</td>
                <td>{{ $po->supplierNvkdPhoneNumber }}</td>
                <td>{{ $po->supplierNvkdEmail }}</td>
                <td>{{ $po->supplierWebsite }}</td>
                <td>{{ $po->note }}</td>
                <td>
                    <div class="checkbox c-checkbox c-checkbox-rounded">
                        <label class="radio-inline c-radio">
                            <input id="inlineradio2"
                                   type="radio"
                                   name="nccStatus"
                                   {{ $po->soStatus == 1 ? ' checked ':'' }}
                            value="{{ $po->soID }}">
                            <span class="ion-record"></span>
                        </label>
                    </div>
                </td>
                <td>
                    @if(in_array('DDH_EDIT', $listUserPermission))
                        <a onclick="deleteRow('/goods-orders/delete-ncc/{{ $po->soID  }}', '{{ url('goods-orders/list-ncc/'.$po->goodsOrdersID) }}', '.ncc{{ $po->goodsOrdersID  }}')">
                            <i data-pack="default" class="ion-trash-a"></i>
                        </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</form>
@else
    <h5>Chưa chọn được NCC nào cho đơn hàng này</h5>
@endif

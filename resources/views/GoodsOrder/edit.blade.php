<?php $disableCustomerForm = ' disabled '?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{!! (($id > 0) ? 'Sửa thông tin đơn đặt hàng' : 'Tạo đơn hàng mới') !!}</h4>
</div>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab-01" aria-controls="home" role="tab" data-toggle="tab">Thông tin đơn hàng</a></li>
    <li role="presentation"><a href="#tab-02" aria-controls="profile" role="tab" data-toggle="tab">Khách hàng</a></li>
</ul>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register"
              action="/goods-orders/edit/{{ $id }}"
              name="registerForm"
              novalidate=""
              class="form-edit"
              class="form-validate form-edit"
              method="post"
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <input type="hidden" name="nvkdID" value="{{ $nvkdID }}" />
            <div class="card-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-01">
                        @if(isset($_GET['add2order']) && $_GET['add2order'] == 1)
                        <input type="hidden" name="add2order" value="1"/>
                        <input type="hidden" name="productID" value="{{ $_GET['pid'] or 0 }}"/>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <input type="number"
                                               name="countProduct"
                                               value="{{ $productOrder->countProduct or '' }}"
                                               required=""
                                               class="form-control"
                                               aria-required="true"
                                               aria-invalid="true">
                                        <div class="mda-form-control-line"></div>
                                        <label>Số lượng sản phẩm</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <input type="number"
                                               name="price1Product"
                                               value="{{ $productOrder->price1Product or '' }}"
                                               required=""
                                               class="form-control"
                                               aria-required="true"
                                               aria-invalid="true">
                                        <div class="mda-form-control-line"></div>
                                        <label> Giá một SP </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <input type="number"
                                               name="CountPackage"
                                               value="{{ $productOrder->CountPackage or '' }}"
                                               required=""
                                               class="form-control"
                                               aria-required="true"
                                               aria-invalid="true">
                                        <div class="mda-form-control-line"></div>
                                        <label>Số lượng package</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <input type="number"
                                               name="countProductToPackage"
                                               value="{{ $productOrder->countProductToPackage or '' }}"
                                               required=""
                                               class="form-control"
                                               aria-required="true"
                                               aria-invalid="true">
                                        <div class="mda-form-control-line"></div>
                                        <label> Số lượng SP trong 1 package </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <select name="optionGoodsOrders"
                                                onclick="checkGoodsOrders()"
                                                class="optionGoodsOrders">
                                            <option value="addNew">Tạo mới đơn hàng</option>
                                            <option value="chosenByOldList">Chọn từ danh sách có sẵn</option>
                                        </select>
                                        <div class="mda-form-control-line"></div>
                                        <label>Chọn Phương thức nhập đơn hàng</label>
                                    </div>
                                </div>
                            </div>
                            <div data-autoclose="true" class="mda-form-group mda-input-group col-sm-6">
                                <label class="label-title01">Mã đơn hàng</label>
                                <div class="mda-form-control">
                                    <input name="goodsOrdersID"
                                           disabled=""
                                           type="number"
                                           value="{{ $customer->id or '' }}"
                                           onchange="findGoodsOrderByID(this)"
                                           class="form-control goodsOrdersID">
                                    <div class="mda-form-control-line"></div>
                                </div><span class="mda-input-group-addon">DH -</span>
                            </div>
                            <label id="candidate-error"
                                   class="error goodsOrdersInvalid hidden01"
                                   for="candidate">Mã đơn hàng này không tồn tại.</label>
                        </div>

                        @endif

                        @include('GoodsOrder.formEditGoodsOrders')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-02">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <select name="option"
                                                onclick="checkFormCustomer()"
                                                class="optionCustomer">
                                            <option value="chosenByOldList">Chọn từ danh sách có sẵn</option>
                                            <option value="addNew">Tạo mới khách hàng</option>
                                        </select>
                                        <div class="mda-form-control-line"></div>
                                        <label>Chọn Phương thức nhập khách hàng</label>
                                    </div>
                                </div>
                            </div>
                            <div data-autoclose="true" class="mda-form-group mda-input-group col-sm-6">
                                <label class="label-title01">Mã khách hàng</label>
                                <div class="mda-form-control">
                                    <input name="customerID"
                                           type="number"
                                           value="{{ $customer->id or '' }}"
                                           onchange="findCustomerByID(this)"
                                           class="form-control customer-id">
                                    <div class="mda-form-control-line"></div>
                                </div><span class="mda-input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;KH -</span>
                            </div>
                            <label id="candidate-error"
                                   class="error customerInvalid hidden01"
                                   for="candidate">Mã khách hàng này không tồn tại.</label>
                        </div>
                        @include('element/form_edit_customer')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Xóa</button>
                        <button type="button"
                                onclick="submitForm('.form-edit', '.loading', '.list-goods-orders', '{{ url('/goods-orders/list') }}', 'goSubmit')"
                                class="btn btn-primary btn-orders">
                            Lưu + thoát
                        </button>
                        <button type="button"
                                onclick="submitForm('.form-edit', '.loading', '.list-goods-orders', '{{ url('/goods-orders/edit-product') }}', 'goAddProduct')"
                                class="btn btn-success">
                            Lưu + thêm Product
                        </button>
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>

//    var cpInput = $('.clockpicker').clockpicker();
    // auto close picker on scroll
    $('main').scroll(function() {
    cpInput.clockpicker('hide');
    });
    $(function() {
    $('#demo4').typeahead({
    ajax:
            '/product/search',
            onSelect: function (data) {
            $('.productID').val(data.value)
            }
    });
    });
</script>

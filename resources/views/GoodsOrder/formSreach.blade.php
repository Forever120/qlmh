<form action="" method="get">

    <div class="col-sm-12 _top10 main_search {{ isset($_GET['goodsOrdersID']) ? 'active-search-form':'' }}">

        <br>
        <div class="grid-item">
            <h6 class="mt0">Mã hoặc trạng thái</h6>
            <ul class="list-unstyled ul_search02">
                <li>
                    <input name="goodsOrdersID"
                           value="{{ $_GET['goodsOrdersID'] or '' }}"
                           tabindex="0"
                           placeholder="Mã đơn hàng"
                           class="form-control input-sm ">
                </li>
                <li>
                    <select name="statusID" class="form-control input-sm">
                        <option  value="0">Chọn trạng thái đơn hàng</option>
                        @foreach($listStatus as $status)
                            @php
                            if (isset($_GET['statusID']) && $_GET['statusID'] == $status->id) {
                                $selected = ' selected ';
                            } else {
                                $selected = '';
                            }
                            @endphp
                            <option  value="{{ $status->id }}" {{ $selected }}>{{ $status->name }}</option>
                        @endforeach
                    </select>
                </li>
            </ul>
        </div>
        <div class="grid-item">
            <h6 class="mt0">Thông tin sản phẩm</h6>
            <ul class="list-unstyled ul_search02">
                <li>
                    <input name="productID"
                           value="{{ $_GET['productID'] or '' }}"
                           tabindex="0"
                           placeholder="Mã sản phẩm"
                           class="form-control input-sm ">
                </li>
                <li>
                    <input name="pName"
                           value="{{ $_GET['pName'] or '' }}"
                           tabindex="0"
                           placeholder="Tên sản phẩm"
                           class="form-control input-sm ">
                </li>
            </ul>
        </div>
        <div class="grid-item">
            <h6 class="mt0">Ngày nhận hàng</h6>
            <ul class="list-unstyled ul_search02">
                <li>
                    <input name="suplierDeliveryDate1"
                           data-date-format="yyyy-mm-dd"
                           value="{{ $_GET['suplierDeliveryDate1'] or '' }}"
                           tabindex="0"
                           placeholder="Từ ngày"
                           class="form-control input-sm datepicker">
                </li>
                <li>
                    <input type="text"
                           data-date-format="yyyy-mm-dd"
                           class="form-control input-sm datepicker"
                           name="suplierDeliveryDate2"
                           placeholder="Đến ngày"
                           value="{{ $_GET['suplierDeliveryDate2'] or '' }}" />
                </li>
            </ul>
        </div>
        <div class="grid-item">
            <h6 class="mt0">Ngày giao hàng</h6>
            <ul class="list-unstyled ul_search02">
                <li>
                    <input type="text"
                           class="form-control input-sm datepicker"
                           name="deliveryToUserDate1"
                           placeholder="Từ ngày"
                           value="{{ $_GET['deliveryToUserDate1'] or '' }}" />
                    <div class="rel-wrapper ui-datepicker ui-datepicker-popup dp-theme-success col-sm-5" id="example-datepicker-container-6"></div>
                </li>
                <li>
                    <input type="text"
                           class="form-control input-sm datepicker"
                           name="deliveryToUserDate2"
                           placeholder="Đến ngày"
                           value="{{ $_GET['deliveryToUserDate2'] or '' }}" />
                    <div class="rel-wrapper ui-datepicker ui-datepicker-popup dp-theme-success col-sm-5" id="example-datepicker-container-5"></div>
                </li>
            </ul>
        </div>
        @if ($isViewAll)
        <div class="grid-item">
            <div class="checkbox c-checkbox">
              <label>
                  <input type="checkbox"
                   name="viewType"
                   {{ ($isSelectViewAll) ? 'checked' : '' }}
                   value="{{ true }}" />
                   <span class="ion-checkmark-round"></span>
                   Xem toàn bộ đơn hàng chưa được gộp
              </label>
          </div>
        </div>
        @endif
        <div class="grid-item">
            <input type="submit" class="btn btn-primary" value="Tìm kiếm"/>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <label class="label-title01">Ngày nhận hàng từ nhà cung cấp</label>
            <div class="mda-form-control rel-wrapper ui-datepicker ui-datepicker-popup dp-theme-success "
                 id="example-datepicker-container-5">
                <input name="suplierDeliveryDate01"
                       value="{{ isset($goodsOrder->suplierDeliveryDate) ? date('Y-m-d', strtotime($goodsOrder->suplierDeliveryDate)):'' }}"
                       tabindex="0"
                       data-date-format="yyyy-mm-dd"
                       id="datepicker-1"
                       aria-required="true"
                       aria-invalid="true"
                       class="form-control datepicker suplierDeliveryDate01 goodsorders">

                <div class="mda-form-control-line"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <label class="label-title01">Ngày giao hàng cho khách</label>
            <div class="mda-form-control rel-wrapper ui-datepicker ui-datepicker-popup dp-theme-success" id="example-datepicker-container-5">
                <input name="deliveryToUserDate01"
                       data-date-format="yyyy-mm-dd"
                       value="{{ isset($goodsOrder->suplierDeliveryDate) ? date('Y-m-d', strtotime($goodsOrder->deliveryToUserDate)):'' }}"
                       tabindex="0"
                       id="datepicker-2"
                       aria-required="true"
                       aria-invalid="true"
                       class="form-control datepicker deliveryToUserDate01 goodsorders">
                <div class="mda-form-control-line"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <select name="statusID"
                        required=""
                        class="form-control statusID goodsorders"
                        aria-required="true"
                        aria-invalid="true">
                    @foreach($listStatus as $status)
                    @php
                    if (isset($goodsOrder->statusID) && $goodsOrder->statusID == $status->id) {
                        $selected = ' selected ';
                    } else {
                        $selected = '';
                    }
                    @endphp
                    <option {{ $selected }} value="{{ $status->id }}">{{ $status->name }}</option>
                    @endforeach
                </select>
                <div class="mda-form-control-line"></div>
                <label> Trạng thái đơn hàng </label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="mda-form-group mb">
            <div class="mda-form-control">
                <textarea name="note" class="gnote goodsorders form-control">{{ $goodsOrder->note or '' }}</textarea>
                <div class="mda-form-control-line"></div>
                <label>Ghi chú</label>
            </div>
        </div>
    </div>

</div>

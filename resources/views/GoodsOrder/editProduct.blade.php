<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{!! (($productOrderID > 0) ? 'Sửa thông tin sản phẩm trong đơn hàng' : 'Thêm sản phẩm vào đơn hàng') !!}</h4>
</div>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab-01" aria-controls="home" role="tab" data-toggle="tab">Thông tin sản phẩm</a></li>
</ul>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register"
              action="/goods-orders/edit-product/{{$goodsOrdersID}}/{{ $productOrderID }}"
              name="registerForm"
              novalidate=""
              class="form-edit"
              class="form-validate form-edit"
              method="post"
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-01">
                        <div class="row">
                            <div data-autoclose="true" class="mda-form-group mda-input-group col-sm-6">
                                <label class="label-title01">Mã sản phẩm</label>
                                <div class="mda-form-control">
                                    <input name="productID"
                                           type="number"
                                           value="{{ $product->id or '' }}"
                                           onchange="checkExistProduct(this, 'id')"
                                           class="form-control product-id">
                                    <div class="mda-form-control-line"></div>
                                </div>
                                <span class="mda-input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;SP -</span>
                                <label id="candidate-error"
                                       class="error productInvalid"
                                       for="candidate">Mã sản phẩm không tồn tại.</label>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <textarea type="text"
                                                  onchange="checkExistProduct(this, 'name')"
                                                  id="demo4"
                                                  class="textarea01 product-name"
                                                  name="name">{{ $product->name or '' }}</textarea>
                                        <div class="mda-form-control-line"></div>
                                        <label> Tên sản phẩm </label>
                                    </div>
                                    <label id="candidate-error"
                                           class="error productInvalid"
                                           for="candidate">Tên sản phẩm không tồn tại.</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <input type="number"
                                               name="countProduct"
                                               value="{{ $productOrder->countProduct or '' }}"
                                               required=""
                                               class="form-control"
                                               aria-required="true"
                                               aria-invalid="true">
                                        <div class="mda-form-control-line"></div>
                                        <label>Số lượng sản phẩm</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <input type="number"
                                               name="price"
                                               value="{{ $productOrder->price or '' }}"
                                               required=""
                                               class="form-control"
                                               aria-required="true"
                                               aria-invalid="true">
                                        <div class="mda-form-control-line"></div>
                                        <label> Giá </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                      @if (!$isOldGoodsOrder)
                        <button type="button"
                                onclick="deleteRow('/goods-orders/delete', '/goods-orders/list', '.list-goods-orders', 'onModal')"
                                class="btn btn-danger">
                            Xóa
                        </button>
                        <button type="button"
                                onclick="exitForm('#myModal', '.list-goods-orders', '{{ url('/goods-orders/list') }}', 'goClose')"
                                class="btn btn-default">
                            Close
                        </button>
                        <button type="button"
                                onclick="submitForm('.form-edit', '.loading', '.list-goods-orders', '{{ url('/goods-orders/list') }}', 'goSubmit')"
                                class="btn btn-primary">
                            Lưu
                        </button>
                        <button type="button"
                                onclick="submitForm('.form-edit', '.loading', '.list-goods-orders', '{{ url('/goods-orders/edit-product') }}', 'goAddMoreProduct')"
                                class="btn btn-success">
                            Lưu + thêm SP
                        </button>
                        <button type="button"
                                onclick="submitForm('.form-edit', '.loading', '.list-goods-orders', '{{ url('/goods-orders/edit-ncc') }}', 'goAddNCC')"
                                class="btn btn-primary">
                            Lưu + thêm NCC
                        </button>
                        @else
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button"
                            onclick="submitForm('.form-edit', '.loading', '.product{{ $goodsOrdersID }}', '{{ url('goods-orders/list-product'). '/' . $goodsOrdersID }}', 'goEditSubmit')"
                            class="btn btn-primary">
                            Lưu
                        </button>
                        @endif
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>

</div>
<script>
  $('#datepicker-1').datepicker({
    container: '#example-datepicker-container-5'
  });
  $('#datepicker-2').datepicker({
    container: '#example-datepicker-container-5'
  });
  var cpInput = $('.clockpicker').clockpicker();
  // auto close picker on scroll
  $('main').scroll(function() {
    cpInput.clockpicker('hide');
  });
  $(function() {
    $('#demo4').typeahead({
    ajax: '/product/search',
      onSelect: function (data) {
        $('.productID').val(data.value)
      }
    });
  });
</script>

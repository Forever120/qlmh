<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">
        {{ $supplierOrderID == 0 ? 'Thêm nhà cung cấp vào đơn hàng':'Sửa nhà cung cấp trong đơn hàng' }}
    </h4>
</div>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab-01" aria-controls="home" role="tab" data-toggle="tab">Thông tin sản phẩm</a></li>
</ul>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register"
              action="/goods-orders/edit-ncc/{{$supplierOrderID}}/{{ $goodsOrdersID }}"
              name="registerForm"
              novalidate=""
              class="form-edit"
              class="form-validate form-edit"
              method="post"
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-01">
                        <div class="row">
                            <div data-autoclose="true" class="mda-form-group mda-input-group col-sm-6">
                                <label class="label-title01">Mã Nhà cung cấp: </label>
                                <div class="mda-form-control">
                                    <input name="supplierID"
                                           type="number"
                                           value="{{ $supplier->id or '' }}"
                                           onchange="checkExistSupplier(this, 'id')"
                                           class="form-control product-id">
                                    <div class="mda-form-control-line"></div>
                                </div>
                                <span class="mda-input-group-addon">&nbsp;NCC -</span>
                                <label id="candidate-error"
                                       class="error productInvalid"
                                       for="candidate">Mã NCC không tồn tại.</label>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <textarea type="text"
                                                  onchange="checkExistSupplier(this, 'name')"
                                                  id="demo4"
                                                  class="textarea01 product-name"
                                                  name="name">{{ $supplier->name or '' }}</textarea>
                                        <div class="mda-form-control-line"></div>
                                        <label> Tên nhà cung cấp </label>
                                    </div>
                                    <label id="candidate-error"
                                           class="error productInvalid"
                                           for="candidate">Tên NCC không tồn tại.</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="tab-01">
                        <div class="row">
                            <div data-autoclose="true" class="mda-form-group mda-input-group col-sm-6">
                                <label class="label-title01">Đặt là nhà cung cấp được chọn: </label>
                                <div class="mda-form-control">
                                    <select name="status">
                                        <option value="0" {{ isset($supplierOrder->status) && intval($supplierOrder->status) == 0 ? ' selected ':'' }}>Bỏ chọn</option>
                                        <option value="1" {{ isset($supplierOrder->status) && intval($supplierOrder->status) == 1 ? ' selected ':'' }}>Chọn làm mặc đinh</option>
                                    </select>
                                    <div class="mda-form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        @if (!$isOldGoodsOrder)
                        <button type="button"
                                onclick="deleteRow('/goods-orders/delete', '/goods-orders/list', '.list-goods-orders', 'onModal')"
                                class="btn btn-danger">
                            Xóa
                        </button>
                        <button type="button"
                                onclick="exitForm('#myModal', '.list-goods-orders', '{{ url('/goods-orders/list') }}', 'goClose')"
                                class="btn btn-default">
                            Close
                        </button>
                        <button type="button"
                                onclick="submitForm('.form-edit', '.loading', '.list-goods-orders', '{{ url('/goods-orders/list') }}', 'goSubmit')"
                                class="btn btn-primary">
                            Lưu
                        </button>
                        <button type="button"
                                onclick="submitForm('.form-edit', '.loading', '.list-goods-orders', '{{ url('/goods-orders/edit-ncc') }}', 'goAddNCC')"
                                class="btn btn-success">
                            Lưu + thêm NCC
                        </button>
                        @else
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button"
                            onclick="submitForm('.form-edit', '.loading', '.ncc{{ $goodsOrdersID }}', '{{ url('goods-orders/list-ncc'). '/' . $goodsOrdersID }}', 'goEditSubmit')"
                            class="btn btn-primary">
                            Lưu
                        </button>
                        @endif
                        <div class="loading"></div>
                    </div>
            </div>
        </form>

    </div>
    <script>
                        $(function() {
                        $('#demo4').typeahead({
                        ajax: '/supplier/search',
                                onSelect: function (data) {
                                $('.productID').val(data.value)
                                }
                        });
                        });
    </script>

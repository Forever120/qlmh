@extends('Layouts.main')

@section('title')
Quản lý đơn đặt hàng
@stop

@section('breadcrumb')
Quản lý đơn hàng
@stop

@section('avatar')
@if(isset($user->avatar) && $user->avatar != '')
<a href="">
    <img src="{{ url($user->avatar) }}" alt="Profile" class="img-circle thumb64">
</a>
@endif
<div class="mt">Welcome, {{ $user->username }}</div>
@stop

@section('scriptCustom')
<script>
  $('.datepicker').datepicker({
    container: '#example-datepicker-container-6'
  });
  $( document ).ready(function() {
  });
</script>
@stop

@section('content')
<style>
    .datepicker-days {
        z-index: 1000;
    }
</style>

<div class="container-fluid">
    <div class="card">
        <div id="bootgrid-basic-header" class="bootgrid-header container-fluid">
            <div class="row">
                @include('GoodsOrder.formSreach')
            </div>
        </div>

        <div class="table-responsive list-table" id="nestable">
            @include('GoodsOrder.btn')
            <br/>
            <br/>
            <div class="list-goods-orders">
                @include('GoodsOrder.listReload')
            </div>
        </div>

    </div>
</div>

<!--gop don hang-->

<div class="modal fade" id="confirmAddOrders" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-content03 form-lumped-order"></div>
    </div>
</div>

@stop

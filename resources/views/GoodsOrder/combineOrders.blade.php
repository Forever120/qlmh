<?php $disableCustomerForm = ' disabled ' ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Sửa thông tin đơn đặt hàng</h4>
</div>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#tab-01" aria-controls="home" role="tab" data-toggle="tab">Thông tin đơn hàng</a></li>
    <li role="presentation"><a href="#tab-02" aria-controls="profile" role="tab" data-toggle="tab">Khách hàng</a></li>
</ul>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register" 
              action="/goods-orders/edit/"
              name="registerForm" 
              novalidate="" 
              class="form-edit"
              class="form-validate form-edit" 
              method="post" 
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <input type="hidden" name="nvkdID" value=" $nvkdID " />
            <div class="card-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab-01">
                        @include('GoodsOrder.formEdit')
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-02">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="mda-form-group ">
                                    <div class="mda-form-control">
                                        <select name="option" 
                                                onclick="checkFormCustomer()"
                                                class="optionCustomer">
                                            <option value="chosenByOldList">Chọn từ danh sách có sẵn</option>
                                            <option value="addNew">Thêm mới khách hàng</option>
                                        </select>
                                        <div class="mda-form-control-line"></div>
                                        <label>Họ tên</label>
                                    </div>
                                </div>
                            </div>
                            <div data-autoclose="true" class="mda-form-group mda-input-group col-sm-6">
                                <label class="label-title01">Mã khách hàng</label>
                                <div class="mda-form-control">
                                    <input name="customerID" 
                                           type="number" 
                                           value="{{ $customer->id or '' }}" 
                                           onchange="findCustomerByID(this)" 
                                           class="form-control customer-id">
                                    <div class="mda-form-control-line"></div>
                                </div><span class="mda-input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;KH -</span>
                            </div>
                            <label id="candidate-error" 
                                   class="error customerInvalid hidden01" 
                                   for="candidate">Mã khách hàng này không tồn tại.</label>
                        </div>
                        @include('element/form_edit_customer')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" 
                                onclick="submitForm('.form-edit', '.loading', '.list-table', '{{ url('/goods-orders/list') }}')"
                                class="btn btn-primary">
                            Cập nhật
                        </button>
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
                    $('#datepicker-1')
                    .datepicker({
                    container: '#example-datepicker-container-5'
                    });
                    $('#datepicker-2')
                    .datepicker({
                    container: '#example-datepicker-container-5'
                    });
                    var cpInput = $('.clockpicker').clockpicker();
                    // auto close picker on scroll
                    $('main').scroll(function() {
            cpInput.clockpicker('hide');
            });
                    $(function() {
                    $('#demo4').typeahead({
                    ajax: '/product/search',
                            onSelect: function (data) {
                            $('.productID').val(data.value)
                            }
                    });
                    });
</script>
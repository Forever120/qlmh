<div class="container-fluid">
    <div class="card">
        <div class="table-responsive">
            @if(count($list) > 0)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên Nhà cung cấp</th>
                        <th>Số lượng sản phẩm</th>
                        <th>Giá bán</th>
                        <th>Thao tác</th>
                    </tr>
                </thead>.
                <tbody>
                    @if(count($list) > 0)
                    <?php $idx = 0;?>
                    @foreach($list as $ps)
                    <?php $idx++?>
                    <tr>
                        <td>{{ $idx }}</td>
                        <td> {{ $ps->supplierName }} </td>
                        <td> {{ number_format($ps->countProduct) }} </td>
                        <td> {{ number_format($ps->price) }} </td>
                        <td>
                            <a onclick="loadData('.modal-content02', '/group/edit/3')" data-toggle="modal" data-target="#myModal">
                                <i data-pack="default" class="ion-edit"></i>
                            </a>
                            &nbsp;
                            <a onclick="deleteRow('/group/delete/3', '/group/list')">
                                <i data-pack="default" class="ion-trash-a"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
            @else
                <h5>Chưa chọn Nhà cung cấp cho sản phẩm này</h5>
            @endif
        </div>
    </div>
</div>



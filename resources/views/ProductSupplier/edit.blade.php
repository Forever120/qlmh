
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{{ $psID == 0 ? 'Thêm mới nhà cung cấp':'Sửa thông tin nhà cung cấp' }}</h4>
</div>

<!-- Tab panes -->
<div class="modal-body">
    <div class="card">
        <form id="form-register" 
              action="/product-supplier/edit/{{ $psID }}/{{ $productID }}"
              name="registerForm" 
              class="form-validate form-edit" 
              method="post" 
              enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="card-body">
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="count">
                        <div class="row">
                            <div data-autoclose="true" class="mda-form-group mda-input-group col-sm-6">
                                <label class="label-title01">Tìm theo mã NCC</label>
                                <div class="mda-form-control">
                                    <input name="supplierID" 
                                           type="number" 
                                           value="{{ $productSupplier->supplierID or '' }}" 
                                           onchange="checkExistSupplier(this, 'id')" 
                                           class="form-control supplierID">
                                    <div class="mda-form-control-line"></div>
                                </div>
                                <span class="mda-input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;SP -</span>
                                <label id="candidate-error" 
                                       class="error productInvalid" >Mã NCC không tồn tại.</label>
                            </div>
                            <div class="col-sm-6">
                                <div class="mda-form-group mb">
                                    <div class="mda-form-control">
                                        <textarea type="text" 
                                                  onchange="checkExistSupplier(this, 'name')" 
                                                  id="demo4" 
                                                  class="textarea01 product-name"
                                                  name="name">{{ $supplier->name or '' }}</textarea>
                                        <div class="mda-form-control-line"></div>
                                        <label> Hoặc tìm kiếm theo tên NCC </label>
                                    </div>
                                    <label id="candidate-error" 
                                           class="error productInvalid" >Tên NCC không tồn tại.</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mda-form-group col-sm-6">
                                <div class="mda-form-control">
                                    <input name="countProduct" 
                                           type="number" 
                                           value="{{ $productSupplier->countProduct or '' }}" 
                                           class="form-control ">
                                    <div class="mda-form-control-line"></div>
                                    <label>Số lượng sản phẩm</label>
                                </div>
                            </div>
                            <div class="mda-form-group col-sm-6">
                                <div class="mda-form-control">
                                    <input name="price" 
                                           type="number" 
                                           value="{{ $productSupplier->price or '' }}" 
                                           class="form-control">
                                    <div class="mda-form-control-line"></div>
                                    <label>Giá bán</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">  
                        <button class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" 
                                onclick="submitForm('.form-edit', '.loading', '.nhacungcap{{ $productID }}', 'product-supplier/list/{{ $productID }}')"
                                class="btn btn-success btnSubmit">
                            {{ $psID == 0 ? 'Thêm mới':'Cập nhật' }}
                        </button>
                        <div class="loading"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>

$(function() {
    $('#demo4').typeahead({
    ajax: '/supplier/search',
            onSelect: function (data) {
            $('.supplierID').val(data.value)
            }
    });
});

</script>
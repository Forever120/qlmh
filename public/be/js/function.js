
var loading = '&nbsp;&nbsp;&nbsp;<img src="/imgs/loading_02.gif" />';
var g_goodsOrdersID = 0

function updatePosition(ajaxUrl) {
    $('.status-update').html(loading);
    $.ajax({
        type: "get",
        url: ajaxUrl,
        data: {
            json: $('#nestable-output').html()
        },
        success: function (data) {
            $('.status-update').html(data);
        }
    });
}


function deleteRow(deleteAPI, urlReload, classReload='#nestable', type='normal') {
    swal({
        title: 'Bạn chắc chắn xóa item này?',
        text: 'Item này sẽ bị xóa hoàn toàn, bạn sẽ không thể backup lại!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Xóa',
        closeOnConfirm: false
    },
            function () {
                $.ajax({
                    type: "get",
                    url: deleteAPI,
                    success: function (data) {
                        swal('Deleted!', 'Xóa data thành công.', 'success');
                        reload(classReload, urlReload);
                        if (data == 'success') {
                            switch (type) {
                                case 'onModal':
                                    $('#myModal').modal('hide')
                                    break
                                case 'reload':
                                    location.reload();
                                    break
                            }
                            //reload(classReload, urlReload);
                        }
                    }
                });
            });
}


function loadPopupLarge(url) {
    $('.popup-content').html(loading);
    $.ajax({
        type: "get",
        url: url,
        success: function (data) {
            $('.popup-content').html(data);
        }
    });
}

function editForm(urlReload) {
    $('.edit-result').html(loading);
    $(".edit-form").ajaxForm({
        target: '.edit-result',
        success: function (data) {
            reload('#nestable', urlReload);
            $('.bs-modal-lg').modal('toggle');
        }
    }).submit();
}

function reload(classLoad, url) {
    $(classLoad).html(loading);
    $.ajax({
        url: url,
        data: {
            reload: 1
        },
        success: function (data) {
            $(classLoad).html(data);
        }
    });
}

function loadContent(classLoad, url, expandDefaultRow = 0) {
    console.log(classLoad);
    console.log(url);
    $(classLoad).html(loading);
    $.ajax({
        data: {
            reload: 1,
            expandDefaultRow: expandDefaultRow
        },
        url: url,
        success: function (data) {
            $(classLoad).html(data);
            console.log($(classLoad))
            if (expandDefaultRow > 0) {
                showDetail('.tr_' + expandDefaultRow, '.pdetail' + expandDefaultRow, '.chitietsp' + expandDefaultRow, '#pro_tab_' + expandDefaultRow)
            }
        }
    });
}

function ajaxLoadContent(urlAjax, classLoad) {
    $(classLoad).html(loading);
    $.ajax({
        url: urlAjax,
        success: function (data) {
            $(classLoad).html(data);
        }
    });
}

function ajaxEdit(urlAjax) {
    $('#ajax_product').html(loading);
    $.ajax({
        url: urlAjax,
        success: function (data) {
            $('#ajax_product').html(data);
        }
    });
}

function loadContentTab(_this, classTab, classLoad, urlAjax) {
    $('.tab_content').hide();
    $('.tab_active').removeClass('tab_active');
    $(_this).addClass('tab_active');
    $(classTab).show();
    if ($(classLoad).html() === '') {
        ajaxLoadContent(urlAjax, classLoad);
    }
}

function showDetail(_this, classDetail, tabShow, tabClickedDefault = null) {
    if ($(_this).hasClass('enable')) {
        $(classDetail).hide();
        $('.enable').removeClass('enable');
    } else {
        $('.enable').removeClass('enable');
        $(_this).addClass('enable');
        $('.pdetail').hide();
        $(classDetail).show();
        $(tabShow).show();
        if (tabClickedDefault != null) {
            $(tabClickedDefault).click();
        }
        $(classDetail + ' span:first-child').addClass('tab_active');
}
}

function closeContentLeft() {
    if ($('.content_left').hasClass('_close')) {
        $('.content_left').removeClass('_close');
        $('.content_left').addClass('_open');
        $('.content_right').removeClass('width100');
    } else {
        $('.content_left').removeClass('_open');
        $('.content_left').addClass('_close');
        $('.content_right').addClass('width100');
    }
}

function loadData(classLoad, urlAjax, data) {
    $(classLoad).html(loading);
    $.ajax({
        url: urlAjax,
        data: {
            data: data
        },
        success: function (result) {
            $(classLoad).html(result);
        }
    });
}

function showPopup(urlAjax, addClass) {

    $('#ajax_notice').html('<img style="margin-top:4px;float:left; width:43px" src="/imgs/icon/loading.gif" /><em style="font-size:12px; margin-left:10px; color:#090">Đang cập nhật dữ liệu ...</em>');
    $("#form_edit").ajaxForm({
        target: '#ajax_notice',
        success: function (data)
        {
            if (data === 'success') {
                $('.popup_box').fadeOut('fast');
                $('.ajax_content_popup').html('<span style="color:#090">Đang tải dữ liệu &nbsp;&nbsp;</span><img width="100px" src="/imgs/icon/loading.gif" width="43" height="11" />');
                if (urlAjax !== 'none') {
                    $.ajax({
                        url: urlAjax,
                        success: function (data) {
                            $('#content_ajax_tottal').html(data);
                        },
                        error: function () {
                            $('#content_ajax_tottal').html('Có lỗi xảy ra');
                        }
                    });
                }
            } else
            {
                $('.ajax_notice').html(data);
            }
        },
    }).submit();
}


function clickToEdit(elementClass, pathSubmit) {
    $(elementClass).editable({
        type: 'text',
        pk: 1,
        method: 'get',
        url: pathSubmit,
        ajaxOptions: {
            type: 'get',
            dataType: 'text'
        },
        success: function (response, newValue) {
            //msg will be shown in editable form
            if (response.status == 'error')
                alert(response.msg);
//                return response.msg;
            console.log(response);
        }
    });
}

function changePermisson(_this, url) {
    var ajaxUrl = url + '/' + $(_this).val();
    console.log(ajaxUrl);
    $.ajax({
        url: ajaxUrl,
        type: "GET",
        success: function (data) {
            permission = $.parseJSON(data);
            $("input[name='permission[]']").each(function () {
                this.removeAttribute("checked");
                this.checked = false;
                console.log(permission.indexOf($(this).val()));
                if (0 <= permission.indexOf($(this).val())) {
                    this.setAttribute("checked", "checked");
                    this.checked = true;
                }
            });
        }
    });
}
function editDetailUserInfo(ajaxUrl, btnID, formID) {
    $(btnID).addClass('disabled');
    $.ajax({
        url: ajaxUrl,
        type: "POST",
        data: $(formID).serialize(),
        success: function (data) {
            $(btnID).removeClass('disabled');
            console.log(data);
        }
    });
}


function changeCollumnPrice(_this, lstPriceID) {
    console.log(_this);
    $('input[name="price[]"]').prop('disabled', true);
    var type;
    if ($(_this).prop('checked')) {
        $('.' + $(_this).val()).show();
        type = 'add';
    } else {
        $('.' + $(_this).val()).hide();
        type = 'unset';
    }
    $.ajax({
        url: '/change-lstprice/' + lstPriceID + '/' + type,
        success: function (data) {
            $('input[name="price[]"]').prop('disabled', false);
        },
        error: function (err) {
            $('input[name="price[]"]').prop('disabled', false);
        }
    });
}

function changeCollumnNotifyPrice(_this, notifyPrice) {
    $('input[name="notifyPrice[]"]').prop('disabled', true);
    var type;
    if ($(_this).prop('checked')) {
        $('.' + $(_this).val()).show();
        type = 'add';
    } else {
        $('.' + $(_this).val()).hide();
        type = 'unset';
    }
    $.ajax({
        url: '/change-lstnotifyprice/' + notifyPrice + '/' + type,
        success: function (data) {
            $('input[name="notifyPrice[]"]').prop('disabled', false);
        },
        error: function (err) {
            $('input[name="notifyPrice[]"]').prop('disabled', false);
        }
    });
}

function loadCollumnPrice() {

    $(".custom_collumn input[name='price[]").each(function () {
        if ($(this).prop('checked')) {
            $('.' + $(this).val()).show();
        } else {
            $('.' + $(this).val()).hide();
        }
    });
}

function submitForm(classForm, classLoading, classReload, urlReload, type = '')
{
    $('.btnSubmit').prop('disabled', true);
    $(classLoading).html(loading);
    $(classForm).ajaxForm({
        target: classLoading,
        delegation: true,
        dataType: 'script',
        success: function (result)
        {
            var data = $.parseJSON(result);
            console.log(result);
            if (data[0] === 'success') {
                switch (type) {
                    case 'goAddProduct':
                    case 'goAddMoreProduct':
                        loadData('.modal-content02', (urlReload + '/' + data[2] + '/0'));
                        break
                    case 'goAddNCC':
                        loadData('.modal-content02', (urlReload + '/0' + '/' + data[2]));
                        break
                    case 'goSubmit':
                        if (classReload !== '') {
                            loadContent(classReload, urlReload, g_goodsOrdersID);
                        }
                        $('#myModal').modal('hide');
                        break;
                    case 'goEditSubmit':
                        console.log('goEditSubmit');
                        if (classReload !== '') {
                            loadContent(classReload, urlReload);
                        }
                        $('#myModal').modal('hide');
                        break;
                    case 'sAddDebit':
                        loadContent(classReload, urlReload);
                        $('#myModal').modal('hide');
                        break;
                    case 'reload':
                        location.reload();
                        break;
                    default:
                        // Submit ok, just close modal
                        $('#myModal').modal('hide');
                        $('#confirmAddOrders').modal('hide');
                        break
                }
                g_goodsOrdersID = data[2];
            } else {
                $(classLoading).html(data[1]);
            }
            $('.btnSubmit').prop('disabled', false);

        },
        error: function (result)
        {
            console.log('false');
            $('.btnSubmit').prop('disabled', false);
            $(classLoading).html('');
        }

    }).submit();
}

function exitForm(modelId, classReload, urlReload, type = '')
{
    switch (type) {
        case 'goClose':
            loadContent(classReload, urlReload, g_goodsOrdersID);
            $(modelId).modal('hide');
            break;
        default:
            $(modelId).modal('hide');
            break
}
}

function deleteImage(id) {
    $.ajax({
        url: '/detete-image/' + id,
        success: function (data) {
            $('.img_' + id).hide();
        }
    });
}

function addCollumn() {
    $(".tbl-thanhphan thead tr").append('<th><input value="" name="collumn[]" class="form-control form-thanhphan" type="text" /></th>');
    $(".tbl-thanhphan tbody tr").append('<td><input value="" name="row[]" class="form-control form-thanhphan" type="text" /></td>');
}
function addRow() {
    var data = $(".tbl-thanhphan tbody tr:first-child").html();
    $(".tbl-thanhphan tbody").append('<tr class="add-new">' + data + '</tr>');
    $('.add-new input').val('');
    $('.add-new').removeClass('add-new');
}

function checkExistProduct(_this, type) {
    var productName = $(_this).val();
    if (productName === '') {
        $('.productInvalid').show();
    } else {
        $.ajax({
            type: "get",
            url: '/product/check-exist',
            data: {
                type: type,
                value: productName
            },
            success: function (data) {
                if (type == 'name') {

                    if (data == 0) {
                        $('.productInvalid').show();
                        $('.product-id').val('');
                    } else {
                        $('.product-id').val(data);
                        $('.productInvalid').hide();
                    }
                } else if (type = 'id') {
                    if (data == 0) {
                        $('.productInvalid').show();
                        $('.product-name').val('');
                    } else {
                        $('.product-name').val(data);
                        $('.productInvalid').hide();
                    }
                }
            }
        });
    }
}

function checkExistSupplier(_this, type) {
    var productName = $(_this).val();
    if (productName === '') {
        $('.productInvalid').show();
    } else {
        $.ajax({
            type: "get",
            url: '/supplier/check-exist',
            data: {
                type: type,
                value: productName
            },
            success: function (data) {
                if (type == 'name') {

                    if (data == 0) {
                        $('.productInvalid').show();
                        $('.product-id').val('');
                    } else {
                        $('.product-id').val(data);
                        $('.productInvalid').hide();
                    }
                } else if (type = 'id') {
                    if (data == 0) {
                        $('.productInvalid').show();
                        $('.product-name').val('');
                    } else {
                        $('.product-name').val(data);
                        $('.productInvalid').hide();
                    }
                }
            }
        });
    }
}

function findCustomerByID(_this, type = 'GET') {
    var cid = $(_this).val();
    if (cid === '') {
        $('.customerInvalid').show();
    } else {
        $.ajax({
            type: "get",
            url: '/customer/search',
            data: {
                cid: cid,
            },
            success: function (data) {
                var dataObj = JSON.parse(data);
                if (data == 0) {
                    $('.customerInvalid').show();
                    $('.customer').val('');
                } else {
                    $('.customerInvalid').hide();
                    $('.customer-name').val(dataObj.fullname);
                    $('.customer-phone').val(dataObj.phone);
                    $('.customer-skype').val(dataObj.skype);
                    $('.customer-email').val(dataObj.email);
                    $('.customer-address').val(dataObj.address);
                }
            }
        });
}
}

function checkFormCustomer() {
    var chosen = $('.optionCustomer').val();
    if (chosen === 'addNew') {
        $('.customer').prop('disabled', false);
        $('.customer').val('');
        $('.customer-id').val('');
        $('.customer-id').prop('disabled', true);
        $('.customerInvalid').hide();
    } else {
        $('.customer').prop('disabled', true);
        $('.customer-id').prop('disabled', false);
    }
}

function checkAllProduct(_this) {
    if ($(_this).prop("checked") == true) {
        $("input[name='option[]']").each(function () {
            this.checked = true;
        });
    } else {
        $("input[name='option[]']").each(function () {
            this.checked = false;
        });
    }
    checkExport();
}

function checkExport() {
    $('.btn-export').prop('disabled', true);
    $("input[name='option[]']").each(function () {
        if ($(this).prop("checked") == true) {
            $('.btn-export').prop('disabled', false);
        }
    });

}



function findGoodsOrderByID(_this, type) {
    var id = $(_this).val();
    if (id === '') {
        $('.customerInvalid').show();
    } else {
        $.ajax({
            type: "get",
            url: '/goods-orders/find',
            data: {
                id: id,
            },
            success: function (data) {
                var dataObj = JSON.parse(data);
                if (data == 0) {
                    $('.goodsOrdersInvalid').show();
                    $('.customer').val('');
                    $('.goodsorders').val('');
                    $('.btn-orders').prop('disabled', true);
                } else {
                    $('.customerInvalid').hide();
                    $('.customer-name').val(dataObj.fullname);
                    $('.customer-phone').val(dataObj.phone);
                    $('.customer-skype').val(dataObj.skype);
                    $('.customer-email').val(dataObj.email);
                    $('.customer-address').val(dataObj.address);
                    $('.customer-address').val(dataObj.address);
                    $('.customer-id').val(dataObj.id);

                    $('.suplierDeliveryDate01').val(dataObj.suplierDeliveryDate01);
                    $('.suplierDeliveryDate02').val(dataObj.suplierDeliveryDate02);
                    $('.deliveryToUserDate01').val(dataObj.deliveryToUserDate01);
                    $('.deliveryToUserDate02').val(dataObj.deliveryToUserDate02);
                    $('.statusID').val(dataObj.statusID);
                    $('.gnote').val(dataObj.gnote);
                    $('.btn-orders').prop('disabled', false);
                }
            }
        });
    }
}

function checkGoodsOrders() {
    var chosen = $('.optionGoodsOrders').val();
    if (chosen === 'addNew') {
        $('.customer').prop('disabled', false);
        $('.customer').val('');
        $('.goodsorders').prop('disabled', false);
        $('.goodsorders').val('');
        $('.optionCustomer').prop('disabled', false);
        $('.optionCustomer').prop('disabled', false);
        $('.customer-id').prop('disabled', false);
        $('.customer-id').val('');

        $('.goodsOrdersID').val('');
        $('.goodsOrdersID').prop('disabled', true);
        $('.customerInvalid').hide();

        $('.btn-orders').prop('disabled', false);
    } else {
        $('.customer').prop('disabled', true);
        $('.customer-id').prop('disabled', true);
        $('.optionCustomer').prop('disabled', true);
        $('.optionCustomer').prop('disabled', true);

        $('.goodsorders').prop('disabled', true);
        $('.goodsOrdersID').prop('disabled', false);

        $('.btn-orders').prop('disabled', true);
    }
}

function ChangeNotifyPriceStatus(sel, changeNotifyStatusAPI, msgChangeFail) {
    changeNotifyStatusAPI += ('/' + sel.value);
    $.ajax({
        type: "GET",
        url: changeNotifyStatusAPI,
        success: function (data) {
            if (data === 'success') {
            }
        },
        error: function () {
            alert(msgChangeFail);
        }
    });
}

function ChangeGoodsOrderStatus(sel, changeGoodsOrderStatusAPI, msgChangeFail)
{
    changeGoodsOrderStatusAPI += ('/' + sel.value);
    //console.log(changeGoodsOrderStatusAPI);
    $.ajax({
        type: "GET",
        url: changeGoodsOrderStatusAPI,
        success: function (data) {

        },
        error: function () {
            alert(msgChangeFail);
        }
    });
}

function addTmpOrders(_this) {
    $('.btn-add-orders').prop('disabled', true);
    $("input[name='orderID[]']").each(function () {
        if ($(this).prop("checked") == true) {
            $('.btn-add-orders').prop('disabled', false);
        }
    });
    var classAdd = '.item-' + $(_this).val();
    var numItems = $(classAdd).length;

    if ($(_this).prop("checked") == true) {
        if (numItems === 0) {
            var html = '<li class="item item-' + $(_this).val() + '">\n\
                        <input style="display: none" value="' + $(_this).val() + '"type="checkbox" name="addOrders[]"/>\n\
                        <span>DH-' + $(_this).val() + '</span>\n\
                    </li>';
            $('.ul-add-orders').append(html);
        }
    } else {
        if (numItems > 0) {
            $(classAdd).remove();
        }
    }



}

function lumpedOrders() {
    //get all order id đã chọn được gộp
    var lstGoodsOrders = [];
    $("input[name='addOrders[]']").each(function () {
        if (lstGoodsOrders.indexOf($(this).val()) < 0) {
            lstGoodsOrders.push($(this).val());
        }
    });
    loadData('.form-lumped-order', '/goods-purchase/lumped-goods-orders/', lstGoodsOrders);
}

function loadPaginator(_this, classElement, ajaxUrl) {

    $('.pagination').children('li').removeClass('active');
    $(_this).addClass('active');
    $(classElement).html(loading);
    $.ajax({
        type: "get",
        url: ajaxUrl,
        data: {
            reload: 1
        },
        success: function (data) {
            //get data to element
            $(classElement).html(data);

            //get all order id đã chọn được gộp
            var lstGoodsOrders = [];
            $("input[name='addOrders[]']").each(function () {
                if (lstGoodsOrders.indexOf($(this).val()) < 0) {
                    lstGoodsOrders.push($(this).val());
                }
            });

            $("input[name='orderID[]']").each(function () {
                if (lstGoodsOrders.indexOf($(this).val()) >= 0) {
                    this.setAttribute("checked", "checked");
//                    this.checked = true;
                }
            });


        }
    });
}

function showElement(classElement) {
    $(classElement).show();
}
function hideElement(classElement) {
    $(classElement).hide()();
}

function autoComplet() {
    $(function () {
        $('.pic-value').typeahead({
            ajax: '/admin/user/search',
            onSelect: function (data) {
                $('.picID').val(data.value);
            }
        });
    });
}
function checkExistUser(_this, classID) {
    if ($(_this).val() === '') {
        $(classID).val('');
    }
}

function updatePriceTotal() {
    var priceTotal = 0;
    $("input[name='prices[]']").each(function () {
        priceTotal = +$(this).val() + priceTotal;
    });
    $('.price-total').val(priceTotal);
}
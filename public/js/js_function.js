
var loading = '<span style="color:#090">Đang tải dữ liệu &nbsp;&nbsp;</span><img width="100px" src="/imgs/icon/loading.gif" width="43" height="11" />';

function ajaxLoadContent(urlAjax, classLoad) {
    $(classLoad).html(loading);
    $.ajax({
        url: urlAjax,
        success: function(data) {
            $(classLoad).html(data);
        }
    });
}

function ajaxEdit(urlAjax) {
    $('#ajax_product').html(loading);
    $.ajax({
        url: urlAjax,
        success: function(data) {
            $('#ajax_product').html(data);
        }
    });
}

function loadContentTab(_this, classTab, urlAjax) {
    $('.tab_content').hide();
    $('.tab_active').removeClass('tab_active');
    $(_this).addClass('tab_active');
    $(classTab).show();
    if ($(classTab).html() === '') {
        ajaxLoadContent(urlAjax, classTab);
    }
}

function showDetail(_this, classDetail, tabShow) {
    if ($(_this).hasClass('enable')) {
        $(classDetail).hide();
        $('.enable').removeClass('enable');
    } else {
        $('.enable').removeClass('enable');
        $(_this).addClass('enable');
        $('.pdetail').hide();
        $(classDetail).show();
        $(tabShow).show();
        $(classDetail + ' span:first-child').addClass('tab_active');
    }
}

function closeContentLeft() {
    if ($('.content_left').hasClass('_close')) {
        $('.content_left').removeClass('_close');
        $('.content_left').addClass('_open');
        $('.content_right').removeClass('width100');
    } else {
        $('.content_left').removeClass('_open');
        $('.content_left').addClass('_close');
        $('.content_right').addClass('width100');
    }
}

function loadData(classLoad, urlAjax) {
    $(classLoad).html(loading);
    $.ajax({
        url: urlAjax,
        success: function(data) {
            $(classLoad).html(data);
        }
    });
}

function showPopup(urlAjax, addClass) {

    $('#ajax_notice').html('<img style="margin-top:4px;float:left; width:43px" src="/imgs/icon/loading.gif" /><em style="font-size:12px; margin-left:10px; color:#090">Đang cập nhật dữ liệu ...</em>');
    $("#form_edit").ajaxForm({
        target: '#ajax_notice',
        success: function(data)
        {
            if (data === 'success') {
                $('.popup_box').fadeOut('fast');
                $('.ajax_content_popup').html('<span style="color:#090">Đang tải dữ liệu &nbsp;&nbsp;</span><img width="100px" src="/imgs/icon/loading.gif" width="43" height="11" />');
                if (urlAjax !== 'none') {
                    $.ajax({
                        url: urlAjax,
                        success: function(data) {
                            $('#content_ajax_tottal').html(data);
                        },
                        error: function() {
                            $('#content_ajax_tottal').html('Có lỗi xảy ra');
                        }
                    });
                }
            }
            else
            {
                $('.ajax_notice').html(data);
            }
        },
    }).submit();
}


function clickToEdit(elementClass, pathSubmit) {
    $(elementClass).editable({
        type: 'text',
        pk: 1,
        method:'get',
        url: pathSubmit,
        ajaxOptions: {
            type: 'get',
            dataType: 'text'
        },
        success: function(response, newValue) {
            //msg will be shown in editable form
            if(response.status == 'error') return response.msg; 
            console.log(response);
        }
    });
}

function changePermisson(_this, mapIDUserTypes) {
    mapIDUserTypes = mapIDUserTypes.replace(/&quot;/g, '"');
    mapIDUserTypes = $.parseJSON(mapIDUserTypes);
    $("input[name='userPermissons[]']").each(function() {
        this.removeAttribute("checked");
        this.checked = false;
        if (0 <= mapIDUserTypes[$(_this).val()]['defaultPermisson'].indexOf($(this).val())) {
            this.setAttribute("checked", "checked");
            this.checked = true;
        }
    });
}
function editDetailUserInfo(ajaxUrl, btnID, formID) {
    $(btnID).addClass('disabled');
    $.ajax({
        url: ajaxUrl,
        type: "POST",
        data: $(formID).serialize(),
        success: function(data) {
            $(btnID).removeClass('disabled');
            console.log(data);
        }
    });
}


function changeCollumnPrice(_this, lstPriceID) {
    var type;
    if ($(_this).prop('checked')) {
        $('.' + $(_this).val()).show();
        type = 'add';
    } else {
        $('.' + $(_this).val()).hide();
        type = 'unset';
    }
    $.ajax({
        url: '/change-lstprice/' + lstPriceID + '/' + type,
        success: function(data) {
        }
    });
}

function loadCollumnPrice() {

    $(".custom_collumn input[name='price[]").each(function() {
        if ($(this).prop('checked')) {
            $('.' + $(this).val()).show();
        } else {
            $('.' + $(this).val()).hide();
        }
    });
}

function submitProductForm(classForm, classLoading)
{
    $(classLoading).html(loading);
    $(classForm).ajaxForm({
        target: classLoading,
        success: function(data)
        {
            if (data === 'success') {
                $('#myModal').modal('hide');
            }
        }
    }).submit();
}

function deleteImage(id){
    $.ajax({
        url: '/detete-image/' + id,
        success: function(data) {
            $('.img_' + id).hide();
        }
    });
}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_supplier', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('supplierNvkdName', 100);
            $table->string('supplierNvkdEmail', 100);
            $table->string('supplierNvkdPhoneNumber', 20);
            $table->string('supplierNvkdSkyAccount', 100);
            $table->string('supplierName', 100);
            $table->string('supplierWebsite', 100);
            $table->text('supplierAddress');
            $table->text('supplierBankInfo');
            $table->unsignedTinyInteger('status');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_supplier');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_product', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->string('code', 50);
            $table->unsignedInteger('categoryID');
            $table->unsignedInteger('colorID');
            $table->unsignedInteger('materialID');
            $table->unsignedInteger('volumeID')->nullable();
            $table->unsignedInteger('weight')->nullable();
            $table->text('description')->nullable();
            $table->text('note')->nullable();
            $table->text('listPriceNote')->nullable();
            $table->unsignedInteger('packingList')->nullable();
            $table->unsignedTinyInteger('StatusID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_product');
    }
}

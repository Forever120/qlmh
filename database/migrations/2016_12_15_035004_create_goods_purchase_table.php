<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_goods_purchase', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('nvmhID');
            $table->text('listGoodsOrderID')->nullable();
            $table->unsignedTinyInteger('statusID');
            $table->string('invoiceFilePath', 260)->nullable();
            $table->string('packingListFilePath', 260)->nullable();
            $table->string('billOfLadingFilePath', 260)->nullable();
            $table->dateTime('orderDate');
            $table->dateTime('deliverDate');
            $table->dateTime('arriveDate')->nullable();
            $table->dateTime('produceDate')->nullable();
            $table->unsignedInteger('price')->nullable();
            $table->unsignedInteger('desposits')->nullable();
            $table->unsignedInteger('remain')->nullable();
            $table->enum('currencyUnit', ['vnd', 'USD', '...']);
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_goods_purchase');
    }
}

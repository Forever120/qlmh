<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPriceOfSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_product_price_of_supplier', function (Blueprint $table) {
            $table->unsignedInteger('productID');
            $table->unsignedInteger('listPriceID');
            $table->unsignedInteger('supplierID');
            $table->unsignedInteger('importProductPrice');
            //$table->primary(['productID', 'listPriceID', 'supplierID']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_product_price_of_supplier');
    }
}

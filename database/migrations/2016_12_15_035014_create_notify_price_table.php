<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifyPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_notify_price', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('priceID')->nullable();
            $table->unsignedTinyInteger('statusID');
            $table->unsignedInteger('nvkdID');
            $table->unsignedInteger('nvmhID')->nullable();
            $table->unsignedInteger('adminID')->nullable();
            $table->unsignedInteger('productID');
            $table->text('priceForNvkd')->nullable();
            $table->text('priceForNvmh')->nullable();
            $table->unsignedInteger('cloneNotifPriceID')->nullable();
            $table->text('notifyUserIDs')->nullable();
            $table->dateTime('deadline');
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_notify_price');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListFunctionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_list_function', function (Blueprint $table) {
            $table->string('functionName', 50);
            $table->unsignedInteger('funcID')->unique();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->primary('functionName');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_list_function');
    }
}

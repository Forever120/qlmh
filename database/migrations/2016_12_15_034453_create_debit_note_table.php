<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebitNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_debit_note', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('supplierID');
            $table->unsignedInteger('nvmhID');
            $table->dateTime('date');
            $table->integer('exchangeMonney');
            $table->unsignedTinyInteger('status');
            $table->unsignedInteger('totalDebitMoney');
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_debit_note');
    }
}

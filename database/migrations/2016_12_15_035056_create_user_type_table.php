<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_user_type', function (Blueprint $table) {
            $table->string('userTypeName', 50);
            $table->unsignedInteger('userTypeID')->unique();
            $table->text('defaultPermisson');
            $table->timestamps();
            $table->primary('userTypeName');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_user_type');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_product_price', function (Blueprint $table) {
            $table->unsignedInteger('productID');
            $table->unsignedInteger('listPriceID');
            $table->unsignedInteger('prodPrice');
            $table->timestamps();
            $table->primary(['productID', 'listPriceID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_product_price');
    }
}

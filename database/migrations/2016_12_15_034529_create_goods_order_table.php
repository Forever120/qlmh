<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_goods_order', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('nvkdID');
            $table->unsignedInteger('suplierID')->nullable();
            $table->unsignedInteger('productID');
            $table->dateTime('createDate');
            $table->dateTime('deliveryToUserDate');
            $table->dateTime('suplierDeliveryDate')->nullable();
            $table->unsignedTinyInteger('statusID');
            $table->integer('numberOfProduct');
            $table->unsignedInteger('priceOfProdect');
            $table->enum('currencyUnit', ['vnd', 'USD', '...']);
            $table->unsignedInteger('numOfPackage');
            $table->unsignedInteger('numProductInPackage');
            $table->text('note');
            $table->unsignedInteger('goodsPurchaseID')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_goods_order');
    }
}

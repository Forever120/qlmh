<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPermissonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zzz_user_permisson', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('userID');
            $table->unsignedInteger('funcID');
            $table->boolean('C');
            $table->boolean('R');
            $table->boolean('U');
            $table->boolean('D');
            $table->boolean('E');
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zzz_user_permisson');
    }
}

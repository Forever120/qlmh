<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        DB::table('zzz_product')->insert([
            'name' => 'product 01',
            'code' => 1,
            'categoryID' => 1,
            'parentId' => 1,
            'colorID    ' => 1,
            'materialID' => 1,
            'volumeID' => 1,
            'weight' => '',
            'description' => '',
            'note' => '',
            'listPriceNote' => '',
            'packingList' => '',
            'StatusID' => '',
            'created_at' => '',
            'weight' => '',
        ]);
        DB::table('zzz_product')->insert([
            'name' => 'product 02',
            'code' => 1,
            'categoryID' => 1,
            'parentId' => 1,
            'colorID    ' => 1,
            'materialID' => 1,
            'volumeID' => 1,
            'weight' => '',
            'description' => '',
            'note' => '',
            'listPriceNote' => '',
            'packingList' => '',
            'StatusID' => '',
            'created_at' => '',
            'weight' => '',
        ]);
        DB::table('zzz_product')->insert([
            'name' => 'product 03',
            'code' => 1,
            'categoryID' => 1,
            'parentId' => 1,
            'colorID    ' => 1,
            'materialID' => 1,
            'volumeID' => 1,
            'weight' => '',
            'description' => '',
            'note' => '',
            'listPriceNote' => '',
            'packingList' => '',
            'StatusID' => '',
            'created_at' => '',
            'weight' => '',
        ]);
        DB::table('zzz_product')->insert([
            'name' => 'product 04',
            'code' => 1,
            'categoryID' => 1,
            'parentId' => 1,
            'colorID    ' => 1,
            'materialID' => 1,
            'volumeID' => 1,
            'weight' => '',
            'description' => '',
            'note' => '',
            'listPriceNote' => '',
            'packingList' => '',
            'StatusID' => '',
            'created_at' => '',
            'weight' => '',
        ]);
    }

}

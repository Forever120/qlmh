<?php

use Illuminate\Database\Seeder;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete old data
        DB::table('zzz_user_type')->delete();
        // Insert default data
        DB::table('zzz_user_type')->insert([
            'userTypeName' => 'Admin',
            'userTypeID' => 1,
            'defaultPermisson' => '1_R,2_R',
        ]);
        DB::table('zzz_user_type')->insert([
            'userTypeName' => 'NVKD',
            'userTypeID' => 2,
            'defaultPermisson' => '1_R',
        ]);
        DB::table('zzz_user_type')->insert([
            'userTypeName' => 'NVMH',
            'userTypeID' => 3,
            'defaultPermisson' => '2_R',
        ]);
    }
}

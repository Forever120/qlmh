<?php
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete old data
        DB::table('zzz_user')->delete();
        // Insert data
        DB::table('zzz_user')->insert([
            'id' => 1,
            'username' => 'admin1',
            'userType' => 1,
            'password' => bcrypt('admin1'),
            'firstName' => str_random(10),
            'lastName' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'phoneNumber' => '1234567890',
            'skypeAccount' => str_random(10).'@skype.com',
        ]);
        DB::table('zzz_user')->insert([
            'id' => 2,
            'username' => 'kd1',
            'userType' => 2,
            'password' => bcrypt('kd1'),
            'firstName' => str_random(10),
            'lastName' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'phoneNumber' => '1234567990',
            'skypeAccount' => str_random(10).'@skype.com',
        ]);
        DB::table('zzz_user')->insert([
            'id' => 3,
            'username' => 'mh1',
            'userType' => 3,
            'password' => bcrypt('mh1'),
            'firstName' => str_random(10),
            'lastName' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'phoneNumber' => '1234567880',
            'skypeAccount' => str_random(10).'@skype.com',
        ]);
        DB::table('zzz_user')->insert([
            'id' => 4,
            'username' => 'mh2',
            'userType' => 3,
            'password' => bcrypt('mh2'),
            'firstName' => str_random(10),
            'lastName' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'phoneNumber' => str_random(10),
            'skypeAccount' => str_random(10).'@skype.com',
        ]);
        DB::table('zzz_user')->insert([
            'id' => 5,
            'username' => 'mh3',
            'userType' => 3,
            'password' => bcrypt('mh3'),
            'firstName' => str_random(10),
            'lastName' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'phoneNumber' => str_random(10),
            'skypeAccount' => str_random(10).'@skype.com',
        ]);
        DB::table('zzz_user')->insert([
            'id' => 6,
            'username' => 'mh4',
            'userType' => 3,
            'password' => bcrypt('mh4'),
            'firstName' => str_random(10),
            'lastName' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'phoneNumber' => str_random(10),
            'skypeAccount' => str_random(10).'@skype.com',
        ]);
    }
}

<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset data
        $this->call(UsersTableSeeder::class);
        $this->call(UserTypeTableSeeder::class);
        $this->call(ListFunctionTalbeSeeder::class);
        $this->call(UserPermissonTalbeSeeder::class);
    }
}

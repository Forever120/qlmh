<?php

use Illuminate\Database\Seeder;

class ListFunctionTalbeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete old data
        DB::table('zzz_list_function')->delete();
        // Insert default data
        DB::table('zzz_list_function')->insert([
            'functionName' => 'LIST_PRICE_SOLD_PRICE',
            'funcID' => 1,
            'note' => 'Quyền để truy cấp giá bán trong bảng giá',
        ]);
        DB::table('zzz_list_function')->insert([
            'functionName' => 'LIST_PRICE_BOUGHT_PRICE',
            'funcID' => 2,
            'note' => 'Quyền để truy cấp giá nhập trong bảng giá',
        ]);
        DB::table('zzz_list_function')->insert([
            'functionName' => 'USER_LIST',
            'funcID' => 3,
            'note' => 'Quyền truy cập danh sách nhân viên',
        ]);
        DB::table('zzz_list_function')->insert([
            'functionName' => 'USER_PERMISSON',
            'funcID' => 4,
            'note' => 'Quyền truy cập User Permisson',
        ]);
    }
}

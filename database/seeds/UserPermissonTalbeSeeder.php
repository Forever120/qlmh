<?php

use Illuminate\Database\Seeder;

class UserPermissonTalbeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete old data
        DB::table('zzz_user_permisson')->delete();
        // Insert default data
        DB::table('zzz_user_permisson')->insert([
            'userID' => 1,
            'funcID' => 1,
            'C' => 1,
            'R' => 1,
            'U' => 1,
            'D' => 1,
            'E' => 1,
            'note' => 'note admin 1, function 1',
        ]);
        DB::table('zzz_user_permisson')->insert([
            'userID' => 1,
            'funcID' => 2,
            'C' => 1,
            'R' => 0,
            'U' => 1,
            'D' => 1,
            'E' => 1,
            'note' => 'note admin 1, function 2',
        ]);
        DB::table('zzz_user_permisson')->insert([
            'userID' => 2,
            'funcID' => 1,
            'C' => 1,
            'R' => 1,
            'U' => 1,
            'D' => 1,
            'E' => 1,
            'note' => 'note nvkd 1, funciton 1',
        ]);
        DB::table('zzz_user_permisson')->insert([
            'userID' => 3,
            'funcID' => 2,
            'C' => 1,
            'R' => 1,
            'U' => 1,
            'D' => 1,
            'E' => 1,
            'note' => 'note nvmh 1, function 2',
        ]);
    }
}
